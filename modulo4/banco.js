db.getCollection("banco").insertMany(
[
    {
        Nome: "Banco Alfa",
        Codigo: "011",
        Agencias: [
            {
                Codigo: "0001",
                Nome: "Web",
                Endereco: {
                    Rua: "Rua Testando",
                    Logradouro: "n 55 loja 1",
                    Bairro: {
                        Nome: "Bairro NA",
                        Cidade: {
                            Nome: "Cidade NA",
                            Estado: {
                                Nome: "Estado NA",
                                Pais: {
                                    Nome: "Brasil"
                                }
                            }
                        }
                    }
                }
            },
            {
                Codigo: "0002",
                Nome: "California",
                Endereco: {
                    Rua: "Rua Testing",
                    Logradouro: "n 122 ",
                    Bairro: {
                        Nome: "Between Hyde and Powell Streets",
                        Cidade: {
                            Nome: "San Francisco",
                            Estado: {
                                Nome: "California",
                                Pais: {
                                    Nome: "EUA"
                                }
                            }
                        }
                    }
                }
            },
            {
                Codigo: "0101",
                Nome: "Londres",
                Endereco: {
                    Rua: "Rua Testing",
                    Logradouro: "n 525 ",//
                    Bairro: {
                        Nome: "Croydon",
                        Cidade: {
                            Nome: "Londres",
                            Estado: {
                                Nome: "Boroughs",
                                Pais: {
                                    Nome: "England"
                                }
                            }
                        }
                    }
                }
            }            
        ]
    },
    {
        Nome: "Banco Beta ",
        Codigo: "241",
        Agencias: [
            {
                Codigo: "0001",
                Nome: "Web",
                Endereco: {
                    Rua: "Rua Testando",
                    Logradouro: "n 55 loja 2",
                    Bairro: {
                        Nome: "Bairro NA",
                        Cidade: {
                            Nome: "Cidade NA",
                            Estado: {
                                Nome: "Estado NA",
                                Pais: {
                                    Nome: "Brasil"
                                }
                            }
                        }
                    }
                }
            }
        ]
    },
    {
        Nome: "Banco Omega ",
        Codigo: "307",
        Agencias: [
            {
                Codigo: "0001",
                Nome: "Web",
                Endereco: {
                    Rua: "Rua Testando",
                    Logradouro: "n 55 loja 3",
                    Bairro: {
                        Nome: "Bairro NA",
                        Cidade: {
                            Nome: "Cidade NA",
                            Estado: {
                                Nome: "Estado NA",
                                Pais: {
                                    Nome: "Brasil"
                                }
                            }
                        }
                    }
                }
            },
            {
                Codigo: "8761",
                Nome: "Itu",
                Endereco: {
                    Rua: "Rua do meio",
                    Logradouro: "n 2233",
                    Bairro: {
                        Nome: "Qualquer",
                        Cidade: {
                            Nome: "Itu",
                            Estado: {
                                Nome: "São Paulo",
                                Pais: {
                                    Nome: "Brasil"
                                }
                            }
                        }
                    }
                }
            },
            {
                Codigo: "4567",
                Nome: "Hermana",
                Endereco: {
                    Rua: "Rua do boca",
                    Logradouro: "n 222",
                    Bairro: {
                        Nome: "Bairro Caminito",
                        Cidade: {
                            Nome: "Buenos Aires",
                            Estado: {
                                Nome: "Buenos Aires",
                                Pais: {
                                    Nome: "Argentina"
                                }
                            }
                        }
                    }
                }
            }
        ]
    }
]
)

