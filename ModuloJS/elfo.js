class Elfo {
    constructor( nome ) {
        this.nome = nome;
    }

    get nome(){
        return this._nome;
    }

    matarElfo(){
        this.status = "morto";
    }

    estaMorto(){
        return this.status = "morto";
    }

    atacarComFlecha() {
        setTimeout( () =>{
            console.log("atacou");
        }, 2000);
    }

}

let legolas = new Elfo("Legolas")
legolas.matarElfo();
legolas.estaMorto;
legolas.atacarComFlecha();