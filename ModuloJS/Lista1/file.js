function imprimirBRL(valor){
    let valorArredondado = parseFloat(valor).toFixed(2);
    let valorEmBrl = valorArredondado.toString();
    let contaPonto = 0;
    let stringBuilder = "";
    for(let i = valorEmBrl.length; i >= 0; i--){
        if(valorEmBrl.charAt(i) == "."){
            stringBuilder = "," + valorEmBrl.substring(i+1, i+3);
            contaPonto = 0;
        }

        if(contaPonto == 3){
            stringBuilder = "." + valorEmBrl.substring(i, i+3) + stringBuilder;
            contaPonto = 0;
        }
        
        contaPonto ++;
    }
    console.log(stringBuilder);
}

imprimirBRL(2000.589);