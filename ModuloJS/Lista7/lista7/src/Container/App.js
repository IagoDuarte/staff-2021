import React, {Component} from 'react';
import './App.css';
import ListaSeries from '../models/ListaSeries';

export default class App extends Component {
  constructor( props ){
    super( props );
    this.listaSeries = new ListaSeries();
  }

  render(){
    console.log(this.listaSeries.filtrarPorNomeAutor('iagod'));
    console.log(this.listaSeries.invalidas());
    console.log(this.listaSeries.calcularSalarios(1));
    console.log(this.listaSeries.filtrarPorGenero('Caos'));
    console.log(this.listaSeries.filtrarPorNome('The'));
    console.log(this.listaSeries.imprimirCreditos(1));
    console.log(this.listaSeries.hashtag());
    return (
      <div className="App">
        <header className="App-header">
        </header>
      </div>
    );
  }
}
