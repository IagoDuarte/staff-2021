import Serie from "./Serie";
import PropTypes from 'prop-types';

export default class ListaSeries{
  constructor(){
    this.series = JSON.parse(`[
      {
        "titulo": "Stranger Things",
        "anoEstreia": 2016,
        "diretor": [
          "Matt Duffer",
          "Ross Duffer"
        ],
        "genero": [
          "Suspense",
          "Ficcao Cientifica",
          "Drama"
        ],
        "elenco": [
          "Winona Ryder",
          "David Harbour",
          "Finn Wolfhard",
          "Millie Bobby Brown",
          "Gaten Matarazzo",
          "Caleb McLaughlin",
          "Natalia Dyer",
          "Charlie Heaton",
          "Cara Buono",
          "Matthew Modine",
          "Noah Schnapp"
        ],
        "temporadas": 2,
        "numeroEpisodios": 17,
        "distribuidora": "Netflix"
      },
      {
        "titulo": "Game Of Thrones",
        "anoEstreia": 2011,
        "diretor": [
          "David Benioff",
          "D. B. Weiss",
          "Carolyn Strauss",
          "Frank Doelger",
          "Bernadette Caulfield",
          "George R. R. Martin"
        ],
        "genero": [
          "Fantasia",
          "Drama"
        ],
        "elenco": [
          "Peter Dinklage",
          "Nikolaj Coster-Waldau",
          "Lena Headey",
          "Emilia Clarke",
          "Kit Harington",
          "Aidan Gillen",
          "Iain Glen ",
          "Sophie Turner",
          "Maisie Williams",
          "Alfie Allen",
          "Isaac Hempstead Wright"
        ],
        "temporadas": 7,
        "numeroEpisodios": 67,
        "distribuidora": "HBO"
      },
      {
        "titulo": "The Walking Dead",
        "anoEstreia": 2010,
        "diretor": [
          "Jolly Dale",
          "Caleb Womble",
          "Paul Gadd",
          "Heather Bellson"
        ],
        "genero": [
          "Terror",
          "Suspense",
          "Apocalipse Zumbi"
        ],
        "elenco": [
          "Andrew Lincoln",
          "Jon Bernthal",
          "Sarah Wayne Callies",
          "Laurie Holden",
          "Jeffrey DeMunn",
          "Steven Yeun",
          "Chandler Riggs ",
          "Norman Reedus",
          "Lauren Cohan",
          "Danai Gurira",
          "Michael Rooker ",
          "David Morrissey"
        ],
        "temporadas": 9,
        "numeroEpisodios": 122,
        "distribuidora": "AMC"
      },
      {
        "titulo": "Band of Brothers",
        "anoEstreia": 20001,
        "diretor": [
          "Steven Spielberg",
          "Tom Hanks",
          "Preston Smith",
          "Erik Jendresen",
          "Stephen E. Ambrose"
        ],
        "genero": [
          "Guerra"
        ],
        "elenco": [
          "Damian Lewis",
          "Donnie Wahlberg",
          "Ron Livingston",
          "Matthew Settle",
          "Neal McDonough"
        ],
        "temporadas": 1,
        "numeroEpisodios": 10,
        "distribuidora": "HBO"
      },
      {
        "titulo": "The JS Mirror",
        "anoEstreia": 2017,
        "diretor": [
          "Lisandro",
          "Jaime",
          "Edgar"
        ],
        "genero": [
          "Terror",
          "Caos",
          "JavaScript"
        ],
        "elenco": [
          "Amanda de Carli",
          "Alex Baptista",
          "Gilberto Junior",
          "Gustavo Gallarreta",
          "Henrique Klein",
          "Isaias Fernandes",
          "João Vitor da Silva Silveira",
          "Arthur Mattos",
          "Mario Pereira",
          "Matheus Scheffer",
          "Tiago Almeida",
          "Tiago Falcon Lopes"
        ],
        "temporadas": 5,
        "numeroEpisodios": 40,
        "distribuidora": "DBC"
      },
      {
        "titulo": "Mr. Robot",
        "anoEstreia": 2018,
        "diretor": [
          "Sam Esmail"
        ],
        "genero": [
          "Drama",
          "Techno Thriller",
          "Psychological Thriller"
        ],
        "elenco": [
          "Rami Malek",
          "Carly Chaikin",
          "Portia Doubleday",
          "Martin Wallström",
          "Christian Slater"
        ],
        "temporadas": 3,
        "numeroEpisodios": 32,
        "distribuidora": "USA Network"
      },
      {
        "titulo": "Narcos",
        "anoEstreia": 2015,
        "diretor": [
          "Paul Eckstein",
          "Mariano Carranco",
          "Tim King",
          "Lorenzo O Brien"
        ],
        "genero": [
          "Documentario",
          "Crime",
          "Drama"
        ],
        "elenco": [
          "Wagner Moura",
          "Boyd Holbrook",
          "Pedro Pascal",
          "Joann Christie",
          "Mauricie Compte",
          "André Mattos",
          "Roberto Urbina",
          "Diego Cataño",
          "Jorge A. Jiménez",
          "Paulina Gaitán",
          "Paulina Garcia"
        ],
        "temporadas": 3,
        "numeroEpisodios": 30,
        "distribuidora": null
      },
      {
        "titulo": "Westworld",
        "anoEstreia": 2016,
        "diretor": [
          "Athena Wickham"
        ],
        "genero": [
          "Ficcao Cientifica",
          "Drama",
          "Thriller",
          "Acao",
          "Aventura",
          "Faroeste"
        ],
        "elenco": [
          "Anthony I. Hopkins",
          "Thandie N. Newton",
          "Jeffrey S. Wright",
          "James T. Marsden",
          "Ben I. Barnes",
          "Ingrid N. Bolso Berdal",
          "Clifton T. Collins Jr.",
          "Luke O. Hemsworth"
        ],
        "temporadas": 2,
        "numeroEpisodios": 20,
        "distribuidora": "HBO"
      },
      {
        "titulo": "Breaking Bad",
        "anoEstreia": 2008,
        "diretor": [
          "Vince Gilligan",
          "Michelle MacLaren",
          "Adam Bernstein",
          "Colin Bucksey",
          "Michael Slovis",
          "Peter Gould"
        ],
        "genero": [
          "Acao",
          "Suspense",
          "Drama",
          "Crime",
          "Humor Negro"
        ],
        "elenco": [
          "Bryan Cranston",
          "Anna Gunn",
          "Aaron Paul",
          "Dean Norris",
          "Betsy Brandt",
          "RJ Mitte"
        ],
        "temporadas": 5,
        "numeroEpisodios": 62,
        "distribuidora": "AMC"
      }
    ]`).map( serie => new Serie(serie) )
  }

  invalidas(){
    const invalidas = this.series.filter( serie => {
      const algumCampoInvalido = Object.values( serie ).some( campo => campo === null || typeof campo === 'undefined');
      const anoEstreia = serie.anoEstreia > new Date().getFullYear();
      return algumCampoInvalido || anoEstreia;
    })
    return `Series Invalidas: ${ invalidas.map( serie => serie.titulo ).join( ' - ' ) }`;
  }

  filtrarPorAno(ano){
    const series = this.series.filter( serie => serie.anoEstreia > ano);
    return series;
  }

  filtrarPorNomeAutor(nomeAtor){
    const resultado = this.series.filter( serie => { return serie.elenco.includes(nomeAtor)});
    return resultado.length > 0;
  }

  mediaDeEpisodios(){
    return parseFloat( this.series.map( serie => serie.numeroEpisodios ).reduce( (prev, cur) => prev + cur, 0) / this.todos.length )
  }

  calcularSalarios(indice){
    const serie = this.series[indice];
    let total = 0;
    total = (serie.elenco.length * 40000 ) + ( serie.diretor.length * 100000 );
    total = total.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
    return total;
  }

  filtrarPorGenero(genero){
    return this.series.filter( serie => {
      return Boolean( serie.genero.find( g => g.toUpperCase() === genero.toUpperCase() ) )
    } )
  }

  filtrarPorNome( nome ){
    let resultado = this.series.filter( serie => { return serie.titulo.includes(nome) } ).map(serie => serie.titulo);
    return resultado;
  }



  imprimirCreditos( indice ){
    this.series[ indice ].elenco.sort( (a, b) => {
      let arrayA = a.split('');
      let arrayB = b.split('');
      return arrayA[ arrayA.length - 1 ].localeCompare( arrayB[ arrayB.length -1 ] );
    });

    this.series[ indice ].diretor.sort( (a, b) => {
      let arrayA = a.split('');
      let arrayB = b.split('');
      return arrayA[ arrayA.length - 1 ].localeCompare( arrayB[ arrayB.length -1 ] );
    });

    let arrayParaJuntar = [];

    arrayParaJuntar.push( `${ this.series[ indice ].titulo }` );
    arrayParaJuntar.push( `Diretores` );

    this.series[ indice ].diretor.forEach( d=> {
      arrayParaJuntar.push( `${ d }` );
    } );

    arrayParaJuntar.push( `Elenco` );

    this.series[ indice ].elenco.forEach( e => {
      arrayParaJuntar.push( `${ e }` );
    } );

    return arrayParaJuntar.join( '\n' )
  }

  nomesAbreviados( nomes ){
    let verifica = nomes.match( '\\s+[a-zA-Z]{1}\\.\\s+' );
    return ( verifica ) ? Boolean( verifica[0] ) : undefined;
  }

  seriesNomesAbreviados(){
    return this.series.filter( serie => {
      return !serie.elenco.find( s => !this.nomesAbreviados( s ));
    } )
  }

  letrasAbreviadas( nomes ){
    if( this.nomesAbreviados( nomes ) ){
      return nomes.match( '\\s+[a-zA-Z]{1}\\.\\s+')[0].charAt(1);
    }
  }

  hashtag(){
    let palavra = "#";
    let array = this.seriesNomesAbreviados();
    array[0].elenco.forEach( s => {
      palavra = palavra.concat( this.letrasAbreviadas( s ) );
    } )
    return palavra;
  }

}

ListaSeries.PropTypes = {
  invalidas: PropTypes.array,
  filtrarPorAno: PropTypes.array,
  filtrarPorNomeAutor: PropTypes.array,
  mediaDeEpisodios: PropTypes.array,
  calcularSalarios: PropTypes.array,
  filtrarPorGenero: PropTypes.array,
  filtrarPorNome: PropTypes.array,
  imprimirCreditos: PropTypes.array,
  hashtag: PropTypes.array
}