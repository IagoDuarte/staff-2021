import React, { Component } from 'react';

import ListaEpisodios from '../models/ListaEpisodios';
import MensagemFlash from '../Components/MensagemFlash';
import EpisodioUi from '../Components/EpisodioUi';
import MeuInputNumero from '../Components/MeuInputNumero';
import BotaoUI from '../Components/BotaoUI'
import EpisodioApi from '../models/episodioApi';
import Episodio from '../models/Episodio';

import './App.css';

export default class App extends Component {
  constructor( props ) {
    super( props );
    this.listaEpisodios = new ListaEpisodios();
    this.sortear = this.sortear.bind( this );
    this.marcarComoAssistido = this.marcarComoAssistido.bind( this );
    this.registrarNota = this.registrarNota.bind(this);
    this.api = new EpisodioApi();
    this.state = {
      deveExibirMensagem:false,
      inputObrigatorio: false
    }
  }

  componentDidMount(){
    const requisicao = [this.api.buscarTodos(), this.api.buscarTodasNotas()];
    Promise.all(requisicao).then(resposta => {

      this.listaEpisodios = new ListaEpisodios(resposta[0], resposta[1]);
      let episodioAleatorio = this.listaEpisodios.episodiosAleatorios;
      console.log(episodioAleatorio)
      this.setState( state => {
        return {
          ...state,
          listaEpisodios: this.listaEpisodios,
          episodio: episodioAleatorio
        }
      })
    })
  
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios;
    this.setState( ( state ) => {
      return {
        ...state,
        episodio
      }
    });
  }

  marcarComoAssistido(){
    const { episodio } = this.state;
    episodio.assistido();
    this.setState( ( state ) => {
      return {
        ...state,
        episodio
      }
    });

  }

  checarInputVazio( nota ){
    let obrigatorio = false;

    if(nota === ''){
      obrigatorio = true;
    }


    this.setState((state) => {
      return {
        ...state,
        inputObrigatorio: obrigatorio
      }
    })

  }

  registrarNota( evt ){
    const nota = evt.target.value;
    const { episodio } = this.state;
    let valorMensagemFlash, corMensagemFlash;
    this.checarInputVazio(nota)

    if( episodio.validarNota(nota)){
      console.log(nota);
      episodio.avaliar( nota ).then(
        corMensagemFlash = 'verde',
        valorMensagemFlash = 'Nota registrada com sucesso!',
        this.exibirMensagem({ corMensagemFlash, valorMensagemFlash })
      );

    } else {
      corMensagemFlash = 'vermelho';
      valorMensagemFlash = 'Informar uma nota valida(entre 1 e 5)!';
    }

    this.exibirMensagem({ corMensagemFlash, valorMensagemFlash })
  }

  exibirMensagem = ({corMensagemFlash, valorMensagemFlash}) => {
    this.setState((state) => {
      return {
        ...state,
        deveExibirMensagem:true,
        valorMensagemFlash,
        corMensagemFlash
      }
    })
  }

  atualizarMensagem = devoExibir => {
    this.setState((state) => {
      return {
        ...state,
        deveExibirMensagem: devoExibir
      }
    })
  }
  
  render(){
    const { episodio, deveExibirMensagem, corMensagemFlash, valorMensagemFlash } = this.state;
    return ( !episodio ? (<div> Nenhum episodio encontrado! </div>) : (
      <div className="App">
        <MensagemFlash segundos = '3' atualizar = { this.atualizarMensagem } exibir = { deveExibirMensagem } mensagem = { valorMensagemFlash } corMensagemFlash = { corMensagemFlash }></MensagemFlash>
        
        <header className="App-header">
          <BotaoUI mensagem = "TodosEpisodios" link = "/todosEpisodios"/>
          <EpisodioUi episodio = { episodio }></EpisodioUi>

          <div className = "botoes">
            <BotaoUI cor = "verde" evento = {this.sortear} mensagem="Próximo"/>
            <BotaoUI cor = "azul" evento = {this.marcarComoAssistido} mensagem="Ja Assisti"/>
            <BotaoUI mensagem = "Listagem" link = {{pathname:"/listaAvaliacoes", state: this.listaEpisodios.episodiosAvaliados}}/>
          </div>

          <MeuInputNumero 
            inputObrigatorio = { this.state.inputObrigatorio }
            mensagemSpanNota = "Qual sua nota para esse episódio?"
            deveExibirInput = { episodio.foiAssistido || false }
            episodio = {episodio}
            obrigatorio
            placeholder = "Insira um numero de 1 a 5."
            registrarNota = { this.registrarNota }/>
        </header>
      </div>
      ))
  }
}