import React, { Component } from 'react';
import BotaoUI from '../Components/BotaoUI';
import EpisodioApi from '../models/episodioApi';
import ListaEpisodios from '../models/ListaEpisodios';

export default class TelaListagem extends Component{
  constructor( props ){
    super(props);
    this.episodiosAvaliados = this.props.location.state;
    this.api = new EpisodioApi();
  }

  componentDidMount(){
    const requisicao = [this.api.buscarTodos(), this.api.buscarTodasNotas()];
    Promise.all(requisicao).then(resposta => {
      this.listaEpisodios = new ListaEpisodios(resposta[0], resposta[1]);
      this.listaEpisodios.ordenarEpisodios();
      this.episodiosAvaliados = this.listaEpisodios.episodiosAvaliados;
      this.setState( state => {
        return {
          ...state,
          listaEpisodios: this.listaEpisodios,
        }
      })
    })
  
  }

  render(){
    return (
      <React.Fragment>
        <BotaoUI mensagem = "Tela Inicial" link="/" nome="Página Inicial" />
          {
          this.episodiosAvaliados.length > 0 ?
          <div>{this.episodiosAvaliados.map( (episodio, i) =>(
            <React.Fragment key={i}>
             <p>Episodio: {episodio.nome} - Nota: {episodio.mediaNota}</p>
             <p>Temporada: {episodio.temporada} - Ordem: {episodio.ordem}</p>
             <BotaoUI mensagem = "Ir para Detalhes" link = {`/detalhesEpisodio/${episodio.id}`}></BotaoUI>
            </React.Fragment>
          ) )}</div>
          : <p>Sem episodios avaliados</p>
          
         }
      </React.Fragment>
    )
  }
}