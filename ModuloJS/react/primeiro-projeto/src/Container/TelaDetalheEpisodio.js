import React, { Component } from 'react';
import BotaoUI from '../Components/BotaoUI';
import Episodio from '../models/Episodio';
import EpisodioApi from '../models/episodioApi';

export default class TelaDetalheEpisodio extends Component{
  constructor(props){
    super(props);
    this.idEpisodio = this.props.match.params.id;
    this.api = new EpisodioApi();
    this.state = {

    }
  }

  componentDidMount(){
    const requisicao = [this.api.buscarDetalhes(this.idEpisodio),
                        this.api.buscarEspecifico(this.idEpisodio),
                        this.api.buscarNotasPorIdEpisodio(this.idEpisodio)];

    Promise.all( requisicao ).then( resposta => {

      this.detalhes = resposta[0];
      this.episodio = resposta[1];
      this.notasEpisodioServidor = resposta[2];
      this.notasEspisodio = this.notasEpisodioServidor.map(n => parseInt(n.nota));
      console.log(this.notasEspisodio);
      this.mediaNota = parseFloat(this.notasEspisodio.reduce((a, b) => a + b, 0) / this.notasEspisodio.length).toFixed(2)
      this.setState( state =>{
        return {
          ...state,
          detalhes: this.detalhes,
          episodio: this.episodio
        }
      } )
    })

  }

  formatarData(){
    let dataFormatada = this.state.detalhes.dataEstreia.split('T')[0].split('-');
    return `Data de estreia: ${ dataFormatada[2]}/${dataFormatada[1]}/${dataFormatada[2]}`
  }
  
  render(){
    const { episodio, detalhes } = this.state;
    return(
      !episodio ? <h2>Aguardando</h2>
      : (
        <React.Fragment>
          <BotaoUI mensagem = "Tela Inicial" link="/" nome="Página Inicial" />
          <h2>{episodio.nome}</h2>
          <img src= {episodio.thumbUrl} alt=""></img>
          <p>Temporada: { episodio.temporada } - Episodio: {episodio.ordem}</p>
          <p>Duracao: { episodio.duracao }</p>
          <p>Nota Imdb: { detalhes.notaImdb }</p>
          <p>Sinopse: { detalhes.sinopse }</p>
          <p>Media: { this.mediaNota }</p>
          <p>Data Estreia: { this.formatarData() }</p>
        </React.Fragment>
      )
    )
  }
}