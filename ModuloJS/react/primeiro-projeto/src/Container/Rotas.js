import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './App';
import TelaDetalheEpisodio from './TelaDetalheEpisodio';
import TelaListagem from './TelaListagem';
import TodosEpisodios from './TodosEpisodios';

export default class Rotas extends Component {
  render(){
    return (
      <Router>
        <Route path="/" exact component={ App } />
        <Route path="/listaAvaliacoes" component={ TelaListagem } />
        <Route path="/detalhesEpisodio/:id" component= {TelaDetalheEpisodio}/>
        <Route path="/todosEpisodios" component= {TodosEpisodios}/>
      </Router>
    );
  }
}