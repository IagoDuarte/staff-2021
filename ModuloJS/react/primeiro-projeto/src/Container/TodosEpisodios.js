import React, { Component } from "react";
import ListaEpisodios from "../models/ListaEpisodios";
import { Link } from "react-router-dom";
import EpisodioApi from "../models/episodioApi";
import BotaoUI from "../Components/BotaoUI";

export default class TodosEpisodios extends Component{
  constructor(props){
    super(props)
    this.api = new EpisodioApi();
    this.duracaoCrescente = true; 
    this.state = {
      duracaoCrescente: this.duracaoCrescente
    }
  }

  componentDidMount(){
    const requisicao = [this.api.buscarTodos(), this.api.buscarTodasNotas(), this.api.buscarTodosDetalhes()];
    Promise.all(requisicao).then(resposta => {
      this.listaEpisodios = new ListaEpisodios(resposta[0], resposta[1], resposta[2]);
      this.listaEpisodios.ordenarEpisodios();
      this.setState( state => {
        return {
          ...state,
          listaEpisodios: this.listaEpisodios
        }
      })
    })
  }

  ordenarPorDuracao(){
    if(this.state.duracaoCrescente){
      this.listaEpisodios.ordenarPorDuracao();
    }else{
      this.listaEpisodios.ordenarPorDuracaoDesc();
    }
    this.duracaoCrescente = !this.duracaoCrescente;

    this.setState( state => {
      return {
        ...state,
        listaEpisodios: this.listaEpisodios,
        duracaoCrescente: this.duracaoCrescente
      }
    })

    console.log(this.duracaoCrescente)
  }

  render(){
    const {listaEpisodios} = this.state;
    return(
      !listaEpisodios ? <h1>Aguarde...</h1> :
      <div>
        <BotaoUI evento= {this.ordenarPorDuracao.bind(this)} mensagem = "Ordenar por Duracao" cor="azul" />
        { listaEpisodios.todos.map( (episodio, i) => {
         return (
          <div key={i}>
            <Link to={`/detalhesEpisodio/${episodio.id}`} > { episodio.nome } </Link>  
          </div>
         )
        })}
      </div>
    )
  }

}