import React from 'react';

const PrimeiroComponente = props => <button type = "number">{ props.valorQueEuQuero }</button>

const Titulo = () => <h1> React Mirror </h1>;

const Familia = props => {
  return(
    <React.Fragment>
      { props.nome }
      { props.sobrenome }
    </React.Fragment>
  );
}

export default PrimeiroComponente;
export { Titulo, Familia };