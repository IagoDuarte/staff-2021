import React, { Component } from 'react';

export default class EpisodioUi extends Component {
  render(){
    const { episodio } = this.props;
    
    return (
      <React.Fragment>
        <h2>{ episodio.nome }</h2>
        <img src = { episodio.imagem } alt = "" ></img>
        <span>Ja Assist? {episodio.foiAssistido ? `Sim, ${ episodio.qtdVezesAssistido } Vezes` : 'Nao'} </span>
        <span>Duracao: { episodio.duracaoEmMin }</span>
        <span>Temporada/Episodio: { episodio.temporadaEpisodio } </span>
      </React.Fragment>
    );
  }
}