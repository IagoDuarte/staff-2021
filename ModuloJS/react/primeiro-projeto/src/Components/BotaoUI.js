import React, { Component } from 'react';
import {Link} from 'react-router-dom';

export default class BotaoUI extends Component {
  dispararEvento(){
    this.props.evento();
  }

  render(){
    return (
      <React.Fragment>
        { this.props.link
        ? <Link className = "btn link branco" to={this.props.link}> {this.props.mensagem} </Link>
        : <button className = {`btn ${this.props.cor}`} onClick = {() => this.dispararEvento() } >{this.props.mensagem}</button>
        }
        </React.Fragment>
    );
  }
}