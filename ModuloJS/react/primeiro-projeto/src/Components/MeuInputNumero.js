import React, { Component } from 'react';

export default class MeuInputNumero extends Component {

  perderFoco = evt => {
    const { obrigatorio, atualizarValor } = this.props;
    const nota = evt.target.value;
    const erro = obrigatorio && !nota;
    atualizarValor( { erro, nota } )
  }

  gerarCampoValorNota(){
    const { episodio } = this.props;
    return (
      <div>
        { episodio.mediaNota > 0 && (
          <span>Media de notas: { episodio.mediaNota }</span>
        )}
      </div>
    );
  }

  render(){
    const {placeholder, inputObrigatorio, registrarNota, mensagemSpanNota, deveExibirInput } = this.props;

    return deveExibirInput &&(
          <React.Fragment>
            { this.gerarCampoValorNota() }
            { mensagemSpanNota  && <span>{ mensagemSpanNota }</span>}
            <input type="number"  className = { inputObrigatorio ? 'borda-vermelha' : '' } placeholder={ placeholder } onBlur={ registrarNota }/>
            { inputObrigatorio && <span className = "span-obrigatorio">*Obrigatorio</span>}
          </React.Fragment>);
  }
}