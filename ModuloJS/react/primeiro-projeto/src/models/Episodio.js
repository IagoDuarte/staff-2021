import EpisodioApi from "./episodioApi";

export default class Episodio {
  constructor( { nome, duracao, temporada, ordemEpisodio, thumbUrl, id } ){
    this.id = id;
    this.nome = nome;
    this.duracao = duracao;
    this.temporada = temporada;
    this.ordem = ordemEpisodio;
    this.imagem = thumbUrl;
    this.qtdVezesAssistido = 0;
    this.nota = [];
    this.detalhes = {};
    this.api = new EpisodioApi();
  }
  

  get duracaoEmMin(){
    return `${ this.duracao } min`;
  }

  get temporadaEpisodio() {
    return `${ this.temporada.toString().padStart( 2, 0 ) }/${ this.ordem.toString().padStart( 2, 0 )}`;
  }


  avaliar( nota ){
    this.nota.push(parseInt(nota));
    return this.api.registrarNota({nota: nota, idEpisodio: this.id});
  }

  assistido( ) {
    this.foiAssistido = true;
    this.qtdVezesAssistido += 1;
  }

  validarNota(nota){
    return (nota >=1 && nota <= 5);
  }

  get mediaNota(){
    return parseFloat(this.nota.reduce((a, b) => a + b, 0) / this.nota.length).toFixed(2);
  }

  setarDetalhe(detalhe){
    this.detalhes = detalhe;
  }
}