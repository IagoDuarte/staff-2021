import axios from 'axios';

const url = 'http://localhost:9000/api/';

const _get = url => new Promise( ( resolve, reject ) => axios.get( url ).then( response => resolve( response.data ) ) );
const _post = ( url, dados ) => new Promise( ( resolve, reject ) => axios.post( url, dados ).then( response => resolve( response.data ) ) );


export default class EpisodioApi {

  async buscarTodos(){
    return await _get(`${ url }episodios`)
  }

  async registrarNota({nota, idEpisodio}){
    const response = await _post(`${url}notas`, {nota, idEpisodio});
    return response[0];
  }

  async buscarEspecifico(id){
    const response = await _get(`${ url }episodios/${id}`)
    console.log(response)
    return response;
  }


  async buscarTodasNotas() {
    return await _get( `${ url }notas` );
  }

  async buscarDetalhes(id){
    const response = await _get(`${ url }episodios/${id}/detalhes`)
    return response[0];
  }

  async buscarNotasPorIdEpisodio(id){
    const response = await _get(`${ url }notas?idEpisodio=${id}`);
    return response;
  }

  async buscarTodosDetalhes(id){
    const response = await _get(`${ url }detalhes`);
    return response;
  }
}