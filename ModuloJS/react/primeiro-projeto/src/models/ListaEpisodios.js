import Episodio from "./Episodio";


function sortear( min, max ){
  min = Math.ceil( min );
  max = Math.floor( max );
  return Math.floor( Math.random() * ( max - min ) ) + min;
}

export default class ListaEpisodios{
  constructor( todosEpisodios = [], todasNotas = [], todosDetalhes = []) {
    this.todos = todosEpisodios.map( ep => new Episodio(ep));
    this.notas = todasNotas;
    this.detalhes = todosDetalhes;
    this.setarNotas(todasNotas);
    this.setarDetalhes(todosDetalhes)
  }

  get episodiosAleatorios(){
    const indice = sortear( 0, this.todos.length );
    return this.todos[indice];
  }

  get episodiosAvaliados(){
    return this.todos.filter(episodio => {return episodio.nota.length > 0});
  }

  setarNotas( notas ) {
    this.todos.forEach(episodio =>{
      notas.forEach( nota => {
        if(episodio.id === nota.idEpisodio){
          episodio.nota.push(parseInt(nota.nota[0]));
        }
      })
    })
	}

  setarDetalhes( detalhes ){
    this.todos.map( episodio => {
      let detalheEpisodio =  detalhes.find( detalhe => detalhe.id === episodio.id);
      return episodio.setarDetalhe(detalheEpisodio)
    })
  }

  ordenarEpisodios(){
    this.todos.sort( (a, b) => {
      return a.temporada < b.temporada ? -1
      : a.temporada > b.temporada ? 1
      : a.ordem < b.ordem ? -1
      : a.ordem > b.ordem ? 1 : 0;
    } )
  }

  ordenarPorDuracao(){
    return this.todos.sort( ( a, b ) => {
      return a.duracao - b.duracao
    } )
  }
  ordenarPorDuracaoDesc(){
    return this.todos.sort( ( a, b ) => {
      return b.duracao - a.duracao
    } )
  }

}
