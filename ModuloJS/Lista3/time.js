class Time{

    constructor(nome, tipoEsporte, liga){
        this._nome = nome;
        this._tipoEsporte = tipoEsporte;
        this._status = "ativo";
        this._liga = liga;
        this._jogadores = [];
    }

    adicionarJogador(jogador){
        this._jogadores.push( jogador );
    }

    buscarJogadorPorNome(nome){
        return this._jogadores.find( jogador => jogador.nome == nome);
    }

    buscarJogadorPorNumero(numero){
        return this._jogadores.find( jogador => jogador.numero == numero);
    }

}

class Jogador{
    constructor(nome, numero){
        this._nome = nome;
        this._numero = numero;
    }

    get nome(){
        return this._nome;
    }

    get numero(){
        return this._numero;
    }
}

let gremio = new Time("Gremio", "Futebol", "Municipal");
let dc = new Jogador("Douglas Costa", 10);
let jp = new Jogador("Jean Pyerre", 88);
gremio.adicionarJogador(dc);
gremio.adicionarJogador(jp);
let jogador = gremio.buscarJogadorPorNome("Douglas Costa");
let jogador2 = gremio.buscarJogadorPorNumero(88);
console.log(jogador);
console.log(jogador2);