class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objDaApi ) {
    this._nome = objDaApi.name;
    this._imagem = objDaApi.sprites.front_default;
    this._altura = objDaApi.height;
    this._id = objDaApi.id;
    this._peso = objDaApi.weight;
    this._tipos = objDaApi.types;
    this._estatisticas = objDaApi.stats;
  }

  get nome() {
    return this._nome;
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `${ this._altura } cm  `
  }

  get id() {
    return this._id;
  }

  get peso() {
    return `${ this._peso / 10 } kg`
  }

  get tipos() {
    return this._tipos;
  }

  get estatisticas() {
    return this._estatisticas;
  }
}
