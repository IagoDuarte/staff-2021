const pokeApi = new PokeApi();
const mensagemRetorno = document.getElementById( 'mensagem-retorno' );

const renderizar = ( pokemon ) => {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );

  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = pokemon.nome;

  const imagem = dadosPokemon.querySelector( '.thumb' );
  imagem.src = pokemon.imagem;

  const id = dadosPokemon.querySelector( '.id' );
  id.innerHTML = pokemon.id;

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = pokemon.altura;

  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = pokemon.peso;

  const listaTipos = dadosPokemon.querySelector( '.listaTipos' );
  listaTipos.innerHTML = '';
  pokemon.tipos.forEach( tipo => {
    const item = document.createElement( 'li' );
    item.innerHTML = tipo.type.name;
    listaTipos.appendChild( item );
  } );

  const estatisticas = dadosPokemon.querySelector( '.estatisticas' );
  estatisticas.innerHTML = '';
  pokemon.estatisticas.forEach( estatistica => {
    const item = document.createElement( 'li' );
    item.innerHTML = `${ estatistica.stat.name } : ${ parseFloat( estatistica.base_stat ) }`;
    estatisticas.appendChild( item );
  } );
}

function imprimirRetorno( mensagem ) {
  mensagemRetorno.innerHTML = mensagem;
}

function mostrarListInfos() {
  const list = document.getElementById( 'hide' );
  list.style.display = 'block';
}

function buscarPokemonEspecifico() { // eslint-disable-line no-unused-vars
  const idBusca = document.getElementById( 'input-id-pokemon' ).value;
  const idPokemonAtual = document.getElementById( 'id-pokemon' ).innerHTML;

  if ( idBusca !== idPokemonAtual ) {
    if ( !Number.isNaN( idBusca ) ) {
      pokeApi.buscarEspecifico( idBusca )
        .then( pokemon => {
          const poke = new Pokemon( pokemon );
          renderizar( poke );
          piscarLuz(); // eslint-disable-line no-undef
          mostrarListInfos();
        } ).catch( () => {
          console.log( 'Erro' );
        } )
      imprimirRetorno( '' );
    } else {
      piscarLuzVermelha() // eslint-disable-line no-undef
      imprimirRetorno( 'Digite um id válido!' );
    }
  }
}

function sortear() {
  return Math.floor( Math.random() * 893 + 1 );
}

function limparInput() {
  document.getElementById( 'input-id-pokemon' ).value = '';
}

function buscarPokemonAleatorio() { // eslint-disable-line no-unused-vars
  let numerosSorteados = JSON.parse( localStorage.getItem( 'numerosSorteados' ) || '[]' );
  let numeroAleatorio = sortear()

  limparInput();

  while ( numerosSorteados.indexOf( numeroAleatorio ) >= 0 ) {
    numeroAleatorio = sortear()
  }

  numerosSorteados = [...numerosSorteados, numeroAleatorio];
  localStorage.setItem( 'numerosSorteados', JSON.stringify( numerosSorteados ) );

  pokeApi.buscarEspecifico( numeroAleatorio )
    .then( pokemon => {
      const poke = new Pokemon( pokemon );
      renderizar( poke );
    } );
  imprimirRetorno( '' );
  mostrarListInfos();
}
