function piscarLuz() { // eslint-disable-line no-unused-vars
  const circulo = document.getElementById( 'circle' );
  circulo.style.backgroundColor = 'rgb(5, 255, 251)';
  circulo.style.boxShadow = '0px 0px 30px 9px rgb(5, 255, 251)';
  setTimeout( () => {
    circulo.style.backgroundColor = 'rgb(106, 106, 228)';
    circulo.style.boxShadow = '';
  }, 600 );
}

function piscarLuzVermelha() { // eslint-disable-line no-unused-vars
  const circulo = document.getElementById( 'luz-vermelha' );
  circulo.style.backgroundColor = 'rgb(248, 62, 62)';
  circulo.style.boxShadow = '0px 0px 30px 9px rgb(248, 62, 62)';
  setTimeout( () => {
    circulo.style.backgroundColor = 'rgb(181, 6, 6)';
    circulo.style.boxShadow = '';
  }, 600 );
}
