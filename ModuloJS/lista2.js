function cardapioIFood( veggie = true, comLactose = true ) {
    let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva',  
    ]
    let i = 0
  
    if ( comLactose ) {
      cardapio.push( 'pastel de queijo' )
    }
  
    cardapio = [...cardapio,
        'pastel de carne',
        'empada de legumes marabijosas'
    ]
  
    if ( veggie ) {
      cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1 )
      cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
    }
    
    const resultadoFinal = cardapio.map( alimento => alimento.toUpperCase() );
    return resultadoFinal;
  }
  
  cardapioIFood() 