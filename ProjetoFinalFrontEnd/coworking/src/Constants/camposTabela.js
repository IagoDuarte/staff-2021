const Campos = {
  CLIENTE: {
    COLUNAS: [
      { title: 'Nome', dataIndex: 'nome', key: 'nome' },
      { title: 'CPF', dataIndex: 'cpf', key: 'cpf' },
      { title: 'Data de Nascimento', dataIndex: 'dataNascimento', key: 'dataNascimento' }],
    SUBCOLUNAS: [
      { title: 'Contato', dataIndex: 'contato', key: 'contato' },
      { title: 'Tipo Contato', dataIndex: 'tipoContato', key: 'tipoContato' }]
  },

  ESPACO: {
    COLUNAS: [
      { title: 'ID', dataIndex: 'id', key: 'id' },
      { title: 'Nome', dataIndex: 'nome', key: 'nome' },
      { title: 'Quantidade de Pessoas', dataIndex: 'qntPessoas', key: 'qntPessoas' },
      { title: 'Valor', dataIndex: 'valor', key: 'valor' }]
  },

  ESPACOPACOTE: {
    COLUNAS: [
      { title: 'Nome', dataIndex: 'nome', key: 'nome' },
      { title: 'Tipo da Contratacao', dataIndex: 'tipoContratacao', key: 'tipoContratacao' },
      { title: 'Quantidade', dataIndex: 'quantidade', key: 'quantidade' },
      { title: 'Prazo', dataIndex: 'prazo', key: 'prazo' }]
  },

  PACOTES: {
    COLUNAS: [
      { title: 'ID', dataIndex: 'id', key: 'id' },
      { title: 'Quantidade de espacos', dataIndex: 'qtdEspacos', key: 'qtdEspacos' },
      { title: 'Valor', dataIndex: 'valor', key: 'valor' },
    ],

    SUBCOLUNAS: [
      { title: 'Espaco', dataIndex: 'espaco', key: 'espaco' },
      { title: 'Tipo da Contratacao', dataIndex: 'tipoContratacao', key: 'tipoContratacao' },
      { title: 'Quantidade', dataIndex: 'quantidade', key: 'quantidade' },
      { title: 'Prazo', dataIndex: 'prazo', key: 'prazo' }]
  },

  CLIENTEPACOTE: {
    COLUNAS: [
      { title: 'ID Pacote', dataIndex: 'idPacote', key: 'idPacote' },
      { title: 'Quantidade de espacos', dataIndex: 'qtdEspacos', key: 'qtdEspacos' },
      { title: 'Nome do Cliente', dataIndex: 'nomeCliente', key: 'nomeCliente' },
      { title: 'CPF do Cliente', dataIndex: 'cpfCliente', key: 'cpfCliente' },
      { title: 'Valor', dataIndex: 'valor', key: 'valor' }]
  },

  CONTRATACAO: {
    COLUNAS: [
      { title: 'Espaco', dataIndex: 'nomeEspaco', key: 'nomeEspaco' },
      { title: 'Quantidade de pessoas', dataIndex: 'qntPessoas', key: 'qntPessoas' },
      { title: 'Nome do Cliente', dataIndex: 'nomeCliente', key: 'nomeCliente' },
      { title: 'CPF do Cliente', dataIndex: 'cpfCliente', key: 'cpfCliente' },
      { title: 'Valor', dataIndex: 'valor', key: 'valor' }],
    SUBCOLUNAS: [
      { title: 'Tipo da Contratacao', dataIndex: 'tipoContratacao', key: 'tipoContratacao' },
      { title: 'Quantidade', dataIndex: 'quantidade', key: 'quantidade' },
      { title: 'Prazo', dataIndex: 'prazo', key: 'prazo' },
    ]
  },

  SALDO: {
    COLUNAS: [
      { title: 'Cliente', dataIndex: 'nomeCliente', key: 'nomeCliente' },
      { title: 'Espaco', dataIndex: 'nomeEspaco', key: 'qntPessoas' },
      { title: 'Quantidade', dataIndex: 'quantidade', key: 'quantidade' },
      { title: 'Tipo da Contratacao', dataIndex: 'tipoContratacao', key: 'tipoContratacao' },
      { title: 'Vencimento', dataIndex: 'vencimento', key: 'vencimento' }],
  }
}

export default Campos;