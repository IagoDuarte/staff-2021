import React from "react";
import { Route, Redirect } from 'react-router-dom'

const PrivateRoutes = ( { component: Component, ...resto } ) => (
  <Route { ...resto } render={ props => (
    localStorage.getItem( 'userToken' ) ?
      <Component { ...props } /> :
      <Redirect to={{ pathname: '/', state: { from: props.location } }} />
  )}
  />
);


export default PrivateRoutes;