import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import App from '../App';
import Cadastro from '../Pages/Cadastro';
import Login from '../Pages/Login';
import PrivateRoutes from './PrivateRoutes'
import PaginaInicial from '../Pages/PaginaInicial'

export default class Rotas extends Component {
  render(){
    return (
      <Router>
        <Route path="/" exact component={ Login } />
        <Route path="/cadastro" component={ Cadastro } />
        <PrivateRoutes path="/home" component={ PaginaInicial }/>
      </Router>
    );
  }
}