import { Card } from 'antd';
import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { Form, Input, Button } from '../../Components/Antd/Form'
import Api from '../../Models/Api';
import CadastroModel from '../../Models/CadastroModel';
import LoginModel from '../../Models/LoginModel';
import { Link } from 'react-router-dom';

const FormularioUi = ({ isLogin }) => {
  const [tentativa, setTentativa] = useState();
  const api = new Api();
  let linkAlternativo = isLogin ? { path: "/cadastro", mensagem: "Cadastre-se" } : { path: "/", mensagem: "Login" }

  const cadastrarUsuario = (valores) => {
    let usuario = new CadastroModel(valores.nome, valores.email, valores.login, valores.senha);
    let requisicao = [api.cadastrarUsuario(usuario)];
    Promise.all(requisicao).then(response => {
      <Redirect push to='/' />
    })
  }

  const logar = (valores) => {
    let login = new LoginModel(valores.login, valores.senha);
    let requisicao = [api.autenticarUsuario(login)];
    Promise.all(requisicao).then(() => {
      setTentativa(tentativa + 1)
    })
  }

  return (
    <React.Fragment>
      {localStorage.getItem("userToken") ? <Redirect push to='/home' /> :
        <Card>
          <h1>{`${isLogin ? " Login Coworking " : " Faça seu cadastro! "}`}</h1>
          <Form name="basic" labelAlign='left' labelCol={{ span: 6 }} wrapperCol={{ span: 16 }} onFinish={isLogin ? logar : cadastrarUsuario}>
            {!isLogin && (
              <React.Fragment>
                <Form.Item label="Nome" name="nome" rules={[{ required: true, message: 'Insira seu Login' }]}>
                  <Input />
                </Form.Item>
                <Form.Item label="Email" name="email" rules={[{ required: true, message: 'Insira seu Email' }]}>
                  <Input />
                </Form.Item>
              </React.Fragment>
            )}
            <Form.Item label="Login" name="login" rules={[{ required: true, message: 'Insira seu Login' }]}>
              <Input />
            </Form.Item>

            <Form.Item label="Senha" name="senha" rules={[{ required: true, message: 'Insira sua senha!' }]}>
              <Input.Password />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4, span: 16 }}>
              <Button type="primary" htmlType="Enviar">
                Enviar
              </Button>
                <Link style={{ display: 'block', marginTop: '10px' }} to={linkAlternativo.path}><Button> {linkAlternativo.mensagem} </Button></Link>
            </Form.Item>
          </Form>
        </Card>
      }
    </React.Fragment>
  )
}

export default FormularioUi;