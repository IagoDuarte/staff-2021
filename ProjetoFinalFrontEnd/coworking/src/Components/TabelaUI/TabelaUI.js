import { Table } from "antd"

const TabelaUI = (props) => {
  const { lista, expansiva, colunas, subColunas, seletiva, evento, model } = props;

  const expandedRowRender = row => {
    if (model === "cliente") {
      let data = row.contatos.map(contato => { return { contato: contato.contato, tipoContato: contato.tipoContato.nome } })
      return <Table columns={subColunas} dataSource={data} pagination={false} />;
    }

    else if (model === "pacote") {
      let data = row.espacoPacote.map(ep => {
        return {
          espaco: ep.espaco.nome,
          tipoContratacao: ep.tipoContratacao,
          quantidade: ep.quantidade,
          prazo: ep.prazo
        }
      })
      return <Table columns={subColunas} dataSource={data} pagination={false} />;
    }

    else if (model === "clientePacote") {
      let data = row.espacos.map(e => { return { ...e, espaco: e.espaco.nome } })
      return <Table columns={subColunas} dataSource={data} pagination={false} />;
    }

    else if (model === "contratacao"){
      let data = [{
        tipoContratacao:row.tipoContratacao,
        quantidade:row.quantidade,
        prazo:row.prazo
      }]
      return <Table columns={subColunas} dataSource={data} pagination={false} />;
    }
    
  }

  return (
    <Table
      dataSource={lista}
      expandable={expansiva ? { expandedRowRender } : null}
      columns={colunas}
      rowSelection={seletiva ? { type: "radio", onChange: evento } : null}
    />
  )
}

export default TabelaUI;