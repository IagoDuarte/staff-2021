import { Form, Input, Button, Cascader } from 'antd'

const FormCadastro = (props) => {
  const { campos, evento } = props;

  const camposEspeciais = ["cascader"];

  const renderizarCampoEspecial = (campo, indiceMap) => {
    if (campo.tipo === "cascader") {
      return (
        <Form.Item key={indiceMap} wrapperCol={{ offset: 0, span: 24 }} label={campo.label} name={campo.nome} rules={[{ required: true, message: `Insira seu ${campo}` }]}>
          <Cascader name={campo.nome} options={campo.opcoes} ></Cascader>
        </Form.Item>
      )
    }
  }

  return (
    <Form layout="vertical" labelCol={{ span: 24 }} wrapperCol={{ span: 16 }} onFinish={evento}>
      {campos.map((campo, i) => {
        return (
          camposEspeciais.includes(campo.tipo) ? renderizarCampoEspecial(campo, i)
            :
            <Form.Item key={i} wrapperCol={{ offset: 0, span: 24 }} label={campo.label} name={campo.nome} rules={[{ required: true, message: `Insira seu ${campo}` }]}>
              <Input type={campo.tipo} />
            </Form.Item>
        )
      })}
      <Form.Item style={{ textAlign: 'center', marginTop: '30px' }} wrapperCol={{ offset: 4, span: 16 }}>
        <Button type="primary" htmlType="Enviar">
          Submit
        </Button>
      </Form.Item>
    </Form>
  )
}

export default FormCadastro;