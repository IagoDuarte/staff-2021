import { Button, Layout, Menu, PageHeader } from 'antd';
import { ScheduleOutlined, UserOutlined, ShopOutlined, SolutionOutlined, ProfileOutlined, WalletOutlined, ShoppingCartOutlined } from '@ant-design/icons';
import { Switch, Link } from 'react-router-dom'
import CadastroCliente from '../../Pages/CadastroCliente/';
import CadastroTipoContato from '../../Pages/CadastroTipoContato';
import PrivateRoute from '../../Routes/PrivateRoutes'
import ListaClientes from '../../Pages/ListaClientes';
import CadastroEspaco from '../../Pages/CadastroEspaco';
import ListaEspacos from '../../Pages/ListaEspacos';
import CadastroPacote from '../../Pages/CadastroPacote';
import ListaPacotes from '../../Pages/ListaPacotes';
import ContratacaoPacote from '../../Pages/ContratacaoPacote';
import ListaContratacoesPacotes from '../../Pages/ListaContratacoesPacotes';
import ContratacaoEspaco from '../../Pages/ContratacaoEspaco';
import ListaContratacoesEspaco from '../../Pages/ListaContratacoesEspaco';
import Pagamento from '../../Pages/Pagamento';
import ListaSaldo from '../../Pages/ListaSaldo';
import AcessoEspacos from '../../Pages/AcessoEspacos/AcessoEspacos';

const { SubMenu } = Menu;
const { Sider } = Layout;

const MainUI = () => {
  return (
    <Layout>
      <Layout>

        <PageHeader style={{ backgroundColor: "#ffffff", border: "1px solid rgb(235, 237, 240)" }}
          title="Coworking do Iagod"
          extra={[<Link to='/home'><Button type="primary" >Home</Button></Link>]} />

      </Layout>
      <Layout>
        <Sider className="site-layout-background" width={200} style={{ height: '100vh' }}>
          <Menu
            mode="inline"
            defaultSelectedKeys={['0']}
            defaultOpenKeys={['']}
            style={{ height: '100%' }}>
            <SubMenu key="sub1" icon={<UserOutlined />} title="Cliente">
              <Menu.Item key="1"><Link to="/home/cadastrarCliente">Cadastrar Cliente</Link></Menu.Item>
              <Menu.Item key="2"><Link to="/home/listarClientes">Listar Clientes</Link></Menu.Item>
              <Menu.Item key="3"><Link to="/home/cadastrarTipoContato">Cadastrar Tipo de Contatos</Link></Menu.Item>
            </SubMenu>
            <SubMenu key="sub2" icon={<ShopOutlined />} title="Espacos">
              <Menu.Item key="4"><Link to="/home/cadastrarEspaco">Cadastrar Espaco</Link></Menu.Item>
              <Menu.Item key="5"><Link to="/home/listarEspacos">Listar Espacos</Link></Menu.Item>
            </SubMenu>
            <SubMenu key="sub3" icon={<ProfileOutlined />} title="Pacotes">
              <Menu.Item key="6"><Link to="/home/cadastrarPacote">Cadastrar Pacote</Link></Menu.Item>
              <Menu.Item key="7"><Link to="/home/listarPacotes">Listar Pacotes</Link></Menu.Item>
            </SubMenu>
            <SubMenu key="sub4" icon={<SolutionOutlined />} title="Contratacoes">
              <Menu.Item key="8"><Link to="/home/contratarPacote">Contratar Pacote</Link></Menu.Item>
              <Menu.Item key="9"><Link to="/home/listarContratacaoPacote">Pacotes Contratados</Link></Menu.Item>
              <Menu.Item key="10"><Link to="/home/contratarEspaco">Contratar Espaco</Link></Menu.Item>
              <Menu.Item key="11"><Link to="/home/listarContratacaoEspaco" >Espacos Contratados</Link></Menu.Item>
            </SubMenu>
            <SubMenu key="sub5" icon={ <ShoppingCartOutlined />} title="Pagamentos">
              <Menu.Item key="12"><Link to="/home/pagamento">Registrar Pagamento</Link></Menu.Item>
            </SubMenu>
            <SubMenu key="sub6" icon={<WalletOutlined />} title="Saldos">
              <Menu.Item key="13"><Link to="/home/listarSaldo">Listar Saldos</Link></Menu.Item>
            </SubMenu>
            <SubMenu key="sub7" icon={<ScheduleOutlined />} title="Acesso">
              <Menu.Item key="13"><Link to="/home/acesso">Acessos</Link></Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout.Content style={{ height: '100vh' }}>
          <Switch>
            <PrivateRoute path='/home/cadastrarCliente' component={CadastroCliente} />
            <PrivateRoute path='/home/cadastrarTipoContato' component={CadastroTipoContato} />
            <PrivateRoute path='/home/listarClientes' component={ListaClientes} />
            <PrivateRoute path='/home/cadastrarEspaco' component={CadastroEspaco} />
            <PrivateRoute path='/home/listarEspacos' component={ListaEspacos} />
            <PrivateRoute path='/home/cadastrarPacote' component={CadastroPacote} />
            <PrivateRoute path='/home/listarPacotes' component={ListaPacotes} />
            <PrivateRoute path='/home/contratarPacote' component={ContratacaoPacote} />
            <PrivateRoute path='/home/listarContratacaoPacote' component={ListaContratacoesPacotes} />
            <PrivateRoute path='/home/contratarEspaco' component={ContratacaoEspaco} />
            <PrivateRoute path='/home/listarContratacaoEspaco' component={ListaContratacoesEspaco} />
            <PrivateRoute path='/home/pagamento' component={Pagamento} />
            <PrivateRoute path='/home/listarSaldo' component={ListaSaldo} />
            <PrivateRoute path='/home/acesso' component={AcessoEspacos} />
          </Switch>
        </Layout.Content>
      </Layout>
    </Layout>
  )
}

export default MainUI;