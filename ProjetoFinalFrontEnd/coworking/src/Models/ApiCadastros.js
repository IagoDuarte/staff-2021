import axios from "axios";

const url = "http://localhost:8080";

const token = localStorage.getItem('userToken');
const config = { headers: { Authorization : token} }

export default class ApiCadastros {

  cadastrarTipoContato( { nome } ){
    return axios.post( `${url}/cw/tipoContato/salvar`, { nome }, config );
  }

  cadastrarCliente( {nome, cpf, dataNascimento, contatos} ){
    return axios.post(`${url}/cw/cliente/salvar`, {nome, cpf, dataNascimento, contatos}, config);
  }

  cadastrarEspaco({ nome, qntPessoas, valor }){
    return axios.post(`${url}/cw/espaco/salvar`, {nome, qntPessoas, valor}, config);
  }

  cadastrarPacote({ valor, espacoPacote, quantidade }){
    return axios.post(`${url}/cw/pacote/salvar`, {valor, espacoPacote}, config);
  }

  cadastrarClientePacote(clientePacote){
    return axios.post(`${url}/cw/clientePacote/salvar`, clientePacote, config);
  }

  cadastrarContratacao(contratacao){
    return axios.post(`${url}/cw/contratacao/salvar`, contratacao, config);
  }

  cadastrarPagamento(pagamento){
    return axios.post(`${url}/cw/pagamento/pagar`, pagamento, config);
  }

  entrada(entrada){
    return axios.post(`${url}/cw/acesso/entrar`, entrada, config);
  }

  saida(saida){
    return axios.post(`${url}/cw/acesso/sair`, saida, config);
  }

}