export default class Pacote {
  constructor( valor, espacoPacote = [], quantidade, id){
    this.valor = valor;
    this.espacoPacote = espacoPacote;
    this.quantidade = quantidade;
    this.id = id ? id : '';
  }
}