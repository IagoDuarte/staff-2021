import axios from "axios";

const url = "http://localhost:8080";

const token = localStorage.getItem('userToken');
const config = { headers: { Authorization : token} }

export default class ApiBuscas {

  buscarTodosTipoContato( ){
    return axios.get( `${url}/cw/tipoContato/`,  config );
  }

  buscarTodosClientes(){
    return axios.get(`${url}/cw/cliente/`, config);
  }

  buscarTodosEspacos(){
    return axios.get(`${url}/cw/espaco/`, config);
  }

  buscarTodosPacotes(){
    return axios.get(`${url}/cw/pacote/`, config);
  }

  buscarTodosClientesPacotes(){
    return axios.get(`${url}/cw/clientePacote/`, config);
  }

  buscarTodasContratacoes(){
    return axios.get(`${url}/cw/contratacao/`, config);
  }

  buscarTodosSaldos(){
    return axios.get(`${url}/cw/saldo/`, config);
  }



}