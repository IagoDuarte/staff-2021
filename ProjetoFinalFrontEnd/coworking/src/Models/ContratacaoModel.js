export default class ContratacaoModel{
  constructor( espaco, cliente, tipoContratacao, desconto, quantidade, prazo, id ){
    this.espaco = espaco;
    this.cliente = cliente;
    this.tipoContratacao = tipoContratacao;
    this.desconto = desconto;
    this.quantidade = quantidade;
    this.prazo = prazo;
    this.id = id ? id : '';
  }
}