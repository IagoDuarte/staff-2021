export default class ClientePacoteModel{
  constructor(cliente, pacote, quantidade, id){
    this.cliente = cliente;
    this.pacote = pacote;
    this.quantidade = quantidade;
    this.id = id ? id : "";
  }
}