import axios from "axios";

const url = "http://localhost:8080";

export default class Api {

  cadastrarUsuario( { nome, email, login, senha } ){
    const response =  axios.post(`${ url }/cadastrar`, { nome, email, login, senha })
    return response;
  }

  autenticarUsuario( { login, senha } ){
    return axios.post( `${url}/login`, { login, senha } ).then( res => localStorage.setItem('userToken', res.headers.authorization));
  }

}