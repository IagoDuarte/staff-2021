export default class SaldoModel{
  constructor( nomeCliente, nomeEspaco, quantidade, tipoContratacao, vencimento, id_cliente, id_espaco ){
    this.nomeCliente = nomeCliente;
    this.nomeEspaco = nomeEspaco;
    this.quantidade = quantidade;
    this.tipoContratacao = tipoContratacao;
    this.vencimento = vencimento;
    this.id_cliente = id_cliente ? id_cliente : '';
    this.id_espaco = id_espaco ? id_espaco : '';
  }
}