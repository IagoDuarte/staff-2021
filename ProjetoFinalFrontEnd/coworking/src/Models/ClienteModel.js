export default class ClienteModel{
  constructor( nome, cpf, dataNascimento, contatos = [], id){
    this.nome = nome;
    this.cpf = cpf;
    this.dataNascimento = dataNascimento;
    this.contatos = contatos;
    this.id = id ? id : '';
  }

}