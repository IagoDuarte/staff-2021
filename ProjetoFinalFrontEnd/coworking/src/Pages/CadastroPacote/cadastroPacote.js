import React, { Component } from "react";
import { Typography, Layout, Row, Col } from "antd";
import camposTabela from "../../Constants/camposTabela";
import ApiBuscas from "../../Models/ApiBuscas";
import EspacoModel from "../../Models/EspacoModel";
import TabelaUI from "../../Components/TabelaUI";
import FormCadastro from "../../Components/FormCadastro"
import EspacoPacoteModel from "../../Models/EspacoPacoteModel";
import PacoteModel from "../../Models/PacoteModel"
import ApiCadastros from "../../Models/ApiCadastros";

export default class CadastroPacote extends Component {
  constructor(props) {
    super(props);
    this.apiBusca = new ApiBuscas();
    this.apiCadastros = new ApiCadastros();
    this.colunasEspacos = camposTabela.ESPACO.COLUNAS;
    this.colunasEspacoPacote = camposTabela.ESPACOPACOTE.COLUNAS;
    this.state = {
      mostrarFormulario: false,
      mostrarTabelaEspacoPacote: true,
      espacosPacotesInseridos: [],
      espacosInseridos: []
    }
    this.camposPacote = [{ nome: "valor", label: "Valor do Pacote", tipo: "text" },
      {nome: "quantidade", label: "Quantidade do Pacote", tipo: "text"}]
    this.camposEspacoPacote = [
      { nome: "tipoContratacao", label: "Tipo Contratacao", tipo: "text" },
      { nome: "quantidade", label: "Quantidade", tipo: "text" },
      { nome: "prazo", label: "Prazo", tipo: "text" }];
  }


  componentDidMount() {
    this.apiBusca.buscarTodosEspacos().then(response => {
      let listaEspacos = response.data.map(
        espaco => new EspacoModel(espaco.nome, espaco.qntPessoas, espaco.valor, espaco.id));
      this.setState(state => {
        return {
          ...state,
          listaEspacos: listaEspacos.map(espaco => { return { ...espaco, key: espaco.id } })
        }
      })
    });
  }

  setarCamposSelecionados(selectedRowKeys, selectedRows) {
    this.setState(state => {
      return {
        ...state,
        espacoAtual: selectedRows[0],
        idEspacoAtual: selectedRowKeys.map(key => { return { id: key } })[0],
        mostrarFormulario: true
      }
    })
  }

  cadastrarEspacoPacote(value) {
    const { espacosPacotesInseridos, espacosInseridos, espacoAtual } = this.state;
    const espacoPacote = new EspacoPacoteModel(espacoAtual, value.tipoContratacao, value.quantidade, value.prazo);
    espacosInseridos.push(espacoAtual);
    espacosPacotesInseridos.push(espacoPacote);
    this.setState(state => {
      return {
        ...state,
        espacosPacotesInseridos: espacosPacotesInseridos,
        espacosInseridos: espacosInseridos,
        mostrarTabelaEspacoPacote: true
      }
    })
  }

  formatarListaEspacoPacote(listaEspacoPacote) {
    return listaEspacoPacote.map(ep => { return { ...ep, nome: ep.espaco.nome } })
  }

  cadastrarPacote(value){
    const { espacosPacotesInseridos } = this.state;
    const listaFormatada = espacosPacotesInseridos.map(ep => {
      let id = {id: ep.espaco.id}
       return {
          ...ep, espaco: id
        } 
      });
    const espacosPacotes = listaFormatada.map(ep => new EspacoPacoteModel(ep.espaco, ep.tipoContratacao, ep.quantidade, ep.prazo));
    const pacote = new PacoteModel( value.valor, espacosPacotes, value.quantidade );
    this.apiCadastros.cadastrarPacote(pacote).then(
      response => {}
    )

  }

  render() {
    const { listaEspacos, mostrarFormulario, espacoAtual, espacosPacotesInseridos } = this.state;
    return (
      !listaEspacos ? '' :
        <Layout style={{ marginTop: '30px' }}>
          <Row type="flex" align="left" justify="space-around">
            <Col span={10}>
              <Typography.Title level={3}>Selecione o Espaco</Typography.Title>
              <TabelaUI lista={listaEspacos} seletiva evento={this.setarCamposSelecionados.bind(this)} colunas={this.colunasEspacos} />
            </Col>
            <Col span={10} >
              {mostrarFormulario && (
                <React.Fragment>
                  <Typography.Title level={3}>Adicionar <span style={{ color: "#8c8c8c" }}>{espacoAtual.nome}</span> ao pacote.</Typography.Title>
                  <FormCadastro campos={this.camposEspacoPacote} evento={this.cadastrarEspacoPacote.bind(this)} />
                </React.Fragment>
              )
              }
            </Col>
          </Row>
            <Row type="flex" align="left" justify="space-around" style={{marginTop:'20px'}}>
              <Col span={24} align="center">
                <Typography.Title level={3}>Revisar e cadastrar pacote</Typography.Title>
                <Row type="flex" align="left" justify="space-around">
                  <Col span={10} >
                    <TabelaUI lista={this.formatarListaEspacoPacote(espacosPacotesInseridos)} colunas={this.colunasEspacoPacote} />
                  </Col>
                  <Col span={10} >
                    <FormCadastro campos={this.camposPacote} evento={this.cadastrarPacote.bind(this)} />
                  </Col>
                </Row>
              </Col>
            </Row>
        </Layout>
    )
  }

}