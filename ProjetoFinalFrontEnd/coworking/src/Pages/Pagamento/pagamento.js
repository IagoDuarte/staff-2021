import React, { Component } from 'react';
import { Row, Col, Button } from 'antd';
import TabelaUI from '../../Components/TabelaUI';
import Layout from 'antd/lib/layout/layout';
import ApiBuscas from '../../Models/ApiBuscas';
import ApiCadastros from '../../Models/ApiCadastros';
import ClientePacoteModel from '../../Models/ClientePacoteModel';
import Campos from '../../Constants/camposTabela';
import ContratacaoModel from '../../Models/ContratacaoModel';
import FormCadastro from '../../Components/FormCadastro';
import PagamentoModel from '../../Models/PagamentoModel';

export default class Pagamento extends Component {
  constructor(props) {
    super(props)
    this.apiBuscas = new ApiBuscas();
    this.apiCadastros = new ApiCadastros();
    this.colunasClientePacote = Campos.CLIENTEPACOTE.COLUNAS;
    this.subColunasClientePacote = Campos.PACOTES.SUBCOLUNAS;

    this.colunasEspaco = Campos.CONTRATACAO.COLUNAS;
    this.subColunasEspaco = Campos.CONTRATACAO.SUBCOLUNAS;

    this.state = {
      mensagemResposta: '',
      pagina: null
    }

    this.camposForm = [{ nome: "tipoPagamento", label: "Tipo do Pagamento", tipo: "text" }]
  }

  componentDidMount() {
    this.apiBuscas.buscarTodosClientesPacotes().then(response => {
      this.listaClientePacote = response.data.map(cp => new ClientePacoteModel(cp.cliente, cp.pacote, cp.quantidade, cp.id));
      this.setState(state => {
        return {
          ...state,
          listaClientePacote: this.listaClientePacote,
        }
      })
    });

    this.apiBuscas.buscarTodosSaldos().then(response => console.log(response));

    this.apiBuscas.buscarTodasContratacoes().then(response => {
      this.listaContratacao = response.data.map(
        cont => new ContratacaoModel(
          cont.espaco,
          cont.cliente,
          cont.tipoContratacao,
          cont.desconto,
          cont.quantidade,
          cont.prazo,
          cont.id));

      this.setState(state => {
        return {
          ...state,
          listaContratacao: this.listaContratacao,
        }
      })
    });
  }

  formatarClientePacote() {
    const dados = this.listaClientePacote.map(cp => {
      return {
        idPacote: cp.pacote.id,
        qtdEspacos: cp.pacote.espacoPacote.length,
        nomeCliente: cp.cliente.nome,
        cpfCliente: cp.cliente.cpf,
        valor: cp.pacote.valor,
        key: cp.id,
        espacos: cp.pacote.espacoPacote,
        id: cp.id
      }
    })
    return dados;
  }

  formatarPacote() {
    return this.listaContratacao.map(cont => {
      return {
        idContratacao: cont.id,
        key: cont.id,
        nomeEspaco: cont.espaco.nome,
        qntPessoas: cont.espaco.qntPessoas,
        nomeCliente: cont.cliente.nome,
        cpfCliente: cont.cliente.cpf,
        valor: cont.espaco.valor,
        tipoContratacao: cont.tipoContratacao,
        quantidade: cont.quantidade,
        prazo: cont.prazo,
        desconto: cont.desconto
      }
    })
  }

  setarContratacao(selectedRowKeys, row) {
    console.log(row[0])
    const { pagina } = this.state;
    row = row[0];
    if (pagina === "espaco") {
      let contratacaoAtual = new ContratacaoModel(row.espaco, row.cliente, row.tipoContratacao, row.desconto, row.quantidade, row.prazo, { id: row.idContratacao })
      this.setState(state => {
        return {
          ...state,
          contratacaoAtual: contratacaoAtual,
          mostrarTelaPagamento: true,
        }
      })
    } else {
      let contratacaoAtual = new ClientePacoteModel(row.cliente, row.idPacote, row.quantidade, { id: row.id });
      this.setState(state => {
        return {
          ...state,
          contratacaoAtual: contratacaoAtual,
          mostrarTelaPagamento: true,
        }
      })
    }
  }

  registrarPagamento(values) {
    const { contratacaoAtual, pagina } = this.state;
    let pagamento;
    if (pagina === "pacote") {
      pagamento = new PagamentoModel(values.tipoPagamento, null, contratacaoAtual.id)
    } else {
      pagamento = new PagamentoModel(values.tipoPagamento, contratacaoAtual.id, null)
    }
    this.apiCadastros.cadastrarPagamento(pagamento).then(response =>
      this.setState(state => {
        console.log(response)
        return {
          ...state,
          mensagemResposta: "Pagamento realizado com sucesso!"
        }
      })
    ).catch(
      this.setState(state => {
        return {
          ...state,
          mensagemResposta: "Erro ao realizar pagamento!"
        }
      })
    )
  }

  render() {
    return (
      <Layout>
        <Row type="flex" align="middle" justify="center" style={{ marginTop: '30px' }}>
          <Col span={4} style={{ textAlign: 'center' }}>
            <Button onClick={() => { this.setState(state => { return { ...state, pagina: "pacote" } }) }} type="primary">Pagamento de pacotes</Button>
          </Col>
          <Col span={4} style={{ textAlign: 'center' }}>
            <Button type="primary" onClick={() => { this.setState(state => { return { ...state, pagina: "espaco" } }) }}>Pagamento de espacos</Button>
          </Col>
        </Row>
        <Row type="flex" align="middle" justify="center" style={{ marginTop: '10px' }}>
          <Col span={16}>
            {this.state.pagina === "espaco" ?
              <TabelaUI evento={this.setarContratacao.bind(this)} expansiva seletiva model="pacote" lista={this.formatarPacote()} colunas={this.colunasEspaco} subColunas={this.subColunasEspaco} > </TabelaUI>
              : this.state.pagina === "pacote" ?
                <TabelaUI evento={this.setarContratacao.bind(this)} expansiva seletiva model="clientePacote" lista={this.formatarClientePacote()} colunas={this.colunasClientePacote} subColunas={this.subColunasClientePacote} />
                : null
            }
          </Col>
        </Row>
        <Row type="flex" align="middle" justify="center" >
          <Col span={16}>
            <p> {this.state.mensagemResposta} </p>
            {this.state.mostrarTelaPagamento ?
              <FormCadastro evento={this.registrarPagamento.bind(this)} campos={this.camposForm} ></FormCadastro>
              : null
            }
          </Col>
        </Row>
      </Layout>
    )
  }
}