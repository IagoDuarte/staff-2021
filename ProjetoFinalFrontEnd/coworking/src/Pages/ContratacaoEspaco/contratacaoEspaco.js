import React, { Component } from "react";
import ApiBuscas from "../../Models/ApiBuscas";
import EspacoModel from "../../Models/EspacoModel"
import ClienteModel from "../../Models/ClienteModel";
import { Col, Row, Typography } from "antd";
import TabelaUI from "../../Components/TabelaUI";
import Campos from "../../Constants/camposTabela";
import FormCadastro from "../../Components/FormCadastro";
import ApiCadastros from "../../Models/ApiCadastros";
import ContratacaoModel from "../../Models/ContratacaoModel";

export default class ContratacaoEspaco extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mostrarTabelaEspacos:false,
      mensagemReposta:""
    }
    this.apiBuscas = new ApiBuscas();
    this.apiCadastros = new ApiCadastros();
    this.listaClientes = []
    this.listaEspacos = []
    this.colunasCliente = Campos.CLIENTE.COLUNAS;
    this.colunasEspaco = Campos.ESPACO.COLUNAS;
    this.colunasContratacao = Campos.CONTRATACAO.COLUNAS;
    this.camposForm = [{nome:"tipoContratacao", label:"Tipo da Contratacao", tipo:"text"},
    {nome:"quantidade", label:"Quantidade", tipo:"number"},
    {nome:"desconto", label:"Desconto (Opcional)", tipo:"number"},
    {nome:"prazo", label:"Prazo", tipo:"number"}]
  }

  componentDidMount() {
    this.apiBuscas.buscarTodosClientes().then(response => {
      const lista = response.data.map(cliente => new ClienteModel(cliente.nome, cliente.cpf, cliente.dataNascimento, cliente.contatos, cliente.id))
      this.listaClientes = lista.map(cliente => { return { ...cliente, key: cliente.id } });
      this.setState(state => {
        return {
          ...state,
          listaClientes: this.listaClientes
        }
      })
    })

    this.apiBuscas.buscarTodosEspacos().then(response => {
      const lista = response.data.map(espaco => new EspacoModel(espaco.nome, espaco.qntPessoas, espaco.valor, espaco.id));
      this.listaEspacos = lista.map(e => {
         return { ...e, key: e.id } 
        });
      this.setState(state => {
        return {
          ...state,
          listaEspacos: this.listaEspacos
        }
      })
    })
  }

  setarClienteSelecionado(selectedRowKeys, selectedRows) {
    this.setState(state => {
      return {
        ...state,
        clienteAtual: selectedRows[0],
        idClienteAtual: selectedRowKeys.map(key => { return { id: key } })[0],
        mostrarTabelaEspacos: true
      }
    })
  }

  setarEspacoSelecionado(selectedRowKeys, selectedRows) {
    this.setState(state => {
      return {
        ...state,
        espacoAtual: selectedRows[0],
        idEspacoAtual: selectedRows.map( row => { return { id: row.id } })[0],
        mostrarTabelaEspacos: false
      }
    })
  }

  contratacaoAtual() {
    const { espacoAtual, clienteAtual } = this.state;
    const dados = [{
      nomeEspaco: espacoAtual.nome,
      qntPessoas: espacoAtual.qntPessoas,
      nomeCliente: clienteAtual.nome,
      cpfCliente: clienteAtual.cpf,
      valor: espacoAtual.valor
    }]
    return dados;
  }

  cadastrarContratacao(values){
    const { idEspacoAtual, idClienteAtual } = this.state;
    const contratacao = new ContratacaoModel(idEspacoAtual, idClienteAtual, values.tipoContratacao, values.desconto, values.quantidade, values.prazo);
    this.apiCadastros.cadastrarContratacao(contratacao).then( response =>
      this.setState( state => {
        return {
          ...state,
          espacoAtual:null,
          clienteAtual:null,
          mensagemReposta: "Contratacao registrada!"
        }
      } )).catch(
        this.setState( state => {
          return {
            ...state,
            mensagemReposta: "Erro! Revise os campos"
          }
        } )
      )
  }

  render() {
    const { listaClientes, listaEspacos, mostrarTabelaEspacos, espacoAtual, clienteAtual } = this.state;
    return (
      <Row type="flex" align="left" justify="space-around" style={{ marginTop: '20px' }}>
        <Col span={16}>
          <p> { this.state.mensagemReposta } </p>
          {
            clienteAtual && espacoAtual ?
            <React.Fragment>
              <Typography.Title level={4}>Confirmar Contratacao</Typography.Title>
              <TabelaUI lista={this.contratacaoAtual()} colunas={this.colunasContratacao} />
              <FormCadastro evento={this.cadastrarContratacao.bind(this)} campos={this.camposForm} ></FormCadastro>
            </React.Fragment>
           : mostrarTabelaEspacos ?
            <React.Fragment>
              <Typography.Title level={4}>Selecione um Espaco</Typography.Title>
              <TabelaUI evento={this.setarEspacoSelecionado.bind(this)} seletiva lista={listaEspacos} colunas={this.colunasEspaco}></TabelaUI>
            </React.Fragment>
            :
            <React.Fragment>
              <Typography.Title level={4}>Selecione um Cliente</Typography.Title>
              <TabelaUI evento={this.setarClienteSelecionado.bind(this)} seletiva lista={listaClientes} colunas={this.colunasCliente} ></TabelaUI>
            </React.Fragment>
          }
        </Col>
      </Row>
    )
  }
}