import { Button, Col, Row, Typography } from "antd";
import React, { Component } from "react";
import TabelaUI from "../../Components/TabelaUI";
import Campos from "../../Constants/camposTabela";
import ApiBuscas from "../../Models/ApiBuscas";
import ApiCadastros from "../../Models/ApiCadastros";
import ClienteModel from "../../Models/ClienteModel";
import SaldoModel from "../../Models/SaldoModel";

export default class AcessoEspacos extends Component {
  constructor(props) {
    super(props);
    this.apiBuscas = new ApiBuscas();
    this.apiCadastros = new ApiCadastros();
    this.colunasSaldo = Campos.SALDO.COLUNAS;
    this.listaSaldo = []
    this.colunasCliente = Campos.CLIENTE.COLUNAS;
    this.state = {}
  }

  componentDidMount() {
    this.apiBuscas.buscarTodosSaldos().then(response => {
      const lista = response.data.map(saldo => new SaldoModel(saldo.cliente.nome, saldo.espaco.nome, saldo.quantidade, saldo.tipoContratacao, saldo.vencimento, saldo.id_cliente, saldo.id_espaco))
      this.listaSaldo = lista.map(saldo => {
        let cont = 0;
        return { ...saldo, key: cont++ }
      })
      this.setState(state => {
        return {
          ...state,
          listaSaldo: this.listaSaldo
        }
      })
    }
    )

    this.apiBuscas.buscarTodosClientes().then(response => {
      const lista = response.data.map(cliente => new ClienteModel(cliente.nome, cliente.cpf, cliente.dataNascimento, cliente.contatos, cliente.id))
      this.listaClientes = lista.map(cliente => { return { ...cliente, key: cliente.id } });
      this.setState(state => {
        return {
          ...state,
          listaClientes: this.listaClientes
        }
      })
    })
  }

  setarClienteSelecionado(selectedRowKeys, selectedRows) {
    this.setState(state => {
      return {
        ...state,
        clienteAtual: selectedRows[0],
        idClienteAtual: selectedRowKeys.map(key => { return { id: key } })[0],
        mostrarTelaSaldos: true
      }
    })
  }

  filtrarLista() {
    const { clienteAtual } = this.state;
    console.log(clienteAtual)
    return this.listaSaldo.filter(saldo => saldo.nomeCliente === clienteAtual.nome);
  }

  setarSaldoSelecionado(selectedRowKeys, selectedRows) {
    this.setState(state => {
      return {
        ...state,
        saldoAtual: selectedRows[0],
        mostrarTelaSaldos: false,
        mostrarBotoes: true
      }
    })
  }

  saldoAtual() {
    const { saldoAtual } = this.state;
    return [saldoAtual];
  }

  acesso(tipo) {
    const { saldoAtual } = this.state;
    let registro = {
      saldoCliente: {
        id_cliente: saldoAtual.id_cliente,
        id_espaco: saldoAtual.id_espaco
      }
    }
    if (tipo === "entrar") {
      this.apiCadastros.entrada(registro).then(response => {

        this.setState(state => {
          return {
            ...state,
            mostrarTelaSaldos: false,
            mostrarBotoes: false,
            mensagemRetorno: "Entrada realizada com sucesso!"
          }
        })
      });
    }

    if (tipo === "saida") {
      this.apiCadastros.saida(registro).then(response => {
        this.setState(state => {
          return {
            ...state,
            mostrarTelaSaldos: false,
            mostrarBotoes: false,
            mensagemRetorno: "Saida realizada com sucesso!"
          }
        })
      })
    }
  }

  render() {
    const { listaClientes, mostrarTelaSaldos, mostrarBotoes } = this.state;
    return (
      <Row type="flex" justify="center" style={{ marginTop: '30px' }}>
        <Col span={16}>
          {mostrarBotoes ?
            <React.Fragment>
              <Row type="flex" justify="center">
                <Col span={24}>
                  <TabelaUI lista={this.saldoAtual()} colunas={this.colunasSaldo} ></TabelaUI>
                </Col>
              </Row>
              <Row type="flex" justify="space-around" align="middle">
                <Col span={4} >
                  <Button onClick={() => this.acesso("entrar")} type="primary" >Entrar</Button>
                </Col>
                <Col span={4}>
                  <Button onClick={() => this.acesso("saida")} type="primary" >Sair</Button>
                </Col>
              </Row>
            </React.Fragment>
            : !mostrarTelaSaldos ?
              <React.Fragment>
                <Typography.Title level={4}>Selecione um Cliente</Typography.Title>
                <TabelaUI seletiva evento={this.setarClienteSelecionado.bind(this)} lista={listaClientes} colunas={this.colunasCliente} ></TabelaUI>
              </React.Fragment>
              : <React.Fragment>
                <Typography.Title level={4}>Selecione um Espaco</Typography.Title>
                <TabelaUI seletiva evento={this.setarSaldoSelecionado.bind(this)} colunas={this.colunasSaldo} lista={this.filtrarLista()}></TabelaUI>
              </React.Fragment>
          }
          <Row style={{ textAlign: 'center' }}>
            <p>{this.state.mensagemRetorno}</p>
          </Row>
        </Col>
      </Row >

    )
  }
}