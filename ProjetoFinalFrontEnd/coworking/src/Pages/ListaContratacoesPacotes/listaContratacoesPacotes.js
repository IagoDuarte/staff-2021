import { Component } from "react";
import { Row, Col, Typography } from "antd";
import ApiBuscas from "../../Models/ApiBuscas";
import ClientePacoteModel from "../../Models/ClientePacoteModel";
import TabelaUI from "../../Components/TabelaUI";
import Campos from "../../Constants/camposTabela";
import ContratacaoModel from "../../Models/ContratacaoModel";

export default class ListaContratacoesPacotes extends Component {
  constructor(props) {
    super(props);
    this.apiBuscas = new ApiBuscas();
    this.colunasClientePacote = Campos.CLIENTEPACOTE.COLUNAS;
    this.subColunas = Campos.PACOTES.SUBCOLUNAS;
    this.listaClientePacote = [];
    this.state = {}
  }

  componentDidMount() {
    this.apiBuscas.buscarTodosClientesPacotes().then(response => {
      console.log(response)
      this.listaClientePacote = response.data.map(cp => new ClientePacoteModel(cp.cliente, cp.pacote, cp.quantidade, cp.id));
      this.setState(state => {
        return {
          ...state,
          listaClientePacote: this.listaClientePacote,
        }
      })
    });

    this.apiBuscas.buscarTodasContratacoes().then(response => {
      this.listaContratacao = response.data.map(
        cont => new ContratacaoModel(
          cont.espaco,
          cont.cliente,
          cont.tipoContratacao,
          cont.desconto,
          cont.quantidade,
          cont.prazo,
          cont.id));

      this.setState(state => {
        return {
          ...state,
          listaContratacao: this.listaContratacao,
        }
      })
    });
  }

  formatarClientePacote() {
  return this.listaClientePacote.map(cp => {
      return {
        idPacote: cp.pacote.id,
        qtdEspacos: cp.pacote.espacoPacote.length,
        nomeCliente: cp.cliente.nome,
        cpfCliente: cp.cliente.cpf,
        valor: cp.pacote.valor,
        key: cp.id,
        espacos: cp.pacote.espacoPacote
      }
    })
  }

  formatarPacote() {
    return this.listaContratacao.map(cont => {
      return {
        key: cont.id,
        nomeEspaco: cont.espaco.nome,
        qntPessoas: cont.espaco.qntPessoas,
        nomeCliente: cont.cliente.nome,
        cpfCliente: cont.cliente.cpf,
        valor: cont.espaco.valor,
        tipoContratacao: cont.tipoContratacao,
        quantidade: cont.quantidade,
        prazo: cont.prazo,
        desconto: cont.desconto
      }
    })
  }

  render() {
    return (
      <Row type="flex" align="left" justify="space-around" style={{ marginTop: '20px' }}>
        <Col span={16}>
          <TabelaUI expansiva model="clientePacote" lista={this.formatarClientePacote()} colunas={this.colunasClientePacote} subColunas={this.subColunas} />
        </Col>
      </Row>

    )
  }
}