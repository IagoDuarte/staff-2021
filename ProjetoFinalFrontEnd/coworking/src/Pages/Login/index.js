import { Button } from "antd";
import React from "react";
import { Link } from "react-router-dom";
import { Row, Col } from "../../Components/Antd/Grid";
import FormularioUi from '../../Components/FormularioUi'

export default class Login extends React.Component{

  render(){
    return(
      <Row style={{marginTop:'40px', textAlign:'center'}} type="flex" align="middle" justify="center" >
        <Col>
          <FormularioUi isLogin/>
        </Col>
      </Row>
      )
  }
}