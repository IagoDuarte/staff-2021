import {Layout, Row, Col} from 'antd'
import { useState } from 'react';
import FormCadastro from '../../Components/FormCadastro/FormCadastro';
import ApiCadastros from '../../Models/ApiCadastros'
import TipoContato from '../../Models/TipoContato'

const { Content } = Layout;

const CadastroTipoContato = () => {
  const [respostaRequisicao, setRespostaRequisicao] = useState(false);
  const [tentativa, setTentativa] = useState(0);

  const campos = [ {nome: "nome", label: "Nome", tipo: "text"} ];

  const api = new ApiCadastros();

  const adicionarTipoContato = ( values ) => {
    const tipoContato = new TipoContato( values.nome );
    const requisicao = [ api.cadastrarTipoContato( tipoContato ) ];
    Promise.all( requisicao ).then( response => {
      setRespostaRequisicao(true);
      setTentativa( tentativa + 1);
    } ).catch(
      setRespostaRequisicao(false)
    )
  }
  
  return(
    <Content >
      <Row style={{marginTop:'40px'}} type="flex" justify="center" align="middle" >
        <Col>
        { respostaRequisicao ? <p> Tipo Contato inserido com sucesso! </p> : tentativa > 0 ? <p>Erro!</p> : null}
          <h1>Cadastrar Tipo Contato</h1>
          <FormCadastro campos = {campos} evento = {adicionarTipoContato}/>
        </Col>
      </Row>
    </Content>
  )
}

export default CadastroTipoContato;