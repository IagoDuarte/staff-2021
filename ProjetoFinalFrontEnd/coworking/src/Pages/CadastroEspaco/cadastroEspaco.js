import { Component } from "react";
import { Layout, Row, Col } from 'antd'
import FormCadastro from "../../Components/FormCadastro";
import EspacoModel from "../../Models/EspacoModel"
import ApiCadastros from  "../../Models/ApiCadastros"

export default class CadastroEspaco extends Component {
  constructor(props) {
    super(props);
    this.apiCadastro = new ApiCadastros();
    this.state = {
      campos : [{ nome: "nome", label: "Nome", tipo: "text" },
      { nome: "qntPessoas", label: "Quantidade de Pessoas", tipo: "number" },
      { nome: "valor", label: "Preco", tipo: "text" }]
    }
  }

  cadastrarEspaco( values ){
    const espaco = new EspacoModel( values.nome, values.qntPessoas, values.valor);
    this.apiCadastro.cadastrarEspaco(espaco).then(response => this.setState( state => {
      console.log(response);
      return {
        ...state,
        mensagemCadastro:"Espaco Cadastrado com sucesso!",
      }
    })).catch(
      this.setState( state => {
        return {
          ...state,
          mensagemCadastro:"Digite os campos novamente!",
        }
      })
    )
  }
  
  render() {
    return (
      <Layout >
        <Row style={{ marginTop: '40px' }} type="flex" justify="center" align="middle" >
          <Col style={{ textAlign: 'center' }}>
            <p>{this.state.mensagemCadastro}</p>
            <h1>Cadastrar Espaco</h1>
            <FormCadastro campos={this.state.campos} evento={this.cadastrarEspaco.bind(this)} />
          </Col>
        </Row>
      </Layout>
    )
  }
}