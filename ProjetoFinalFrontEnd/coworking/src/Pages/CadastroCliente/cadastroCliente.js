import {Col, Layout, Row, notification} from 'antd'
import FormCadastro from '../../Components/FormCadastro';
import ApiBuscas from '../../Models/ApiBuscas';
import { Component } from 'react';
import ClienteModel from '../../Models/ClienteModel';
import ApiCadastros from '../../Models/ApiCadastros';

const { Content } = Layout;

export default class CadastroCliente extends Component{
  constructor(props){
    super(props);
    this.apiBuscas = new ApiBuscas();
    this.apiCadastros = new ApiCadastros();

    this.campos =  [{ nome: "nome", label:"Nome" , tipo: "text" },
    { nome: "cpf", label:"CPF" , tipo: "text" },
    { nome: "dataNascimento", label:"Data de Nascimento", tipo: "text" },
    { nome: "contato", label: "Contato", tipo: "text" }];

    this.state = {
      campos: this.campos,
      mensagemCadastro: ""
    }
  }

    componentDidMount(){
        this.apiBuscas.buscarTodosTipoContato().then( response => {
        let opcoes = response.data.map( objeto => {return { value: objeto.id, label : objeto.nome } } )
        this.campos.push( { nome: "tipoContato", label: "Tipo de Contato", tipo: "cascader", opcoes: opcoes } )
        this.campos.push({ nome: "contatoRecado", label: "Contato de Recado", tipo: "text" })
        this.campos.push( { nome: "tipoContatoRecado", label: "Tipo de Contato", tipo: "cascader", opcoes: opcoes } )
        this.setState(state =>{
          return{
            ...state,
            campos: this.campos,
          }
        })
      })
    }

    montarCliente( values ){
      let contatos = [ { contato: values.contato, tipoContato: { id: values.tipoContato[0]} },
        { contato: values.contatoRecado, tipoContato: { id: values.tipoContatoRecado[0]} } ]
      let cliente = new ClienteModel( values.nome, values.cpf, values.dataNascimento,contatos);
      this.apiCadastros.cadastrarCliente(cliente).then(response => this.setState( state => {
        return {
          ...state,
          mensagemCadastro:"Cliente Cadastrado com sucesso!",
        }
      })).catch(
        this.setState( state => {
          return {
            ...state,
            mensagemCadastro:"Digite os campos novamente!",
          }
        })
      )
    }

  
  render(){
    return(
    <Layout >
      <Row style={{marginTop:'40px'}} type="flex" justify="center" align="middle" >
        <Col style={{textAlign: 'center'}}>
          <p>{this.state.mensagemCadastro}</p> 
          <h1>Cadastrar Cliente</h1>
          <FormCadastro campos = {this.state.campos} evento = {this.montarCliente.bind(this)}/>
        </Col>
      </Row>
    </Layout>
    )
  }
}
