import { Row, Col, Input } from "antd";
import { Component } from "react";
import TabelaUI from "../../Components/TabelaUI";
import Campos from "../../Constants/camposTabela";
import ApiBuscas from "../../Models/ApiBuscas";
import SaldoModel from "../../Models/SaldoModel";

export default class ListaSaldo extends Component {
  constructor(props) {
    super(props);
    this.apiBuscas = new ApiBuscas();
    this.colunasTabela = Campos.SALDO.COLUNAS;
    this.listaFiltrada = []
    this.state = {

    }
  }

  componentDidMount() {
    this.apiBuscas.buscarTodosSaldos().then(response => {
      console.log(response)
      this.listaSaldo = response.data.map(saldo => new SaldoModel(saldo.cliente.nome, saldo.espaco.nome, saldo.quantidade, saldo.tipoContratacao, saldo.vencimento))
      console.log(this.listaSaldo)
      this.setState(state => {
        return {
          ...state,
          listaSaldo: this.listaSaldo
        }
      })
    }
    )
  }

  buscaSaldo(value) {
    this.listaFiltrada = this.state.listaSaldo.filter(saldo => {
      return saldo.nomeCliente.toLowerCase().indexOf(value.target.value.toLowerCase()) > -1
    })
    this.setState(state => {
      return {
        ...state,
        listaFiltrada: this.listaFiltrada
      }
    })
  }

  render() {
    const { listaFiltrada } = this.state;
    return (
      <Row type="flex" align="middle" justify="center" style={{ marginTop: '30px' }}>
        <Col span={16}>
          <Row>
            <Input style={{ width: "200px", marginBottom: "10px" }} placeholder="Digite um nome para filtrar" onChange={this.buscaSaldo.bind(this)}></Input>
          </Row>
          <TabelaUI colunas={this.colunasTabela} lista={listaFiltrada ? listaFiltrada.length > 0 ? listaFiltrada : listaFiltrada : this.listaSaldo}></TabelaUI>
        </Col>
      </Row>
    )
  }
}