import React from  'react';
import Api from '../../Models/Api';
import { Row, Col } from "../../Components/Antd/Grid";
import FormularioUi from '../../Components/FormularioUi';

export default class Cadastro extends React.Component{
  constructor( props ){
    super( props )
    this.api = new Api();
    this.state= {
    }
  }
  
  render (){
    return(
      <Row style={{margin:'40px', textAlign:'center'}} type="flex" align="middle" justify="center" >
        <Col>
          <FormularioUi/>
        </Col>
      </Row>
    )
  }
}