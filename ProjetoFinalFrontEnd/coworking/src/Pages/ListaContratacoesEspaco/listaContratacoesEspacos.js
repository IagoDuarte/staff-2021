import React, { Component } from "react";
import ApiBuscas from "../../Models/ApiBuscas";
import ContratacaoModel from "../../Models/ContratacaoModel";
import { Col, Row } from "antd";
import TabelaUI from "../../Components/TabelaUI";
import Campos from "../../Constants/camposTabela";

export default class ListaContratacoesEspacos extends Component {
  constructor(props) {
    super(props);
    this.state = {}
    this.listaContratacao = [];
    this.colunaTabela = Campos.CONTRATACAO.COLUNAS;
    this.subColunaTabela = Campos.CONTRATACAO.SUBCOLUNAS;
    this.apiBuscas = new ApiBuscas();
  }

  componentDidMount() {
    this.apiBuscas.buscarTodasContratacoes().then(response => {
      this.listaContratacao = response.data.map(
        cont => new ContratacaoModel(
          cont.espaco,
          cont.cliente,
          cont.tipoContratacao,
          cont.desconto,
          cont.quantidade,
          cont.prazo,
          cont.id));

      this.setState(state => {
        return {
          ...state,
          listaContratacao: this.listaContratacao,
        }
      })
    });
  }

  formatarLista() {
    return this.listaContratacao.map(cont => {
      return {
        key: cont.id,
        nomeEspaco: cont.espaco.nome,
        qntPessoas: cont.espaco.qntPessoas,
        nomeCliente: cont.cliente.nome,
        cpfCliente: cont.cliente.cpf,
        valor: cont.espaco.valor,
        tipoContratacao: cont.tipoContratacao,
        quantidade: cont.quantidade,
        prazo: cont.prazo,
        desconto: cont.desconto
      }
    })
  }

  render() {
    return (
      <Row type="flex" align="left" justify="space-around" style={{ marginTop: '20px' }}>
        <Col span={16}>
          {
            this.state.listaContratacao ? <TabelaUI expansiva model="contratacao" lista={this.formatarLista()} colunas={this.colunaTabela} subColunas={this.subColunaTabela}></TabelaUI>
              : ''
          }
        </Col>
      </Row>
    )
  }


}