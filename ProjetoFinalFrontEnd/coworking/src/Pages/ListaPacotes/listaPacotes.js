import { Component } from "react";
import camposTabela from "../../Constants/camposTabela";
import ApiBuscas from "../../Models/ApiBuscas";
import PacoteModel from "../../Models/PacoteModel"
import { Layout, Col, Row, Typography} from "antd";
import TabelaUI from "../../Components/TabelaUI";

export default class ListaPacotes extends Component{
  constructor(props){
    super(props);
    this.apiBuscas = new ApiBuscas();
    this.state = {
    }
    this.colunasTabela = camposTabela.PACOTES.COLUNAS;
    this.subColunasTabela = camposTabela.PACOTES.SUBCOLUNAS;
  }

  componentDidMount(){
    this.apiBuscas.buscarTodosPacotes().then(response => {
      let listaPacotes = response.data.map(
        pacote => new PacoteModel(pacote.valor, pacote.espacoPacote, null, pacote.id));
       this.setState(state => {
        return {
          ...state,
          listaPacotes: listaPacotes.map( pacote => {
            const qtdEspacos = pacote.espacoPacote.length;
            return {
              ...pacote,
              qtdEspacos: qtdEspacos,
              key:pacote.id 
            }} )
        }
      })
    });
  }

  render(){
    const { listaPacotes } = this.state;
    return (
      !listaPacotes ? <Typography.Title> Sem Pacotes Registrados </Typography.Title>
      :
      <Layout style= {{marginTop:'30px'}}>
        <Row type="flex" align="middle" justify="center">
          <Col span={23}>
              <TabelaUI model="pacote" colunas={this.colunasTabela} expansiva subColunas={this.subColunasTabela} lista={listaPacotes}/>
          </Col>
        </Row>
      </Layout>
    )
  }
}