import { Col, Row, Typography } from "antd";
import React, { Component } from "react";
import TabelaUI from "../../Components/TabelaUI";
import Campos from "../../Constants/camposTabela";
import ApiBuscas from "../../Models/ApiBuscas";
import ClienteModel from "../../Models/ClienteModel";
import PacoteModel from "../../Models/PacoteModel"
import FormCadastro from "../../Components/FormCadastro"
import ClientePacoteModel from "../../Models/ClientePacoteModel";
import ApiCadastros from "../../Models/ApiCadastros";

export default class ContratacaoPacote extends Component {
  constructor(props) {
    super(props);
    this.apiBuscas = new ApiBuscas();
    this.apiCadastros = new ApiCadastros();
    this.colunasCliente = Campos.CLIENTE.COLUNAS;
    this.subColunasCliente = Campos.CLIENTE.SUBCOLUNAS;
    this.colunasPacote = Campos.PACOTES.COLUNAS;
    this.subColunasPacote = Campos.PACOTES.SUBCOLUNAS;
    this.colunasClientePacote = Campos.CLIENTEPACOTE.COLUNAS;
    this.camposForm = [{ nome: "quantidade", label: "Quantidade", tipo: "text" }]
    this.state = {
      sucesso: false,
      mostrarTabelaPacotes: false
    }
  }


  componentDidMount() {
    this.apiBuscas.buscarTodosClientes().then(response => {
      const lista = response.data.map(cliente => new ClienteModel(cliente.nome, cliente.cpf, cliente.dataNascimento, cliente.contatos, cliente.id))
      this.listaClientes = lista.map(cliente => { return { ...cliente, key: cliente.id } });
      this.setState(state => {
        return {
          ...state,
          listaClientes: this.listaClientes
        }
      })
    })

    this.apiBuscas.buscarTodosPacotes().then(response => {
      const lista = response.data.map(pacote => new PacoteModel(pacote.valor, pacote.espacoPacote, null, pacote.id));
      this.listaPacotes = lista.map(pacote => { return { ...pacote, key: pacote.id, qtdEspacos: pacote.espacoPacote.length } });

      this.setState(state => {
        return {
          ...state,
          listaPacotes: this.listaPacotes
        }
      })
    })
  }

  setarClienteSelecionado(selectedRowKeys, selectedRows) {
    this.setState(state => {
      return {
        ...state,
        clienteAtual: selectedRows[0],
        idClienteAtual: selectedRowKeys.map(key => { return { id: key } })[0],
        mostrarTabelaPacotes: true
      }
    })
  }

  setarPacoteSelecionado(selectedRowKeys, selectedRows) {
    this.setState(state => {
      return {
        ...state,
        pacoteAtual: selectedRows[0],
        idPacoteAtual: selectedRowKeys.map(key => { return { id: key } })[0],
        mostrarTabelaPacotes: false
      }
    })
  }

  clientePacoteAtual() {
    const { pacoteAtual, clienteAtual } = this.state;
    console.log(pacoteAtual)
    const dados = [{
      idPacote: pacoteAtual.id,
      qtdEspacos: pacoteAtual.qtdEspacos,
      nomeCliente: clienteAtual.nome,
      cpfCliente: clienteAtual.cpf,
      valor: pacoteAtual.valor
    }]
    return dados;
  }

  cadastrarEspacoPacote(values) {
    const { idClienteAtual, idPacoteAtual } = this.state;
    const clientePacote = new ClientePacoteModel(idClienteAtual, idPacoteAtual, values.quantidade);
    this.apiCadastros.cadastrarClientePacote(clientePacote).then(response =>
      this.setState(state => {
        return {
          ...state,
          sucesso: true,
          clienteAtual: null,
          pacoteAtual: null,
        }
      }))
  }

  render() {
    const { listaPacotes, listaClientes, mostrarTabelaPacotes, clienteAtual, pacoteAtual } = this.state;
    return (
      <Row type="flex" align="left" justify="space-around" style={{ marginTop: '20px' }}>
        <Col span={16}>
          <p>{this.state.sucesso ? "Pacote Contratado!" : ""}</p>
          {clienteAtual && pacoteAtual ?
            <React.Fragment>
              <Typography.Title level={4}>Confirmar Contratacao</Typography.Title>
              <TabelaUI lista={this.clientePacoteAtual()} colunas={this.colunasClientePacote} />
              <FormCadastro evento={this.cadastrarEspacoPacote.bind(this)} campos={this.camposForm} ></FormCadastro>
            </React.Fragment>
            : mostrarTabelaPacotes ?
              <React.Fragment>
                <Typography.Title level={4}>Selecione um Pacote</Typography.Title>
                <TabelaUI evento={this.setarPacoteSelecionado.bind(this)} model="pacote" seletiva expansiva lista={listaPacotes} colunas={this.colunasPacote} subColunas={this.subColunasPacote} ></TabelaUI>
              </React.Fragment>
              :
              <React.Fragment>
                <Typography.Title level={4}>Selecione um Cliente</Typography.Title>
                <TabelaUI evento={this.setarClienteSelecionado.bind(this)} model="cliente" seletiva expansiva lista={listaClientes} colunas={this.colunasCliente} subColunas={this.subColunasCliente} ></TabelaUI>
              </React.Fragment>
          }
        </Col>
      </Row>
    )
  }
}