import React, { Component } from "react";
import ApiBuscas from "../../Models/ApiBuscas";
import ClienteModel from "../../Models/ClienteModel";
import TabelaUI from "../../Components/TabelaUI"
import { Row, Col } from "antd";
import Layout from "antd/lib/layout/layout";
import camposTabela from  "../../Constants/camposTabela"

export default class ListaClientes extends Component{
  constructor(props){
    super(props);
    this.apiBuscas = new ApiBuscas();
    this.state = {
    }

    this.colunasTabela = camposTabela.CLIENTE.COLUNAS;
    this.subColunasTabela = camposTabela.CLIENTE.SUBCOLUNAS;
  }

  adicionarKeysParaTabela(listaClientes){
    let cont = 0;
    return listaClientes.map(cliente => { return { ...cliente, key: cont++}})
  }

  componentDidMount(){
    this.apiBuscas.buscarTodosClientes().then(response => {
      let listaClientes = response.data.map(
        cliente => new ClienteModel(cliente.nome, cliente.cpf, cliente.dataNascimento, cliente.contatos));
       this.setState(state => {
        return {
          ...state,
          listaClientes: this.adicionarKeysParaTabela(listaClientes)
        }
      })
    });
  }
  
  render(){
    const { listaClientes } = this.state;
    return (
      !listaClientes ? <h1> Sem Clientes Registrados </h1>
      :
      <Layout style= {{marginTop:'30px'}}>
        <Row type="flex" align="middle" justify="center">
          <Col span={23}>
              <TabelaUI model="cliente" colunas={this.colunasTabela} expansiva subColunas={this.subColunasTabela} lista={listaClientes}/>
          </Col>
        </Row>
      </Layout>
    )
  }
}