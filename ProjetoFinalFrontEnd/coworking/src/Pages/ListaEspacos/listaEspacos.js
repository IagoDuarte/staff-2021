import { Component } from "react";
import ApiBuscas from "../../Models/ApiBuscas";
import EspacoModel from "../../Models/EspacoModel";
import { Layout, Row, Col, Input } from "antd";
import TabelaUI from "../../Components/TabelaUI";
import camposTabela from "../../Constants/camposTabela"

export default class ListaEspacos extends Component{
  constructor(props) {
    super(props);
    this.state = {
      listaEspacos: [],
      listaFiltrada: []
    }
    this.apiBusca = new ApiBuscas();
    this.colunasTabela = camposTabela.ESPACO.COLUNAS;
  }

  componentDidMount() {
    this.apiBusca.buscarTodosEspacos().then(response => {
      let listaEspacos = response.data.map(
        espaco => new EspacoModel(espaco.nome, espaco.qntPessoas, espaco.valor, espaco.id));
      this.setState(state => {
        return {
          ...state,
          listaEspacos: listaEspacos
        }
      })
    });
  }

  buscaEspaco(value){
    const listaFiltrada = this.state.listaEspacos.filter( espaco => {
      return espaco.nome.toLowerCase().indexOf(value.target.value.toLowerCase()) > -1} )
    this.setState(state => {
      return {
        ...state,
        listaFiltrada : listaFiltrada
      }
    })
  }

  render() {
    const { listaEspacos, listaFiltrada } = this.state;

    return (
      !listaEspacos ? <h1> Carregando </h1>
        :
        <Layout style={{ marginTop: '30px' }}>
          <Row type="flex" align="middle" justify="center">
            <Col span={23}>
              <Row>
              <Input style={{width:"200px", marginBottom:"10px"}} placeholder="Digite um nome para filtrar" onChange = {this.buscaEspaco.bind(this)}></Input>
              </Row>
             <TabelaUI colunas={this.colunasTabela} lista={ listaFiltrada.length > 0 ? listaFiltrada : listaEspacos} />
            </Col>
          </Row>
        </Layout>
    )
  }
}