package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InventarioService {
    @Autowired
    private InventarioRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public InventarioDTO salvar(InventarioEntity inventario ){
        return new InventarioDTO(this.salvarEEditar( inventario ));
    }

    @Transactional( rollbackFor = Exception.class)
    public InventarioDTO editar( InventarioEntity inventario, int id ){
        inventario.setId(id);
        return new InventarioDTO(this.salvarEEditar( inventario ));
    }

    private InventarioEntity salvarEEditar( InventarioEntity inventario ){
        return repository.save( inventario );
    }

    private List<InventarioDTO> converterLista(List<InventarioEntity> inventarioList){
        List<InventarioDTO> novaLista = new ArrayList<>();
        for(InventarioEntity inventario: inventarioList){
            novaLista.add(new InventarioDTO(inventario));
        }
        return novaLista;
    }

    public List<InventarioDTO> trazerTodosOsInventarios(){
        return this.converterLista( (List<InventarioEntity>) repository.findAll());
    }

    public InventarioDTO buscarPorId( Integer id ){
            return new InventarioDTO(repository.findById(id).get());
    }

    public List<InventarioDTO> buscarTodosPorId (Integer id){
        return this.converterLista(repository.findAllById(id));
    }

    public List<InventarioDTO> buscarTodosPorIdIn (List<Integer> idList){
        return this.converterLista(repository.findAllByIdIn(idList));
    }
    public InventarioDTO buscarPorPersonagem (PersonagemEntity personagem ){
        return new InventarioDTO(repository.findByPersonagem(personagem));
    }
    public List<InventarioDTO> buscarTodosPorPersonagem (PersonagemEntity personagem ){
        return this.converterLista(repository.findAllByPersonagem(personagem));
    }
    public List<InventarioDTO> buscarTodosPorPersonagemIn (List<PersonagemEntity> personagem ){
        return this.converterLista(repository.findAllByPersonagemIn(personagem));
    }
    public InventarioDTO buscarPorInventarioItem ( Inventario_X_Item inventarioItem){
        return new InventarioDTO(repository.findByInventarioItem(inventarioItem));
    }
    public List<InventarioDTO> buscarTodosPorInventarioItem ( Inventario_X_Item inventarioItem){
        return this.converterLista(repository.findAllByInventarioItem(inventarioItem));
    }
    public List<InventarioDTO> buscarTodosPorInventarioItemIn ( List<Inventario_X_Item> inventarioItemList){
        return this.converterLista(repository.findAllByInventarioItemIn(inventarioItemList));
    }
}
