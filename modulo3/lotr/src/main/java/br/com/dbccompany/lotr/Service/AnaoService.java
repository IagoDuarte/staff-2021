package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AnaoService {
    @Autowired
    AnaoRepository  repository;

    private List<AnaoDTO> converterLista(List<AnaoEntity> anaoList){
        List<AnaoDTO> novaLista = new ArrayList<>();
        for(AnaoEntity anao: anaoList){
            novaLista.add(new AnaoDTO(anao));
        }
        return novaLista;
    }

    public List<AnaoDTO> trazerTodosPersonagens(){
        return converterLista((List<AnaoEntity>) repository.findAll());
    }

    @Transactional(rollbackFor = Exception.class )
    public AnaoDTO salvar(AnaoEntity anao){
        return new AnaoDTO(this.salvarEEditar(anao));
    }

    @Transactional(rollbackFor = Exception.class )
    public AnaoDTO editar(AnaoEntity anao, Integer id){
        anao.setId(id);
        return new AnaoDTO(this.salvarEEditar(anao));
    }

    public AnaoEntity salvarEEditar (AnaoEntity anao){
        return repository.save(anao);
    }

    public AnaoDTO buscarPorId(Integer id){
        Optional<AnaoEntity> anao = repository.findById(id);
        return anao.isPresent() ? new AnaoDTO(anao.get()) : null;
    }

    public List<AnaoDTO> buscarTodosPorId(Integer id){
        return this.converterLista(repository.findAllById(id));
    }

    public List<AnaoDTO> buscarTodosPorIdIn(List<Integer> ids){
        return this.converterLista(repository.findAllByIdIn(ids));
    }

    public AnaoDTO buscarPorNome(String nome){
        return new AnaoDTO(repository.findByNome(nome));
    }

    public List<AnaoDTO> buscarTodosPorNome(String nome){
        return this.converterLista(repository.findAllByNome(nome));
    }

    public List<AnaoDTO> buscarTodosPorNomeIn(List<String> nomeList){
        return this.converterLista(repository.findAllByNomeIn(nomeList));
    }

    public AnaoDTO buscarPorExperiencia (Integer experiencia){
        return new AnaoDTO(repository.findByExperiencia(experiencia));
    }

    public List<AnaoDTO> buscarTodosPorExperiencia (Integer experiencia){
        return this.converterLista(repository.findAllByExperiencia(experiencia));
    }

    public List<AnaoDTO> buscarTodosPorExperienciaIn (List<Integer> experienciaList){
        return this.converterLista(repository.findAllByExperienciaIn(experienciaList));
    }

    public AnaoDTO buscarPorVida( Double vida ){
        return new AnaoDTO(repository.findByVida(vida));
    }

    public List<AnaoDTO> buscarTodosPorVida ( Double vida){
        return this.converterLista(repository.findAllByVida(vida));
    }

    public List<AnaoDTO> buscarTodosPorVidaIn (List<Double> vidaList){
        return this.converterLista(repository.findAllByVidaIn(vidaList));
    }

    public AnaoDTO buscarPorQtdDano ( Double dano){
        return new AnaoDTO(repository.findByQtdDano(dano));
    }

    public List<AnaoDTO> buscarTodosPorQtdDano ( Double dano){
        return this.converterLista(repository.findAllByQtdDano(dano));
    }

    public List<AnaoDTO> buscarTodosPorQtdDanoIn ( List<Double> danoList){
        return this.converterLista(repository.findAllByQtdDanoIn(danoList));
    }

    public AnaoDTO buscarPorQtdExperienciaPorAtaque( Integer expPorAtk ){
        return new AnaoDTO(repository.findByQtdExperienciaPorAtaque(expPorAtk));
    }

    public List<AnaoDTO> buscarTodosPorQtdExperienciaPorAtaque ( Integer expPorAtk){
        return this.converterLista(repository.findAllByQtdExperienciaPorAtaque(expPorAtk));
    }

    public List<AnaoDTO> buscarTodosPorQtdExperienciaPorAtaqueIn ( List<Integer> expPorAtkList){
        return this.converterLista(repository.findAllByQtdExperienciaPorAtaqueIn(expPorAtkList));
    }

    public AnaoDTO buscarPorInventario(InventarioEntity inventario){
        return new AnaoDTO(repository.findByInventario(inventario));
    }

    public List<AnaoDTO> buscarTodosPorInventario(InventarioEntity inventario){
        return this.converterLista(repository.findAllByInventario(inventario));
    }

    public List<AnaoDTO> buscarTodosPorInventarioIn(List<InventarioEntity> inventarioList){
        return this.converterLista(repository.findAllByInventarioIn(inventarioList));
    }
}
