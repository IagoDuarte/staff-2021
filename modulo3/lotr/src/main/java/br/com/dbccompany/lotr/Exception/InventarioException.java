package br.com.dbccompany.lotr.Exception;

public class InventarioException extends Exception{
    private String mensagem;

    public InventarioException( String mensagem ) {
        super(mensagem);
    }

    public String getMensagem() {
        return this.mensagem;
    }
}
