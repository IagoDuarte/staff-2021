package br.com.dbccompany.lotr.Security;

import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface UserPrincipalRepository extends JpaRepository<UserPrincipal, Integer> {
    UserPrincipal findByUsername(String username);
}
