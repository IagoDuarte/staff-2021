package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

    @Controller
    @RequestMapping( value = "api/inventario")
    public class InventarioController {
        @Autowired
        private InventarioService service;

        @GetMapping(value = "/")
        @ResponseBody
        public List<InventarioDTO> findAll() {
            return service.trazerTodosOsInventarios();
        }

        @GetMapping(value = "/{id}")
        @ResponseBody
        public InventarioDTO findById(@PathVariable Integer id) throws InventarioNaoEncontrado {
            try {
                return service.buscarPorId(id);
            }catch( Exception e ){
                throw new InventarioNaoEncontrado();
            }
        }

        @PostMapping(value = "/salvar")
        @ResponseBody
        public InventarioDTO save(@RequestBody InventarioEntity inventario) throws InventarioNaoEncontrado {
                return service.salvar(inventario);
        }

        @PutMapping(value = "/editar/{ id }")
        @ResponseBody
        public InventarioDTO update(@RequestBody InventarioEntity inventario, @PathVariable Integer id) {
            return service.editar(inventario, id);
        }

        @PostMapping(value = "/personagem")
        @ResponseBody
        public InventarioDTO findByPersonagem(@RequestBody PersonagemEntity personagem) {
            return service.buscarPorPersonagem(personagem);
        }

        @PostMapping(value = "/todos/personagem")
        @ResponseBody
        public List<InventarioDTO> findAllByPersonagem(@RequestBody PersonagemEntity personagem) {
            return service.buscarTodosPorPersonagem(personagem);
        }

        @PostMapping(value = "/todosin/personagem")
        @ResponseBody
        public List<InventarioDTO> findAllByPersonagemIn(@RequestBody List<PersonagemEntity> personahgemList) {
            return service.buscarTodosPorPersonagemIn(personahgemList);
        }

        @PostMapping(value = "/todos/inventarioitem")
        @ResponseBody
        public List<InventarioDTO> findByInventarioItemIn(@RequestBody List<Inventario_X_Item> inventarioItem) {
            return service.buscarTodosPorInventarioItemIn(inventarioItem);
        }

        @PostMapping(value = "/todosin/inventarioitem")
        @ResponseBody
        public List<InventarioDTO> findAllByInventarioItemIn(@RequestBody List<Inventario_X_Item> inventarioItem) {
            return service.buscarTodosPorInventarioItemIn(inventarioItem);
        }
    }
