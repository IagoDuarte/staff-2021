package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Service.Inventario_X_ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping ( value = "/api/inventarioItem")
public class Inventario_X_ItemController {

    @Autowired
    private Inventario_X_ItemService service;

    @GetMapping( value = "/")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAll(){
        return service.trazerTodosInventario_X_Item();
    }

    @PostMapping( value = "/")
    @ResponseBody
    public Inventario_X_ItemDTO findById(@RequestBody Inventario_X_ItemId id){
        return service.buscarPorId(id);
    }

    @PostMapping( value = "/salvar")
    @ResponseBody
    public Inventario_X_ItemDTO save( @RequestBody Inventario_X_ItemDTO inventarioItem ){
        return service.salvar( inventarioItem.converter() );
    }

    @PutMapping( value = "/editar")
    @ResponseBody
    public Inventario_X_ItemDTO update( @RequestBody Inventario_X_ItemDTO inventario, @RequestBody  Inventario_X_ItemId id ){
        return service.editar( inventario.converter(), id );
    }

    //------------
    @PostMapping( value = "/inventario")
    @ResponseBody
    public Inventario_X_ItemDTO findByInventario( @RequestBody InventarioDTO inventario){
        return service.buscarPorInventario(inventario.converter());
    }

    @PostMapping( value = "/todos/inventario")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByInventario( @RequestBody InventarioDTO inventario){
        return service.buscarTodosPorInventario(inventario.converter());
    }

    @PostMapping( value = "/todosin/inventario")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByInventarioIn( @RequestBody List<InventarioEntity> inventario){
        return service.buscarTodosPorInventarioIn(inventario);
    }

    @PostMapping( value = "/item")
    @ResponseBody
    public Inventario_X_ItemDTO findByItem( @RequestBody ItemDTO item ){
        return service.buscarPorItem(item.converter());
    }

    @PostMapping( value = "/todos/item")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByItem( @RequestBody ItemDTO item ){
        return service.buscarTodosPorItem(item.converter());
    }

    @PostMapping( value = "/todosin/item")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByItemIn( @RequestBody List<ItemEntity> itemList ){
        return service.buscarTodosPorItemIn(itemList);
    }

    @GetMapping( value = "/quantidade/{quantidade}")
    @ResponseBody
    public Inventario_X_ItemDTO findByQuantidade( @PathVariable Integer quantidade ){
        return service.buscarPorQuantidade(quantidade);
    }

    @GetMapping( value = "/todos/quantidade/{quantidade}")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByQuantidade( @PathVariable Integer quantidade ){
        return service.buscarTodosPorQuantidade(quantidade);
    }

    @PostMapping( value = "/todosin/quantidade")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByQuantidadeIn( @RequestBody List<Integer> quantidade ){
        return service.buscarTodosPorQuantidadeIn(quantidade);
    }

}
