package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface InventarioRepository extends CrudRepository<InventarioEntity, Integer> {
    Optional<InventarioEntity> findById(Integer id);
    List<InventarioEntity> findAllById(Integer id);
    List<InventarioEntity> findAllByIdIn(List<Integer> idList);
    InventarioEntity findByPersonagem(PersonagemEntity personagem );
    List<InventarioEntity> findAllByPersonagem(PersonagemEntity personagem );
    List<InventarioEntity> findAllByPersonagemIn(List<PersonagemEntity> personagem );
    InventarioEntity findByInventarioItem( Inventario_X_Item inventarioItem);
    List<InventarioEntity> findAllByInventarioItem( Inventario_X_Item inventarioItem);
    List<InventarioEntity> findAllByInventarioItemIn( List<Inventario_X_Item> inventarioItemList );
}
