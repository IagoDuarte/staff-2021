package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.hibernate.cache.spi.support.AbstractReadWriteAccess;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends CrudRepository<ItemEntity, Integer> {
    Optional<ItemEntity> findById(Integer id);
    ItemEntity  findByDescricao( String descricao );
    List<ItemEntity>  findAllByDescricao( String descricao );
    List<ItemEntity>  findAllByDescricaoIn( List<String> descricaoList );
    List<ItemEntity> findAllById(Integer id);
    List<ItemEntity> findAllByIdIn(List<Integer> idList);
    ItemEntity findByInventarioItem(Inventario_X_Item inventarioItem);
    List<ItemEntity> findAllByInventarioItem (Inventario_X_Item inventarioItem);
    List<ItemEntity> findAllByInventarioItemIn (List<Inventario_X_Item> inventarioItemList);
}
