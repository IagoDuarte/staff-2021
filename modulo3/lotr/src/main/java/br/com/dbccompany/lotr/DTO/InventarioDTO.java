package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;

import java.util.List;

public class InventarioDTO {
        private List<Inventario_X_Item> inventarioItem;
        private PersonagemEntity personagem;

        public InventarioDTO (InventarioEntity inventario){
            this.inventarioItem = inventario.getInventarioItem();
            this.personagem = inventario.getPersonagem();
        }

        public InventarioEntity converter(){
            InventarioEntity inventario = new InventarioEntity();
            inventario.setPersonagem(this.personagem);
            inventario.setInventarioItem(this.inventarioItem);
            return inventario;
        }

        public List<Inventario_X_Item> getInventarioItem() {
            return inventarioItem;
        }

        public void setInventarioItem(List<Inventario_X_Item> inventarioItem) {
            this.inventarioItem = inventarioItem;
        }

        public PersonagemEntity getPersonagem() {
            return personagem;
        }

        public void setPersonagem(PersonagemEntity personagem) {
            this.personagem = personagem;
        }

    }
