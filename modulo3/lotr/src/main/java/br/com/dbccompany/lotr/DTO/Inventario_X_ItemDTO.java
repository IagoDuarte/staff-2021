package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Repository.ItemRepository;

import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import java.util.ArrayList;
import java.util.List;

public class Inventario_X_ItemDTO {
    private Integer quantidade;
    private Integer idInventario;
    private Integer idItem;

    public Inventario_X_ItemDTO(Inventario_X_Item inventarioItem) {
        this.quantidade = inventarioItem.getQuantidade();
        this.idInventario = inventarioItem.getId().getId_inventario();
        this.idItem = inventarioItem.getId().getId_item();
    }

    public Inventario_X_ItemDTO() {
    }

    public Inventario_X_Item converter(){
        Inventario_X_Item inventarioItem = new Inventario_X_Item();
        Inventario_X_ItemId id = new Inventario_X_ItemId(this.idInventario, this.idItem);
        inventarioItem.setId(id);
        inventarioItem.setQuantidade(this.quantidade);
        return inventarioItem;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(Integer idInventario) {
        this.idInventario = idInventario;
    }

    public Integer getIdItem() {
        return idItem;
    }

    public void setIdItem(Integer idItem) {
        this.idItem = idItem;
    }
}
