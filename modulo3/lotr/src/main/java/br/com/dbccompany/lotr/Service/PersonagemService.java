package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public abstract class PersonagemService<R extends PersonagemRepository<E>, E extends PersonagemEntity> {

    @Autowired
    private R repository;

    public List<E> trazerTodosPersonagens(){
        return (List<E>) repository.findAll();
    }

    @Transactional(rollbackFor = Exception.class )
    public E salvar(E e){
        return this.salvarEEditar(e);
    }

    @Transactional(rollbackFor = Exception.class )
    public E editar(E e, Integer id){
        e.setId(id);
        return this.salvarEEditar(e);
    }

    public E salvarEEditar (E e){
        return repository.save(e);
    }

    public E buscarPorId(Integer id){
        Optional<E> e = repository.findById(id);
        if(e.isPresent()){
            return repository.findById(id).get();
        }
        return null;
    }

    public List<E> buscarTodosPorId(Integer id){
        return repository.findAllById(id);
    }

    public List<E> buscarTodosPorIdIn(List<Integer> ids){
        return repository.findAllByIdIn(ids);
    }

    public E buscarPorNome(String nome){
        return repository.findByNome(nome);
    }

    public List<E> buscarTodosPorNome(String nome){
        return repository.findAllByNome(nome);
    }

    public List<E> buscarTodosPorNomeIn(List<String> nomeList){
        return repository.findAllByNomeIn(nomeList);
    }

    public E buscarPorExperiencia (Integer experiencia){
        return repository.findByExperiencia(experiencia);
    }

    public List<E> buscarTodosPorExperiencia (Integer experiencia){
        return repository.findAllByExperiencia(experiencia);
    }

    public List<E> buscarTodosPorExperienciaIn (List<Integer> experienciaList){
        return repository.findAllByExperienciaIn(experienciaList);
    }

    public E buscarPorVida( Double vida ){
        return repository.findByVida(vida);
    }

    public List<E> buscarTodosPorVida ( Double vida){
        return repository.findAllByVida(vida);
    }

    public List<E> buscarTodosPorVidaIn (List<Double> vidaList){
        return repository.findAllByVidaIn(vidaList);
    }

    public E buscarPorQtdDano ( Double dano){
        return repository.findByQtdDano(dano);
    }

    public List<E> buscarTodosPorQtdDano ( Double dano){
        return repository.findAllByQtdDano(dano);
    }

    public List<E> buscarTodosPorQtdDanoIn ( List<Double> danoList){
        return repository.findAllByQtdDanoIn(danoList);
    }

    public E buscarPorQtdExperienciaPorAtaque( Integer expPorAtk ){
        return repository.findByQtdExperienciaPorAtaque(expPorAtk);
    }

    public List<E> buscarTodosPorQtdExperienciaPorAtaque ( Integer expPorAtk){
        return repository.findAllByQtdExperienciaPorAtaque(expPorAtk);
    }

    public List<E> buscarTodosPorQtdExperienciaPorAtaqueIn ( List<Integer> expPorAtkList){
        return repository.findAllByQtdExperienciaPorAtaqueIn(expPorAtkList);
    }

    public E buscarPorInventario(InventarioEntity inventario){
        return repository.findByInventario(inventario);
    }

    public List<E> buscarTodosPorInventario(InventarioEntity inventario){
        return repository.findAllByInventario(inventario);
    }

    public List<E> buscarTodosPorInventarioIn(List<InventarioEntity> inventarioList){
        return repository.findAllByInventarioIn(inventarioList);
    }
}
