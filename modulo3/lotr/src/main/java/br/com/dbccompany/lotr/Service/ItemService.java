package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosItem;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.hibernate.cache.spi.support.AbstractReadWriteAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public ItemDTO salvar(ItemEntity item ) throws ArgumentosInvalidosItem {
        ItemEntity itemNovo = this.salvarEEditar(item);
        return new ItemDTO(itemNovo);
    }

    @Transactional( rollbackFor = Exception.class)
    public ItemDTO editar( ItemEntity item, int id ){
        item.setId(id);
        ItemEntity itemNovo = this.salvarEEditar(item);
        return new ItemDTO(itemNovo);
    }

    private ItemEntity salvarEEditar( ItemEntity item ){
        return repository.save( item );
    }

    public List<ItemDTO> converterLista(List<ItemEntity> itemList){
        List<ItemDTO> novaLista = new ArrayList<>();
        for(ItemEntity item: itemList){
            novaLista.add(new ItemDTO(item));
        }
        return novaLista;
    }

    public List<ItemDTO> trazerTodosOsItens(){
        List<ItemEntity> novosItens = (List<ItemEntity>) repository.findAll();
        return this.converterLista(novosItens);
    }

    public ItemDTO buscarPorId( Integer id ) throws ItemNaoEncontrado{
        try {
            return new ItemDTO(repository.findById(id).get());
        }catch (Exception e){
            throw new ItemNaoEncontrado();
        }
    }

    public ItemDTO  buscarPorDescricao( String descricao ){
        return new ItemDTO(repository.findByDescricao(descricao));
    }

    public List<ItemDTO>  buscarTodosPorDescricao( String descricao ){
        return this.converterLista(repository.findAllByDescricao(descricao));
    }

    public List<ItemDTO>  buscarTodosPorDescricaoIn( List<String> descricaoList ){
        return this.converterLista(repository.findAllByDescricaoIn(descricaoList));
    }

    public List<ItemDTO> buscarTodosPorId(Integer id){
        return this.converterLista(repository.findAllById(id));
    }

    public List<ItemDTO> buscarTodosPorIdIn(List<Integer> idList){
        return this.converterLista(repository.findAllByIdIn(idList));
    }

    public ItemDTO buscarPorInventarioItem(Inventario_X_Item inventarioItem){
        return new ItemDTO(repository.findByInventarioItem(inventarioItem));
    }

    public List<ItemDTO> buscarTodosPorInventarioItem(Inventario_X_Item inventarioItem){
        return this.converterLista(repository.findAllByInventarioItem(inventarioItem));
    }

    public List<ItemDTO> buscarTodosPorInventarioItem(List<Inventario_X_Item> inventarioItemList){
        return this.converterLista(repository.findAllByInventarioItemIn(inventarioItemList));
    }


}
