package br.com.dbccompany.ProjetoFinal.Repository;


import br.com.dbccompany.ProjetoFinal.Entity.EspacoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Espacos_PacotesEntity;
import br.com.dbccompany.ProjetoFinal.Entity.PacoteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.TipoContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class Espacos_PacotesRepositoryTest {
    @Autowired
    Espacos_PacotesRepository repository;

    @Autowired
    EspacoRepository espacoRepository;

    @Autowired
    PacoteRepository pacoteRepository;

    @Test
    public void salvarEspacoPacote(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("espaco1");
        espaco.setQtdPessoas(1);
        float valor = 2;
        espaco.setValor(valor);
        espacoRepository.save(espaco);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(200);
        pacote.setId(1);
        pacoteRepository.save(pacote);

        Espacos_PacotesEntity espacoPacote = new Espacos_PacotesEntity();
        espacoPacote.setEspaco(espaco);
        espacoPacote.setPacote(pacote);
        espacoPacote.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        espacoPacote.setPrazo(1);
        espacoPacote.setQuantidade(1);
        repository.save(espacoPacote);

        assertEquals(espacoPacote.getEspaco().getNome(),
                repository.findById(1).get().getEspaco().getNome());
    }

    @Test
    public void buscarTodosPorPacote(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("espaco2");
        espaco.setQtdPessoas(1);
        float valor = 2;
        espaco.setValor(valor);
        espacoRepository.save(espaco);

        EspacoEntity espaco2 = new EspacoEntity();
        espaco2.setNome("espaco3");
        espaco2.setQtdPessoas(1);
        float valor2 = 2;
        espaco2.setValor(valor2);
        espacoRepository.save(espaco2);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(200);
        pacote = pacoteRepository.save(pacote);

        Espacos_PacotesEntity espacoPacote = new Espacos_PacotesEntity();
        espacoPacote.setEspaco(espaco);
        espacoPacote.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        espacoPacote.setPacote(pacote);
        espacoPacote.setPrazo(1);
        espacoPacote.setQuantidade(1);
        repository.save(espacoPacote);

        Espacos_PacotesEntity espacoPacote2 = new Espacos_PacotesEntity();
        espacoPacote2.setEspaco(espaco2);
        espacoPacote2.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        espacoPacote2.setPacote(pacote);
        espacoPacote2.setPrazo(1);
        espacoPacote2.setQuantidade(1);
        repository.save(espacoPacote2);

        assertEquals(2,
                repository.findAllByPacote(pacote).size());
    }

}
