package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.PacoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class PacoteRepositoryTest {
    @Autowired
    PacoteRepository repository;

    @Test
    public void salvarPacote(){
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(200);
        pacote.setId(1);
        repository.save(pacote);
        assertEquals(pacote.getId(), repository.findById(1).get().getId());
    }

    @Test
    public void buscarPorId(){
        PacoteEntity pacote = new PacoteEntity();
        PacoteEntity pacote2 = new PacoteEntity();
        PacoteEntity pacote3 = new PacoteEntity();
        pacote.setValor(200);
        pacote2.setValor(200);
        pacote3.setValor(200);
        pacote.setId(1);
        pacote2.setId(2);
        pacote3.setId(3);
        repository.save(pacote);
        assertEquals(pacote2.getId(), repository.findById(2).get().getId());
    }

    @Test
    public void pacoteNaoExiste(){
        assertFalse(repository.findById(1).isPresent());
    }
}
