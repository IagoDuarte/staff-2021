package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class TipoContatoRepositoryTest {
    @Autowired
    private TipoContatoRepository repository;

    @Test
    public void salvarTipoContato(){
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("whatsapp");
        repository.save(tipoContato);
        assertEquals(tipoContato.getNome(), repository.findById(1).get().getNome());
    }

    @Test
    public void tipoContatoNaoExiste(){
        assertFalse(repository.findById(1).isPresent());
    }
}
