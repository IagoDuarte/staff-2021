package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.AcessoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Saldo_ClienteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class AcessoRepositoryTest {
    @Autowired
    AcessoRepository repository;

    @Test
    public void salvarAcesso(){
        AcessoEntity acesso = new AcessoEntity();
        acesso.setEntrada(true);
        acesso.setSaldoCliente(new Saldo_ClienteEntity());
        acesso.setData(LocalDateTime.now());
        repository.save(acesso);

    }
}
