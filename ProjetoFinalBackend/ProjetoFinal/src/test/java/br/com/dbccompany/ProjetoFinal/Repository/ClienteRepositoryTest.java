package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.ContatoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.TipoContatoEntity;
import jdk.vm.ci.meta.Local;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@DataJpaTest
@Profile("test")
public class ClienteRepositoryTest {
    @Autowired
    private ClienteRepository repository;

    @Test
    public void salvarCliente(){
        ClienteEntity cliente = new ClienteEntity();
        ContatoEntity contato = new ContatoEntity();
        cliente.setCpf("123");
        LocalDate data = LocalDate.of(2020, 02, 04);
        cliente.setDataNascimento(data);
        cliente.setNome("Joao");
        contato.setValor("123");
        contato.setTipoContato(new TipoContatoEntity("whatsapp"));
        repository.save(cliente);
        assertEquals(cliente.getNome(), repository.findById(1).get().getNome());
    }

    @Test
    public void buscarClientePorContato(){
        ClienteEntity cliente = new ClienteEntity();
        ContatoEntity contato = new ContatoEntity();

        cliente.setCpf("123");
        LocalDate data = LocalDate.of(2020, 02, 04);
        cliente.setDataNascimento(data);
        cliente.setNome("Joao");
        contato.setValor("123");
        contato.setTipoContato(new TipoContatoEntity("whatsapp"));

        List<ContatoEntity> contatoList = new ArrayList<>();
        contatoList.add(contato);
        cliente.setContato(contatoList);

        repository.save(cliente);
        assertEquals(cliente.getNome(), repository.findByContato(contato).getNome());
    }

    @Test
    public void buscarClienteQueNaoExiste(){
        assertFalse(repository.findById(1).isPresent());
    }
}
