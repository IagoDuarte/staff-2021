package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

@DataJpaTest
@Profile("test")
public class PagamentoRepositoryTest {
    @Autowired
    PagamentoRepository repository;

    @Autowired
    Clientes_PacotesRepository clientesPacotesRepository;

    @Autowired
    ContratacaoRepository contratacaoRepository;

    @Autowired
    EspacoRepository espacoRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    PacoteRepository pacoteRepository;

    @Test
    public void salvarPagamentoDeContratacao(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("espaco1");
        espaco.setQtdPessoas(1);
        float valor = 2;
        espaco.setValor(valor);
        espacoRepository.save(espaco);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setCpf("123");
        LocalDate data = LocalDate.of(2020, 02, 04);
        cliente.setDataNascimento(data);
        cliente.setNome("Joao");
        cliente = clienteRepository.save(cliente);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setTipoContratacao(TipoContratacaoEnum.MINUTO);
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setDesconto((float) 0);
        contratacao.setPrazo(1);
        contratacao.setQuantidade(1);
        contratacao = contratacaoRepository.save(contratacao);

        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setContratacao(contratacao);
        pagamento.setClientePacote(null);
        pagamento.setTipoPagamento(TipoPagamentoEnum.DEBITO);
        repository.save(pagamento);
        assertEquals(cliente, repository.findAll().get(0).getContratacao().getCliente());
    }

    @Test
    public void salvarPagamentoComClientePacote(){
        ClienteEntity cliente = new ClienteEntity();
        cliente.setCpf("123");
        LocalDate data = LocalDate.of(2020, 02, 04);
        cliente.setDataNascimento(data);
        cliente.setNome("Joao");
        clienteRepository.save(cliente);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(200);
        pacote.setId(1);
        pacote = pacoteRepository.save(pacote);

        Clientes_PacotesEntity clientePacote = new Clientes_PacotesEntity();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(1);
        clientesPacotesRepository.save(clientePacote);

        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setContratacao(null);
        pagamento.setClientePacote(clientePacote);
        pagamento.setTipoPagamento(TipoPagamentoEnum.DEBITO);
        repository.save(pagamento);
        assertEquals(cliente, repository.findById(1).get().getClientePacote().getCliente());
    }
}
