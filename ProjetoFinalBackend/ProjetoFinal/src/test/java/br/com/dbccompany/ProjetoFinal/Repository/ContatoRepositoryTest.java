package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.ContatoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ContatoRepositoryTest {
    @Autowired
    ContatoRepository repository;

    @Test
    public void salvarContato(){
        ContatoEntity contato = new ContatoEntity();
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("whatsapp");
        contato.setTipoContato(tipoContato);
        contato.setCliente(new ClienteEntity());
        contato.setValor("1546854");
        repository.save(contato);
        assertEquals(contato.getValor(), repository.findById(1).get().getValor() );
    }

    @Test
    public void contatoNaoExiste(){
        assertFalse(repository.findById(1).isPresent());
    }
}
