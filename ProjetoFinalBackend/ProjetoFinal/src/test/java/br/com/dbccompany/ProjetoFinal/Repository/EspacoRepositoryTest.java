package br.com.dbccompany.ProjetoFinal.Repository;


import br.com.dbccompany.ProjetoFinal.Entity.EspacoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class EspacoRepositoryTest {
    @Autowired
    EspacoRepository repository;

    @Test
    public void salvarEspaco(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("espaco1");
        espaco.setQtdPessoas(1);
        float valor = 2;
        espaco.setValor(valor);
        repository.save(espaco);
        assertEquals(espaco.getNome(), repository.findByNome("espaco1").getNome());
    }

    @Test
    public void buscarPorId(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("espaco1");
        espaco.setQtdPessoas(1);
        float valor = 2;
        espaco.setValor(valor);
        repository.save(espaco);
        assertEquals(espaco.getNome(), repository.findById(2).get().getNome());
    }

    @Test
    public void buscarEspacoQueNaoExiste(){
        assertFalse(repository.findById(1).isPresent());
    }

}
