package br.com.dbccompany.ProjetoFinal.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class AcessoEntity {
    @Id
    @SequenceGenerator( name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue( generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private Boolean entrada;

    @Column
    private LocalDateTime data;

    @ManyToOne( fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn( name = "idCliente", nullable = false ),
            @JoinColumn( name = "idEspaco", nullable = false )
    })
    private Saldo_ClienteEntity saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getEntrada() {
        return entrada;
    }

    public void setEntrada(Boolean entrada) {
        this.entrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Saldo_ClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(Saldo_ClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }
}
