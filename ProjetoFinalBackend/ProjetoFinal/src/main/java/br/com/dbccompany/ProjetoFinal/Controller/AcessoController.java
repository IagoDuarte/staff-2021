package br.com.dbccompany.ProjetoFinal.Controller;

import br.com.dbccompany.ProjetoFinal.DTO.AcessoDTO;
import br.com.dbccompany.ProjetoFinal.DTO.Saldo_ClienteDTO;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Service.AcessoService;
import br.com.dbccompany.ProjetoFinal.Service.ClienteService;
import br.com.dbccompany.ProjetoFinal.Service.EspacoService;
import br.com.dbccompany.ProjetoFinal.Service.Saldo_ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping( value = "/apiTrabalho/acesso")
public class AcessoController {
    @Autowired
    AcessoService service;

    @Autowired
    EspacoService espacoService;

    @Autowired
    ClienteService clienteService;

    @Autowired
    Saldo_ClienteService saldoClienteService;

    @PostMapping( value = "/entrar")
    @ResponseBody
    public ResponseEntity<Object> entrar(@RequestBody AcessoDTO acesso ){
        try {
            Saldo_ClienteDTO saldoCliente = saldoClienteService.findById(acesso.getSaldoCliente().getId());
            acesso.setSaldoCliente(saldoCliente);
            ResponseEntity<Object> object = new ResponseEntity<>(service.entrar(acesso.converter()), HttpStatus.ACCEPTED);
            return object;
        }catch (DadosInvalidos e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping( value = "/sair")
    @ResponseBody
    public ResponseEntity<Object> sair(@RequestBody AcessoDTO acesso ){
        try{
            Saldo_ClienteDTO saldoCliente = saldoClienteService.findById(acesso.getSaldoCliente().getId());
            acesso.setSaldoCliente(saldoCliente);
            ResponseEntity<Object> object = new ResponseEntity<>(service.sair( acesso.converter() ) ,HttpStatus.ACCEPTED);
            return object;
        }catch (DadosInvalidos e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem() ,HttpStatus.ACCEPTED);
        }
    }

}
