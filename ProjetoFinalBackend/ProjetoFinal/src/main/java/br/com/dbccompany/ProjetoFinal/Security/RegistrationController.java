package br.com.dbccompany.ProjetoFinal.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping( value = "/registrar")
public class RegistrationController {
    @Autowired
    UserPrincipalDetailService service;

    @PostMapping( value = "/salvar")
    @ResponseBody
    public UserPrincipalDTO salvarUsuario(@RequestBody UserPrincipalDTO userPrincipal ){
        service.salvar(userPrincipal.converter());
        return userPrincipal;
    }
}
