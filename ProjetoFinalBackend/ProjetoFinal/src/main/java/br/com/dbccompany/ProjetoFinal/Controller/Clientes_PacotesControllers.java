package br.com.dbccompany.ProjetoFinal.Controller;

import br.com.dbccompany.ProjetoFinal.DTO.*;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.ProjetoFinal.Service.ClienteService;
import br.com.dbccompany.ProjetoFinal.Service.Clientes_PacotesService;
import br.com.dbccompany.ProjetoFinal.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping( value = "/apiTrabalho/clientePacote")
public class Clientes_PacotesControllers {
    @Autowired
    private Clientes_PacotesService service;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private PacoteService pacoteService;

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> saveClientePacote(@RequestBody Clientes_PacotesRetornoDTO clientePacote ){
        try{
            Clientes_PacotesDTO dto = new Clientes_PacotesDTO();
            ClienteDTO cliente = clienteService.findById(clientePacote.getIdCliente());
            PacoteDTO pacote = pacoteService.findById(clientePacote.getIdPacote());
            dto.setPacote(pacote);
            dto.setCliente(cliente);
            dto.setQuantidade(clientePacote.getQuantidade());
            ResponseEntity<Object> object = new ResponseEntity<>(service.save( dto.converter() ) , HttpStatus.ACCEPTED);
            return object;
        }catch(DadosInvalidos | ObjetoNaoEncontrado e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.BAD_REQUEST);
        }

    }
}
