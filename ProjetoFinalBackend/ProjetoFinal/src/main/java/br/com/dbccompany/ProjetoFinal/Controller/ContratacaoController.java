package br.com.dbccompany.ProjetoFinal.Controller;

import br.com.dbccompany.ProjetoFinal.DTO.ClienteDTO;
import br.com.dbccompany.ProjetoFinal.DTO.ContratacaoDTO;
import br.com.dbccompany.ProjetoFinal.DTO.ContratacaoRetornoDTO;
import br.com.dbccompany.ProjetoFinal.DTO.EspacoDTO;
import br.com.dbccompany.ProjetoFinal.Entity.ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.ProjetoFinal.Service.ClienteService;
import br.com.dbccompany.ProjetoFinal.Service.ContratacaoService;
import br.com.dbccompany.ProjetoFinal.Service.EspacoService;
import oracle.jdbc.proxy.annotation.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/apiTrabalho/contratacao")
public class ContratacaoController {

    @Autowired
    private ContratacaoService service;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private EspacoService espacoService;

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> saveContratacao(@RequestBody ContratacaoRetornoDTO contratacao ){
        try {
            ContratacaoDTO dto = new ContratacaoDTO();
            ClienteDTO cliente = clienteService.findById(contratacao.getIdCliente());
            EspacoDTO espaco = espacoService.findById(contratacao.getIdEspaco());
            dto.setCliente(cliente);
            dto.setEspaco(espaco);
            dto.setTipoContratacao(contratacao.getTipoContratacao());
            dto.setDesconto(contratacao.getDesconto());
            dto.setPrazo(contratacao.getPrazo());
            dto.setQuantidade(contratacao.getQuantidade());
            ResponseEntity<Object> object = new ResponseEntity<>(service.save( dto.converter() ) , HttpStatus.ACCEPTED);
            return object;
        }catch(ObjetoNaoEncontrado | DadosInvalidos e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public List<ContratacaoDTO> findAll(){
        return service.findAll();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> findAllByClienteId(@PathVariable Integer id){
        try{
            ResponseEntity<Object> object = new ResponseEntity<>( service.findAllByClienteId(id),HttpStatus.ACCEPTED);
            return object;
        }catch ( ObjetoNaoEncontrado e ){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.BAD_REQUEST);
        }
    }




}
