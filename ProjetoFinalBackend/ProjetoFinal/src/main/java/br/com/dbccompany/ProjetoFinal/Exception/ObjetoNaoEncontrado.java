package br.com.dbccompany.ProjetoFinal.Exception;

public class ObjetoNaoEncontrado extends ObjetoException{

    public ObjetoNaoEncontrado(){
        super("Esse item nao existe!");
    }


}
