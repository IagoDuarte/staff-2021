package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.ContatoEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {
    @Override
    Optional<ClienteEntity> findById(Integer integer);

    ClienteEntity findByContato(ContatoEntity contato);
}
