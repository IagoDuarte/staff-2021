package br.com.dbccompany.ProjetoFinal.Service;

import br.com.dbccompany.ProjetoFinal.DTO.PagamentoDTO;
import br.com.dbccompany.ProjetoFinal.Entity.*;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.ProjetoFinal.ProjetoFinalApplication;
import br.com.dbccompany.ProjetoFinal.Repository.Espacos_PacotesRepository;
import br.com.dbccompany.ProjetoFinal.Repository.PacoteRepository;
import br.com.dbccompany.ProjetoFinal.Repository.PagamentoRepository;
import br.com.dbccompany.ProjetoFinal.Repository.Saldo_ClienteRepository;
import br.com.dbccompany.ProjetoFinal.Util.LogsErrorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {
    @Autowired
    private PagamentoRepository repository;

    @Autowired
    private Saldo_ClienteRepository saldoClienteRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Autowired
    private Espacos_PacotesRepository espacoPacoteRepository;

    private Logger logger = LoggerFactory.getLogger(ProjetoFinalApplication.class);

    @Transactional( rollbackFor = Exception.class )
    public PagamentoDTO save(PagamentoEntity pagamento ) throws DadosInvalidos {
        try {
            PagamentoEntity pagamentoNovo = this.saveAndEdit( pagamento );
            PagamentoEntity atualizado = adicionarSaldo(pagamentoNovo);
            PagamentoDTO dto = new PagamentoDTO( atualizado );
            logger.info("Pagamento salvo. ID: " + pagamentoNovo.getId());
            return dto;
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro400(e);
            throw new DadosInvalidos();
        }
    }

    private PagamentoEntity saveAndEdit( PagamentoEntity pagamento ){
        return repository.save(pagamento);
    }

    private PagamentoEntity adicionarSaldo(PagamentoEntity pagamento){
        if(pagamento.getClientePacote() == null){
            this.adicionarSaldoContratacao(pagamento);
        }
        if(pagamento.getContratacao() == null){
            Optional<PacoteEntity> pacote = pacoteRepository.findById(pagamento.getClientePacote().getPacote().getId());
            ClienteEntity cliente = pagamento.getClientePacote().getCliente();
            Integer idCliente = cliente.getId();
            if(pacote.isPresent()) {

                List<Espacos_PacotesEntity> listaEspacos = espacoPacoteRepository.findAllByPacote(pacote.get());

                for (Espacos_PacotesEntity espacos : listaEspacos) {

                    Integer idEspaco = espacos.getEspaco().getId();
                    Saldo_ClienteId saldoClienteId = new Saldo_ClienteId(idCliente, idEspaco);
                    Optional<Saldo_ClienteEntity> saldoCliente = saldoClienteRepository.findById(saldoClienteId);

                    if (saldoCliente.isPresent()) {
                        LocalDate vencimento = saldoCliente.get().getVencimento();
                        vencimento = vencimento.plusDays(espacos.getPrazo());
                        saldoCliente.get().setVencimento(vencimento);
                        saldoClienteRepository.save(saldoCliente.get());
                    } else {
                        Saldo_ClienteEntity saldoClienteNovo = new Saldo_ClienteEntity();
                        Integer quantidade = espacos.getQuantidade();
                        TipoContratacaoEnum tipoContratacao = espacos.getTipoContratacao();
                        Integer prazo = espacos.getPrazo();
                        saldoClienteNovo.setCliente(pagamento.getClientePacote().getCliente());
                        saldoClienteNovo.setEspaco(espacos.getEspaco());
                        saldoClienteNovo.setVencimento(LocalDate.now().plusDays(espacos.getPrazo()));
                        saldoClienteNovo.setQuantidade(espacos.getQuantidade());
                        saldoClienteNovo.setTipoContratacao(espacos.getTipoContratacao());
                        saldoClienteNovo.setId(saldoClienteId);
                        saldoClienteRepository.save(saldoClienteNovo);
                        logger.info("Saldo adicionado. ID Cliente: " + idCliente + " ID Espaco: " + idEspaco);
                    }
                }
            }
        }
        return pagamento;
    }

    private void adicionarSaldoContratacao(PagamentoEntity pagamento){
        Integer idCliente = pagamento.getContratacao().getCliente().getId();
        Integer idEspaco = pagamento.getContratacao().getEspaco().getId();
        Integer quantidade = pagamento.getContratacao().getQuantidade();
        TipoContratacaoEnum tipoContratacao = pagamento.getContratacao().getTipoContratacao();
        Integer prazo = pagamento.getContratacao().getPrazo();

        Saldo_ClienteId saldoClienteId = new Saldo_ClienteId(idCliente, idEspaco);

        Optional<Saldo_ClienteEntity> saldoCliente = saldoClienteRepository.findById(saldoClienteId);

        if(saldoCliente.isPresent() == false){
            Saldo_ClienteEntity saldoClienteNovo = new Saldo_ClienteEntity();
            saldoClienteNovo.setCliente(pagamento.getContratacao().getCliente());
            saldoClienteNovo.setEspaco(pagamento.getContratacao().getEspaco());
            saldoClienteNovo.setQuantidade(quantidade);
            saldoClienteNovo.setTipoContratacao(tipoContratacao);
            saldoClienteNovo.setVencimento( LocalDate.now().plusDays(prazo) );
            saldoClienteNovo.setId(saldoClienteId);
            saldoClienteRepository.save(saldoClienteNovo);
        }else{
            LocalDate vencimento = saldoCliente.get().getVencimento();
            saldoCliente.get().setVencimento(vencimento.plusDays(prazo));
            saldoClienteRepository.save(saldoCliente.get());
        }
        logger.info("Saldo adicionado. ID Cliente: " + idCliente + " ID Espaco: " + idEspaco);
    }

}
