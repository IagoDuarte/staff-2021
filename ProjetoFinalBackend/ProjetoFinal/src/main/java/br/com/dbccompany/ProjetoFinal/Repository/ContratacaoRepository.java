package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.ContratacaoEntity;
import ch.qos.logback.core.net.server.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
    Optional<ContratacaoEntity> findById(Integer integer);

   ContratacaoEntity save(ContratacaoEntity contratacao);

   List<ContratacaoEntity> findAll();

   List<ContratacaoEntity> findAllByCliente_Id(Integer id);
}
