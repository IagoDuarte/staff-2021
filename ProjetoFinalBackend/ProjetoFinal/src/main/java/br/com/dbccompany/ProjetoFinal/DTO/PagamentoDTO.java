package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.ContratacaoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.PagamentoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.TipoPagamentoEnum;

public class PagamentoDTO {
    private ContratacaoDTO contratacao;
    private Clientes_PacotesDTO clientePacote;
    private TipoPagamentoEnum tipoPagamento;

    public PagamentoDTO(PagamentoEntity pagamento){
        if(pagamento.getContratacao() != null)
            this.contratacao = new ContratacaoDTO( pagamento.getContratacao());
        if(pagamento.getClientePacote() != null)
            this.clientePacote = new Clientes_PacotesDTO( pagamento.getClientePacote() );
        this.tipoPagamento = pagamento.getTipoPagamento();
    }

    public PagamentoDTO(){}

    public PagamentoEntity converter(){
        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setTipoPagamento(this.tipoPagamento);
        if(this.clientePacote != null)
            pagamento.setClientePacote(this.clientePacote.converter());
        if(this.contratacao != null)
            pagamento.setContratacao(this.contratacao.converter());
        return pagamento;
    }

    public ContratacaoDTO getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoDTO contratacao) {
        this.contratacao = contratacao;
    }

    public Clientes_PacotesDTO getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(Clientes_PacotesDTO clientePacote) {
        this.clientePacote = clientePacote;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

}
