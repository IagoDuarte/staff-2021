package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.EspacoEntity;

public class EspacoDTO {
    Integer id;
    String nome;
    Integer qtdPessoas;
    float valor;

    public EspacoDTO(EspacoEntity espaco) {
        this.id = espaco.getId();
        this.nome = espaco.getNome();
        this.qtdPessoas = espaco.getQtdPessoas();
        this.valor = espaco.getValor();
    }

    public EspacoEntity converter(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome(this.nome);
        espaco.setQtdPessoas(this.qtdPessoas);
        espaco.setValor(this.valor);
        espaco.setId(id);
        return espaco;
    }

    public EspacoDTO(){
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPesoas) {
        this.qtdPessoas = qtdPesoas;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
