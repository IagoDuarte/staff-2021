package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Clientes_PacotesEntity;
import br.com.dbccompany.ProjetoFinal.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Clientes_PacotesRepository extends CrudRepository<Clientes_PacotesEntity, Integer> {
    @Override
    List<Clientes_PacotesEntity> findAll();

    List<Clientes_PacotesEntity> findAllByPacote(PacoteEntity pacote);
}
