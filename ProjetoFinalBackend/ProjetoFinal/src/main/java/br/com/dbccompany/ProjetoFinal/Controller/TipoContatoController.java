package br.com.dbccompany.ProjetoFinal.Controller;

import br.com.dbccompany.ProjetoFinal.DTO.TipoContatoDTO;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/apiTrabalho/tipoContato")
public class TipoContatoController {
    @Autowired
    private TipoContatoService service;

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> saveTipoContato(@RequestBody TipoContatoDTO tipoContato ){
        try{
            ResponseEntity<Object> object = new ResponseEntity<>(service.save( tipoContato.converter() ) , HttpStatus.ACCEPTED);
            return object;
        }catch (DadosInvalidos e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping( value = "/")
    @ResponseBody
    public List<TipoContatoDTO> findAll(){
        return service.findAll();
    }

}
