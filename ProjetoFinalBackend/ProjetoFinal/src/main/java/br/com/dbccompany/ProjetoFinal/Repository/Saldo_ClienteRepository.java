package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.DTO.Saldo_ClienteDTO;
import br.com.dbccompany.ProjetoFinal.Entity.Saldo_ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Saldo_ClienteId;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface Saldo_ClienteRepository extends CrudRepository<Saldo_ClienteEntity, Saldo_ClienteId> {
    @Override
    Optional<Saldo_ClienteEntity> findById(Saldo_ClienteId saldo_clienteId);

    List<Saldo_ClienteEntity> findAll();
}
