package br.com.dbccompany.ProjetoFinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ContratacaoEntity {
    @Id
    @SequenceGenerator( name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne( fetch = FetchType.LAZY )
    private ClienteEntity cliente;

    @ManyToOne( fetch = FetchType.LAZY)
    private EspacoEntity espaco;

    @OneToMany(cascade =  CascadeType.ALL, mappedBy = "contratacao")
    private List<PagamentoEntity> pagamento;

    @Enumerated( EnumType.STRING )
    private TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    Integer quantidade;

    @Column
    Float desconto;

    @Column(nullable = false)
    Integer prazo;

    public ContratacaoEntity(ClienteEntity cliente, EspacoEntity espaco, TipoContratacaoEnum tipoContratacao, Integer quantidade, Float desconto, Integer prazo) {
        this.cliente = cliente;
        this.espaco = espaco;
        this.tipoContratacao = tipoContratacao;
        this.quantidade = quantidade;
        this.desconto = desconto;
        this.prazo = prazo;
    }

    public ContratacaoEntity(){}

    public Integer getId() {
        return id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Float getDesconto() {
        return desconto;
    }

    public void setDesconto(Float desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<PagamentoEntity> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentoEntity> pagamento) {
        this.pagamento = pagamento;
    }
}
