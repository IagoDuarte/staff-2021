package br.com.dbccompany.ProjetoFinal.Service;

import br.com.dbccompany.ProjetoFinal.DTO.TipoContatoDTO;
import br.com.dbccompany.ProjetoFinal.Entity.TipoContatoEntity;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.ProjetoFinalApplication;
import br.com.dbccompany.ProjetoFinal.Repository.TipoContatoRepository;
import br.com.dbccompany.ProjetoFinal.Util.LogsErrorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class TipoContatoService {
    @Autowired
    private TipoContatoRepository repository;

    private Logger logger = LoggerFactory.getLogger(ProjetoFinalApplication.class);

    @Transactional( rollbackFor = Exception.class )
    public TipoContatoDTO save(TipoContatoEntity tipoContato ) throws DadosInvalidos {
        try {
            TipoContatoEntity tipoContatoNovo = this.saveAndEdit( tipoContato );
            logger.info("tipoContato salvo. ID: " + tipoContatoNovo.getId());
            return new TipoContatoDTO( tipoContatoNovo );
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro400(e);
            throw new DadosInvalidos();
        }
    }

    private TipoContatoEntity saveAndEdit( TipoContatoEntity tipoContato ){
        return repository.save(tipoContato);
    }

    public List<TipoContatoDTO> findAll(){
        List<TipoContatoEntity> tipoContatoList = (List<TipoContatoEntity>) repository.findAll();
        return this.listConverter(tipoContatoList);
    }

    private List<TipoContatoDTO> listConverter( List<TipoContatoEntity> tipoContatoList){
        List<TipoContatoDTO> newList = new ArrayList<>();
        for(TipoContatoEntity tipoContato: tipoContatoList){
            newList.add(new TipoContatoDTO(tipoContato));
        }
        return newList;
    }

}
