package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.ContatoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.TipoContatoEntity;

public class ContatoDTO {
    private TipoContatoDTO tipoContato;
    private String valor;

    public ContatoDTO(ContatoEntity contato){
        this.tipoContato = new TipoContatoDTO(contato.getTipoContato());
        this.valor = contato.getValor();
    }

    public ContatoDTO(){}

    public ContatoEntity converter(){
        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato(this.tipoContato.converter());
        contato.setValor(this.valor);
        return contato;
    }

    public TipoContatoDTO getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoDTO tipoContato) {
        this.tipoContato = tipoContato;
    }


    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

}
