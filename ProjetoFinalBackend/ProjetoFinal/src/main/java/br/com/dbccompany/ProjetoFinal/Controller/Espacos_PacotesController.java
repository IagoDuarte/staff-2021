package br.com.dbccompany.ProjetoFinal.Controller;

import br.com.dbccompany.ProjetoFinal.DTO.EspacoDTO;
import br.com.dbccompany.ProjetoFinal.DTO.Espacos_PacotesDTO;
import br.com.dbccompany.ProjetoFinal.DTO.PacoteDTO;
import br.com.dbccompany.ProjetoFinal.DTO.Espacos_PacotesRetornoDTO;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.ProjetoFinal.Service.EspacoService;
import br.com.dbccompany.ProjetoFinal.Service.Espacos_PacotesService;
import br.com.dbccompany.ProjetoFinal.Service.PacoteService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping( value = "/apiTrabalho/espacoPacote")
public class Espacos_PacotesController {

    @Autowired
    Espacos_PacotesService service;

    @Autowired
    EspacoService espacoService;

    @Autowired
    PacoteService pacoteService;

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> saveEspacoContato(@RequestBody Espacos_PacotesRetornoDTO espacoContato ){
        try {
            Espacos_PacotesDTO dto = new Espacos_PacotesDTO();
            EspacoDTO espaco = espacoService.findById(espacoContato.getIdEspaco());
            PacoteDTO pacote = pacoteService.findById(espacoContato.getIdPacote());
            dto.setEspaco(espaco);
            dto.setPacote(pacote);
            dto.setPrazo(espacoContato.getPrazo());
            dto.setTipoContratacao(espacoContato.getTipoContratacao());
            dto.setQuantidade(espacoContato.getQuantidade());
            ResponseEntity<Object> object = new ResponseEntity<>(service.save( dto.converter() ) , HttpStatus.ACCEPTED);
            return object;
        }catch (DadosInvalidos | ObjetoNaoEncontrado e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem() , HttpStatus.ACCEPTED);
        }

    }
}
