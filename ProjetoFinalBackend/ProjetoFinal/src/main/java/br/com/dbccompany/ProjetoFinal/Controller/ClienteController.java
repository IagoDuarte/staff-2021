package br.com.dbccompany.ProjetoFinal.Controller;

import br.com.dbccompany.ProjetoFinal.DTO.ClienteDTO;
import br.com.dbccompany.ProjetoFinal.DTO.TipoContatoDTO;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Service.ClienteService;
import br.com.dbccompany.ProjetoFinal.Service.TipoContatoService;
import ch.qos.logback.core.net.server.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/apiTrabalho/cliente")
public class ClienteController {
    @Autowired
    private ClienteService service;

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> saveCliente(@RequestBody ClienteDTO cliente ){
        try{
            ResponseEntity<Object> object = new ResponseEntity<>( service.save( cliente.converter() ) , HttpStatus.ACCEPTED);
            return object;
        }catch(DadosInvalidos e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping( value = "/")
    @ResponseBody
    public List<ClienteDTO> findAll(){
        return service.findAll();
    }
}
