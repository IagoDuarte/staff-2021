package br.com.dbccompany.ProjetoFinal.Controller;

import br.com.dbccompany.ProjetoFinal.DTO.EspacoDTO;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/apiTrabalho/espaco")
public class EspacoController {
    @Autowired
    private EspacoService service;

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> saveEspaco(@RequestBody EspacoDTO espaco ){
        try {
            ResponseEntity<Object> object = new ResponseEntity<>(service.save( espaco.converter() ) , HttpStatus.ACCEPTED);
            return object;
        }catch (DadosInvalidos e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping( value = "/")
    @ResponseBody
    public List<EspacoDTO> findAll(){
        return service.findAll();
    }

}
