package br.com.dbccompany.ProjetoFinal.Service;

import br.com.dbccompany.ProjetoFinal.DTO.ClienteDTO;
import br.com.dbccompany.ProjetoFinal.DTO.ContatoDTO;
import br.com.dbccompany.ProjetoFinal.Entity.ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.ContatoEntity;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.ProjetoFinal.ProjetoFinalApplication;
import br.com.dbccompany.ProjetoFinal.Repository.ClienteRepository;
import br.com.dbccompany.ProjetoFinal.Util.LogsErrorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository repository;

    private Logger logger = LoggerFactory.getLogger(ProjetoFinalApplication.class);

    @Transactional( rollbackFor = Exception.class )
    public ClienteDTO save(ClienteEntity cliente ) throws DadosInvalidos {
        try{
            ClienteEntity clienteNovo = this.saveAndEdit( cliente );
            logger.info("Cliente salvo. ID: " + clienteNovo.getId());
            return new ClienteDTO( clienteNovo );
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro400(e);
            throw new DadosInvalidos();
        }
    }

    private boolean checarContatos( List<ContatoEntity> lista ){
        boolean email = false;
        boolean telefone = false;
        for(int i = 0; i < lista.size(); i++){
            if( lista.get(i).getTipoContato().getNome().equalsIgnoreCase("email")){
                email = true;
            }
            if( lista.get(i).getTipoContato().getNome().equalsIgnoreCase("telefone")){
                telefone = true;
            }
        }
        if(email == true && telefone == true){
            return true;
        }
        return false;
    }

    private ClienteEntity saveAndEdit( ClienteEntity cliente ) throws DadosInvalidos {
            if(checarContatos(cliente.getContato())){
                return repository.save(cliente);
            }else{
                throw new DadosInvalidos();
            }
    }

    public List<ClienteDTO> findAll(){
        List<ClienteEntity> clienteList = (List<ClienteEntity>) repository.findAll();
        return this.listConverter(clienteList);
    }

    public ClienteDTO findById(Integer id) throws ObjetoNaoEncontrado {
        try {
            return new ClienteDTO(repository.findById(id).get());
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro404(e);
            throw new ObjetoNaoEncontrado();
        }
    }

    private List<ClienteDTO> listConverter( List<ClienteEntity> clienteList){
        List<ClienteDTO> newList = new ArrayList<>();
        for(ClienteEntity contato: clienteList){
            newList.add(new ClienteDTO(contato));
        }
        return newList;
    }

}
