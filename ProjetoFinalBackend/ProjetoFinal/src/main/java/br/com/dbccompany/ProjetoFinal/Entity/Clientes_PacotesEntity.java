package br.com.dbccompany.ProjetoFinal.Entity;

import javax.persistence.*;

@Entity
public class Clientes_PacotesEntity {
    @Id
    @SequenceGenerator( name = "CLIENTEPACOTE_SEQ", sequenceName = "CLIENTEPACOTE_SEQ")
    @GeneratedValue( generator = "CLIENTEPACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne( fetch = FetchType.LAZY)
    private PacoteEntity pacote;

    @ManyToOne( fetch = FetchType.LAZY )
    private ClienteEntity cliente;

    @Column( nullable = false )
    private Integer quantidade;

    public Clientes_PacotesEntity(PacoteEntity pacote, ClienteEntity cliente, Integer quantidade) {
        this.pacote = pacote;
        this.cliente = cliente;
        this.quantidade = quantidade;
    }

    public Clientes_PacotesEntity(){}

    public Integer getId() {
        return id;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
