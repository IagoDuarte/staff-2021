package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.Espacos_PacotesEntity;
import br.com.dbccompany.ProjetoFinal.Entity.TipoContratacaoEnum;

public class Espacos_PacotesDTO {
    private PacoteDTO pacote;
    private EspacoDTO espaco;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Integer prazo;

    public Espacos_PacotesDTO( Espacos_PacotesEntity espacosPacotes){
        this.pacote = new PacoteDTO(espacosPacotes.getPacote());
        this.espaco = new EspacoDTO(espacosPacotes.getEspaco());
        this.tipoContratacao = espacosPacotes.getTipoContratacao();
        this.quantidade = espacosPacotes.getQuantidade();
        this.prazo = espacosPacotes.getPrazo();
    }

    public Espacos_PacotesDTO(){}

    public Espacos_PacotesEntity converter(){
        Espacos_PacotesEntity espacosPacotes = new Espacos_PacotesEntity();
        espacosPacotes.setEspaco(this.espaco.converter());
        espacosPacotes.setPacote(this.pacote.converter());
        espacosPacotes.setTipoContratacao(this.tipoContratacao);
        espacosPacotes.setQuantidade(this.quantidade);
        espacosPacotes.setPrazo(this.prazo);
        return espacosPacotes;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
