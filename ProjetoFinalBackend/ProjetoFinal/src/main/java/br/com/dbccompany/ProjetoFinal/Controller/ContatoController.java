package br.com.dbccompany.ProjetoFinal.Controller;

import br.com.dbccompany.ProjetoFinal.DTO.ContatoDTO;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/apiTrabalho/contato")
public class ContatoController {
    @Autowired
    private ContatoService service;

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> saveContato(@RequestBody ContatoDTO contato ){
        try{
            ResponseEntity<Object> object = new ResponseEntity<>(service.save( contato.converter() ) , HttpStatus.ACCEPTED);
            return object;
        }catch( DadosInvalidos e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping( value = "/")
    @ResponseBody
    public List<ContatoDTO> findAll(){
        return service.findAll();
    }
}
