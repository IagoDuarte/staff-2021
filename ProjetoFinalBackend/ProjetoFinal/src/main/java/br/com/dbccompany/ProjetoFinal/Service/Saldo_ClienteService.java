package br.com.dbccompany.ProjetoFinal.Service;

import br.com.dbccompany.ProjetoFinal.DTO.EspacoDTO;
import br.com.dbccompany.ProjetoFinal.DTO.Saldo_ClienteDTO;
import br.com.dbccompany.ProjetoFinal.Entity.EspacoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Saldo_ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Saldo_ClienteId;
import br.com.dbccompany.ProjetoFinal.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.ProjetoFinal.Repository.Saldo_ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

@Service
public class Saldo_ClienteService {
    @Autowired
    Saldo_ClienteRepository repository;

    public List<Saldo_ClienteDTO> findAll(){
        return listConverter(repository.findAll());
    }

    public Saldo_ClienteDTO findById(Saldo_ClienteId id)  {
            return new Saldo_ClienteDTO(repository.findById(id).get());
    }

    private List<Saldo_ClienteDTO> listConverter(List<Saldo_ClienteEntity> saldoList){
        List<Saldo_ClienteDTO> newList = new ArrayList<>();
        for(Saldo_ClienteEntity saldo: saldoList){
            newList.add(new Saldo_ClienteDTO(saldo));
        }
        return newList;
    }

}
