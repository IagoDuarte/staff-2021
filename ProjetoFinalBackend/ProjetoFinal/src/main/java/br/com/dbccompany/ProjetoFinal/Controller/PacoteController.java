package br.com.dbccompany.ProjetoFinal.Controller;

import br.com.dbccompany.ProjetoFinal.DTO.EspacoDTO;
import br.com.dbccompany.ProjetoFinal.DTO.PacoteDTO;
import br.com.dbccompany.ProjetoFinal.DTO.PacoteRetornoDTO;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Repository.PacoteRepository;
import br.com.dbccompany.ProjetoFinal.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/apiTrabalho/pacote")
public class PacoteController {

    @Autowired
    private PacoteService service;

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> savePacote(@RequestBody PacoteDTO pacote ){
        try{
            ResponseEntity<Object> object = new ResponseEntity<>(service.save( pacote.converter() ) , HttpStatus.ACCEPTED);
            return object;
        }catch(DadosInvalidos e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping( value = "/")
    @ResponseBody
    public List<PacoteRetornoDTO> findAll(){
        return service.findAll();
    }
}
