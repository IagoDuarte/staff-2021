package br.com.dbccompany.ProjetoFinal.Security;

import javax.persistence.*;
import java.util.List;

public class UserPrincipalDTO {
    private String email;
    private String nome;
    private String username;
    private String password;
    private boolean active;
    List<String> roles;
    List<String> permissions;
    String rolesConverted;
    String permissionsConverted;

    public UserPrincipalDTO(UserPrincipal user){
        this.email = user.getEmail();
        this.nome = user.getNome();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.active = true;
        this.roles = user.getRoles();
        this.permissions = user.getPermissions();
    }

    public UserPrincipalDTO(){}

    public UserPrincipal converter(){
        convertListsToString(this.roles, this.permissions);
        UserPrincipal user = new UserPrincipal();
        user.setPassword(this.password);
        user.setActive(this.active);
        user.setEmail(this.email);
        user.setUsername(this.username);
        user.setNome(this.nome);
        user.setRoles(this.rolesConverted);
        user.setPermissions(this.permissionsConverted);
        return user;
    }

    private void convertListsToString(List<String> roles, List<String> permissions){
        StringBuilder stringBuilderRole = new StringBuilder();
        for(String role: roles){
            stringBuilderRole.append(role);
            stringBuilderRole.append(",");
        }
        this.rolesConverted = stringBuilderRole.toString();

        StringBuilder stringBuilderPerm = new StringBuilder();
        for(String permission: permissions){
            stringBuilderRole.append(permission);
            stringBuilderRole.append(",");
        }
        this.permissionsConverted = stringBuilderPerm.toString();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

}
