package br.com.dbccompany.ProjetoFinal.Service;

import br.com.dbccompany.ProjetoFinal.DTO.Espacos_PacotesDTO;
import br.com.dbccompany.ProjetoFinal.DTO.PacoteDTO;
import br.com.dbccompany.ProjetoFinal.DTO.PacoteRetornoDTO;
import br.com.dbccompany.ProjetoFinal.Entity.ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Clientes_PacotesEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Espacos_PacotesEntity;
import br.com.dbccompany.ProjetoFinal.Entity.PacoteEntity;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.ProjetoFinal.ProjetoFinalApplication;
import br.com.dbccompany.ProjetoFinal.Repository.Clientes_PacotesRepository;
import br.com.dbccompany.ProjetoFinal.Repository.Espacos_PacotesRepository;
import br.com.dbccompany.ProjetoFinal.Repository.PacoteRepository;
import br.com.dbccompany.ProjetoFinal.Util.LogsErrorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PacoteService {
    @Autowired
    private PacoteRepository repository;

    @Autowired
    private Espacos_PacotesRepository espacosPacotesRepository;

    @Autowired
    private Clientes_PacotesRepository clientesPacotesRepository;

    private Logger logger = LoggerFactory.getLogger(ProjetoFinalApplication.class);

    @Transactional( rollbackFor = Exception.class )
    public PacoteDTO save(PacoteEntity pacote ) throws DadosInvalidos {
        try {
            PacoteEntity pacoteNovo = this.saveAndEdit( pacote );
            logger.info("Pacote salvo. ID: " + pacoteNovo.getId());
            return new PacoteDTO( pacoteNovo );
        }catch(Exception e){
            LogsErrorsHandler.excecaoErro400(e);
            throw new DadosInvalidos();
        }
    }

    private PacoteEntity saveAndEdit( PacoteEntity pacote ){
        return repository.save(pacote);
    }

    public List<PacoteRetornoDTO> findAll(){
        List<PacoteEntity> newList = repository.findAll();
        for(PacoteEntity pacote: newList){
            List<Espacos_PacotesEntity> espacosPacotes = espacosPacotesRepository.findAllByPacote(pacote);
            pacote.setEspacoPacotes(espacosPacotes);
        }
        for(PacoteEntity pacote: newList){
            List<Clientes_PacotesEntity> clientesPacotes = clientesPacotesRepository.findAllByPacote(pacote);
            pacote.setClientePacote(clientesPacotes);
        }
        return listConverterRetorno(newList);
    }

    public PacoteDTO findById(Integer id) throws ObjetoNaoEncontrado {
        try{
            PacoteEntity pacote = repository.findById(id).get();
            return new PacoteDTO(pacote);
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro404(e);
            throw new ObjetoNaoEncontrado();
        }
    }

    private List<PacoteRetornoDTO> listConverterRetorno( List<PacoteEntity> pacotes ){
        List<PacoteRetornoDTO> newList = new ArrayList<>();
        for(PacoteEntity pacote: pacotes){
            newList.add(new PacoteRetornoDTO(pacote));
        }
        return newList;
    }
}
