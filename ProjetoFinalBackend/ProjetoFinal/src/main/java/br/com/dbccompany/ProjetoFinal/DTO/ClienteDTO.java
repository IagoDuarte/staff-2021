package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.ContatoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.ContratacaoEntity;
import org.yaml.snakeyaml.util.ArrayUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ClienteDTO{
    private Integer id;
    private String nome;
    private String cpf;
    private LocalDate dataNascimento;
    private List<ContatoDTO> contato;

    public ClienteDTO(ClienteEntity cliente){
        this.id = cliente.getId();
        this.nome = cliente.getNome();
        this.cpf = cliente.getCpf();
        this.dataNascimento = cliente.getDataNascimento();
        this.contato = converterToDTO(cliente.getContato());
    }

    public ClienteDTO(){}

    public ClienteEntity converter(){
        ClienteEntity cliente = new ClienteEntity();
        cliente.setContato( converterToEntity(this.contato) );
        cliente.setNome(this.nome);
        cliente.setCpf(this.cpf);
        cliente.setDataNascimento(this.dataNascimento);
        cliente.setId(this.id);
        return cliente;
    }

    List<ContratacaoDTO> converterContratacaoToDTO(List<ContratacaoEntity> contratacoes){
        List<ContratacaoDTO> newList = new ArrayList<>();
            for(ContratacaoEntity contratacao : contratacoes ){
            newList.add( new ContratacaoDTO(contratacao));
        }
        return newList;
    }

    List<ContratacaoEntity> converterContratacaoToEntity(List<ContratacaoDTO> contratacoes){
        List<ContratacaoEntity> newList = new ArrayList<>();
        for(ContratacaoDTO contratacao : contratacoes ){
            newList.add( contratacao.converter());
        }
        return newList;
    }

    private List<ContatoEntity> converterToEntity( List<ContatoDTO> contatos){
        List<ContatoEntity> newList = new ArrayList<>();
        for(ContatoDTO contato : contatos ){
            newList.add(contato.converter());
        }
        return newList;
    }

    private List<ContatoDTO> converterToDTO( List<ContatoEntity> contatos){
        List<ContatoDTO> newList = new ArrayList<>();
        for(ContatoEntity contato : contatos ){
            newList.add(new ContatoDTO(contato));
        }
        return newList;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoDTO> getContato() {
        return contato;
    }

    public void setContato(List<ContatoDTO> contato) {
        this.contato = contato;
    }

    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
