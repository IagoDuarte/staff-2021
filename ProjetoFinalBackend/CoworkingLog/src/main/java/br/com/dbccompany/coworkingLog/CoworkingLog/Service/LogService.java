package br.com.dbccompany.coworkingLog.CoworkingLog.Service;

import br.com.dbccompany.coworkingLog.CoworkingLog.DTO.LogDTO;
import br.com.dbccompany.coworkingLog.CoworkingLog.Entity.LogEntity;
import br.com.dbccompany.coworkingLog.CoworkingLog.Repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogService {

    @Autowired
    private LogRepository repository;

    private MongoTemplate mongoTemplate;

    public List<LogDTO> findAll(){
        return this.convertListToDTO(repository.findAll());
    }

    public LogDTO save(LogDTO log) {
            return new LogDTO( repository.insert(log.converter()) );
    }

    public List<LogDTO> findAllByCodigo(String codigo){
            return this.convertListToDTO( repository.findAllByCodigo(codigo) );
    }

    public List<LogDTO> findAllByTipo( String tipo ){
            return this.convertListToDTO( repository.findAllByTipo( tipo ) );
    }

    private List<LogDTO> convertListToDTO(List<LogEntity> logs){
        ArrayList<LogDTO> newList = new ArrayList<LogDTO>();
        for( LogEntity log:logs){
            newList.add( new LogDTO( log ) );
        }
        return newList;
    }

}