package br.com.dbccompany.coworkingLog.CoworkingLog.Entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document( collection="Log" )
public class LogEntity {
    @Id
    private String id;

    private String data;
    private String codigo;
    private String descricao;
    private String tipo;

    public LogEntity(String id, String data, String tipo, String codigo, String descricao) {
        this.id = id;
        this.data = data;
        this.codigo = codigo;
        this.descricao = descricao;
        this.tipo = tipo;
    }

    public LogEntity(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}

