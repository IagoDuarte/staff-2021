package br.com.dbccompany.coworkingLog.CoworkingLog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoworkingLogApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoworkingLogApplication.class, args);
	}

}
