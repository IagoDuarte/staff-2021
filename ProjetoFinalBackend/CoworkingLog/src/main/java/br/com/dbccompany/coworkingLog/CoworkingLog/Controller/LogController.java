package br.com.dbccompany.coworkingLog.CoworkingLog.Controller;

import br.com.dbccompany.coworkingLog.CoworkingLog.DTO.LogDTO;
import br.com.dbccompany.coworkingLog.CoworkingLog.Service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( value = "/coworkingLogs" )
public class LogController {

    @Autowired
    private LogService service;

    @GetMapping(path = "/")
    public List<LogDTO> retornarTodos() {
        return service.findAll();
    }

    @PostMapping(path = "/salvar")
    public LogDTO salvar(@RequestBody LogDTO erro) {
            return service.save(erro);
    }

    @GetMapping(path = "/buscarPorCodigo/{codigo}")
    public List<LogDTO> buscarPorCodigo(@PathVariable String codigo) {
            return service.findAllByCodigo(codigo);
    }

    @GetMapping(path = "/buscarPorTipo/{tipo}")
    public List<LogDTO> buscarPorTipo(@PathVariable String tipo) {
            return service.findAllByTipo(tipo);
    }
}