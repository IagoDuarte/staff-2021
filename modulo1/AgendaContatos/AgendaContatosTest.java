import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AgendaContatosTest{
    @Test
    public void adicionarContato(){
        int qtdContatos;
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Iago", "999999999");
        agenda.adicionar("Marcos", "55555555");
        qtdContatos = agenda.getAgenda().size();
        assertEquals(2, qtdContatos);
    }
    
    @Test
    public void consultarContatoPeloNome(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Iago", "999999999");
        agenda.adicionar("Marcos", "55555555");
        assertEquals("999999999", agenda.consultar("Iago"));
    }
    
    @Test
    public void consultarContatoPeloNumero(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Iago", "999999999");
        agenda.adicionar("Marcos", "55555555");
        assertEquals("Iago", agenda.consultarNumero("999999999"));
    }
}
