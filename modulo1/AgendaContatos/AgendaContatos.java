import java.util.*;
public class AgendaContatos{
    private HashMap<String, String> contatos;
    
    public AgendaContatos(){
        contatos = new LinkedHashMap<>();
    }
    
    public void adicionar(String nome, String numero){       
        this.contatos.put(nome, numero);
    }
    
    public HashMap<String, String> getAgenda(){
        return this.contatos;
    }
    
    public String consultar(String nome){
        return contatos.get(nome);
    }
    
    public String consultarNumero(String numero){
        for(HashMap.Entry<String, String> entry : contatos.entrySet()){
            String nome = entry.getKey();
            if( entry.getValue().equals(numero) ){
                return entry.getKey();
            }
        }
        return null;
    }
    
    public String csv(){
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        for(HashMap.Entry<String, String> entry : this.contatos.entrySet()){
            String chave = entry.getKey();
            String valor = entry.getValue();
            String contato = String.format("%s,%s%s", chave, valor, separador);
            builder.append(contato);
        }
        return builder.toString();
    }
    
    public void main(){
        String csv = this.csv();
        System.out.println(csv);
    }
    
}
