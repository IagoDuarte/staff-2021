import java.util.*;
public class EstrategiaPriorizandoElfosVerdes implements EstrategiaDeAtaque {
    
    private ArrayList<Elfo> collections( ArrayList<Elfo> elfos ){
        Collections.sort(elfos, new ComparadorDeElfos());
        return elfos;
    }
    
    public ArrayList<Elfo> getOrdemDeAtaque( ArrayList<Elfo> atacantes ){
        return collections( atacantes );
    }
}
