

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
public class EstatisticasInventarioTest{
    
    @Test
    public void mediaDosItens(){
        Inventario inventario = new Inventario();
        double media;
        Item flecha = new Item(4, "Flecha");
        Item arco = new Item(3, "Arco");
        Item escudo = new Item(3, "Escudo");
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
        inventario.adicionarItem(escudo);
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        media = estatistica.calcularMedia();
        assertEquals(3.3 , media, 0.1);
    }
    
    @Test
    public void medianaDosItens(){
        Inventario inventario = new Inventario();
        Item adaga = new Item(4, "Adaga");
        Item flecha = new Item(3, "Flecha");
        Item arco = new Item(2, "Arco");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(arco);
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(adaga);
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        assertEquals(2.5 , estatistica.calcularMediana(), 1e-8);
    }
    
    @Test
    public void itensAcimaDaMedia(){
        Inventario inventario = new Inventario();
        int itensAcima;
        Item flecha = new Item(4, "Flecha");
        Item pocao = new Item(4, "Pocao de vida");
        Item arco = new Item(3, "Arco");
        Item escudo = new Item(3, "Escudo");
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
        inventario.adicionarItem(pocao);
        inventario.adicionarItem(escudo);
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        itensAcima = estatistica.qtdItensAcimaDaMedia();
        assertEquals(2 , itensAcima);
    }
    
    @Test
    public void calcularMediaInventarioVazio(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertTrue( Double.isNaN( resultado ) );
        
    }
    
    @Test
    public void calcularMediaUmItem(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem( new Item( 1, "Escudo de Madeira" ) );
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals( 1, resultado, 1e-8 );
    }
    
    @Test
    public void calcularMediaQtdsIguais(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem( new Item( 3, "Espada de Madeira" ) );
        inventario.adicionarItem( new Item( 3, "Escudo de Madeira" ) );
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals( 3, resultado, 1e-8 );        
    }
    
    @Test
    public void calcularMediaQtdsDiferentes(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem( new Item( 2, "Espada de Madeira" ) );
        inventario.adicionarItem( new Item( 4, "Escudo de Madeira" ) );
        inventario.adicionarItem( new Item( 3, "Botas" ) );
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals( 3, resultado, 1e-8 );      
    }
    
    @Test
    public void calcularMedianaUmItem(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem( new Item( 6, "Escudo de Madeira" ) );
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals( 6, resultado, 1e-8 );
    }
    
    @Test
    public void calcularMedianaQtdsImpares(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem( new Item( 5, "Espada de Madeira" ) );
        inventario.adicionarItem( new Item( 10, "Escudo de Madeira" ) );
        inventario.adicionarItem( new Item( 20, "Botas" ) );
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals( 10, resultado, 1e-8 ); 
    }
    @Test
    public void calcularMedianaQtdsPares(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem( new Item( 5, "Espada de Madeira" ) );
        inventario.adicionarItem( new Item( 10, "Escudo de Madeira" ) );
        inventario.adicionarItem( new Item( 20, "Botas" ) );
        inventario.adicionarItem( new Item( 20, "Adaga" ) );
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals( 15, resultado, 1e-8 ); 
    }
}
