import java.util.*;
public class ExercitoElfo{
    private ArrayList<Elfo> exercito;
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    
    public ExercitoElfo(){
        this.exercito = new ArrayList<>();
    }
    
    public ArrayList<Elfo> getExercito(){
        return this.exercito;
    }
    
    public void alistarElfo(Elfo elfo){
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if( podeAlistar ){
            this.exercito.add(elfo);
            
            ArrayList<Elfo> elfoDeUmStatus = porStatus.get(elfo.getStatus() );
            if(elfoDeUmStatus == null){
                elfoDeUmStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfoDeUmStatus);
            }
            elfoDeUmStatus.add( elfo );
        }
    }
    
    public ArrayList<Elfo> buscar(Status status){
        return this.porStatus.get(status);
    }
    
    public ArrayList<Elfo> buscarExcluindoUmStatus(Status status){
        ArrayList<Elfo> todosElfos = this.exercito;
        ArrayList<Elfo> elfosParaExcluir = this.porStatus.get(status);
        todosElfos.removeAll(elfosParaExcluir);
        todosElfos.removeAll(elfosParaExcluir);
        return todosElfos;
    }
    
    public ArrayList<Elfo> buscarLista(Status status){
        ArrayList<Elfo> listaRetorno = new ArrayList<>();
        for(Elfo elfo : this.exercito){
            if(elfo.getStatus() == status){
                listaRetorno.add(elfo);
            }
        }
        return listaRetorno;
    }
    
    public ArrayList<Elfo> pelotaoNoturnosPorUltimo(ArrayList<Elfo> elfos){
        ArrayList<Elfo> pelotaoDeAtaque = new ArrayList<>();
        if(!elfos.isEmpty()){
            for ( Elfo elfo : elfos ){
                if( !elfo.getStatus().equals(Status.MORTO) ){
                    if( elfo instanceof ElfoVerde ){
                        pelotaoDeAtaque.add(0,elfo);
                    }else{
                        int indiceFinal = pelotaoDeAtaque.size();
                        pelotaoDeAtaque.add(indiceFinal,elfo);
                    }
                }
            }
            return pelotaoDeAtaque;
        }
        return null;
    }

    public ArrayList<Elfo> pelotaoDeAtaqueIntercalado(ArrayList<Elfo> elfos){
        ArrayList<Elfo> pelotaoDeAtaque = new ArrayList<>();
        // MAIS VERDES DO QUE NOTURNOS
        // elfos = [verde,verde,verde,verde,verde,noturno,noturno];
        // pelotaoDeAtaque = [verde,verde,verde,verde,verde];
        // pelotaoDeAtaque = [verde,noturno,verde,verde,verde,verde];
        // pelotaoDeAtaque = [verde,noturno,verde,noturno,verde,verde,verde];

        // MAIS NOTURNOS DO QUE VERDES
        // elfos = [verde,verde,noturno,noturno,noturno,noturno];
        // pelotaoDeAtaque = [verde,verde];
        // pelotaoDeAtaque = [verde,noturno,verde];
        // pelotaoDeAtaque = [noturno,noturno,noturno,verde,noturno,verde];

        if(!elfos.isEmpty()){
            for ( Elfo elfo : elfos ){
                if( !elfo.getStatus().equals(Status.MORTO) && elfo instanceof ElfoVerde )
                    pelotaoDeAtaque.add(0,elfo);   
            }

            for ( Elfo elfo : elfos ){
                if( !elfo.getStatus().equals(Status.MORTO) && elfo instanceof ElfoNoturno ){
                    int indice = 1;
                    boolean continueANadar = true;

                    while( continueANadar ){
                        if( indice >=  pelotaoDeAtaque.size()){
                            pelotaoDeAtaque.add( 0 ,elfo); 
                        }

                        boolean posicaoValida = pelotaoDeAtaque.get(indice - 1) instanceof ElfoVerde && pelotaoDeAtaque.get(indice) instanceof ElfoVerde;

                        if( posicaoValida ){
                            pelotaoDeAtaque.add( indice ,elfo);  
                            continueANadar = false; 
                        }
                        indice++;
                    }
                }
            }

            return pelotaoDeAtaque;
        }
        return null;
    }
    
    public ArrayList<Elfo> ataqueSenhorDosAneis (ArrayList<Elfo> elfos){
        ArrayList<Elfo> pelotao = new ArrayList<>();
        int elfosNoturnos = 0;
        for(Elfo elfo : elfos){
            if( (!elfo.getStatus().equals(Status.MORTO)) && elfo.getQtdFlecha() > 0){
                pelotao.add(elfo);
            }
        }
        double tamanhoPelotao = pelotao.size();
        int umTerco = (int) (tamanhoPelotao * 0.3);

        pelotao = ordenaArray(pelotao);
        int i = 0;
        while (i < pelotao.size()){
            if(pelotao.get(i) instanceof ElfoNoturno){
                elfosNoturnos++;
                if(elfosNoturnos > umTerco ){
                    pelotao.remove(i);
                }
                
            }
            i++;
        }

        return pelotao;
    }
    
    public ArrayList<Elfo> ordenaArray(ArrayList<Elfo> elfos){
        Elfo aux = new Elfo(" ");
        for ( int i = 0; i < elfos.size(); i++){
            for( int j = 0; j < elfos.size() - 1; j++){
                if (elfos.get(j).getQtdFlecha() < elfos.get(j + 1).getQtdFlecha()){
                    aux = elfos.get(j);
                    elfos.set(j, elfos.get(j + 1)) ;
                    elfos.set(j + 1, aux);
                }
            }
        }
        return elfos;
    }
}
