public abstract class Personagem {
    protected String nome;
    protected int experiencia, qtdExperienciaPorAtaque;
    protected Inventario inventario = new Inventario();
    protected double vida;
    protected Status status;
    protected double qtdDano;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.qtdExperienciaPorAtaque = 1;
        this.experiencia = 0;
        this.qtdDano = 0.0;
    }
    
    public Personagem(String nome){
        this.nome = nome;
    }
    
    public String getNome (){
        return this.nome;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public int getExperiencia(){
        return this.experiencia;
    }
    
    public double getVida(){
        return this.vida;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
            
    public void ganharItem(Item item){
        this.inventario.adicionarItem(item);
    }
    
    public void perderItem( Item item ){
        this.inventario.removerItem(item);
    }
    
    protected void aumentarXP(){
        this.experiencia += qtdExperienciaPorAtaque;
    }
    
    protected boolean podeSofrerDano(){
        return this.vida > 0;
    }
    
    private Status validacaoStatus(){
        return this.vida == 0 ? Status.MORTO : Status.SOFREU_DANO;
    }
    
    public void receberDano(){
        if( podeSofrerDano() ){
            this.vida -= this.vida >= qtdDano ? this.qtdDano : 0;
            this.status = this.validacaoStatus();
        }
    }
    
    public abstract String imprimirNomeClasse();
}
