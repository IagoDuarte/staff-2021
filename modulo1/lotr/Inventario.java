
import java.util.*;
public class Inventario{
    private ArrayList<Item> itens;
    
    public Inventario(){
        this.itens = new ArrayList<>();
    }
    
    public ArrayList<Item> getItens(){
        return this.itens;
    }
    
    public void adicionarItem(Item item){       
        this.itens.add( item );
    }
    
    private boolean validaPosicao( int posicao ){
        return posicao >= this.itens.size();
    }
    
    public Item obterItem(int posicao){
        return  this.validaPosicao(posicao) ? null : this.itens.get(posicao);
    }
    
    public void removerItem(int posicao){
        if( !this.validaPosicao(posicao) ){
            this.itens.remove(posicao);
        }
    }
    
    
    public void removerItem(Item item){
        this.itens.remove(item);
    }
    
    public String getDescricoesItens(){
        StringBuilder itens = new StringBuilder();
        for(Item item : this.itens){
            String descricao = item.getDescricao();
            itens.append(descricao);
            if(this.itens.indexOf(item) < this.itens.size() - 1){
                itens.append(",");
            }
        }
        return itens.toString();       
    }
    
    public boolean checarSeTemItemNaPosicao(int posicao){
        return obterItem(posicao) != null ? true : false;
    }
    
    public Item getItemComMaiorQuantidade(){
        int indice = 0, maiorQuantidadeParcial = 0;
        
        for(Item item : this.itens){
            int quantidadeAtual = item.getQuantidade();
            if( quantidadeAtual > maiorQuantidadeParcial){
                indice = this.itens.indexOf(item);
                maiorQuantidadeParcial = quantidadeAtual;
            }
        }
        return this.itens.size() > 0 ? this.obterItem(indice) : null;
    }
    
    public Item getItemPorDescricao(String descricao){
        for(Item item : this.itens){
            if(item.getDescricao().equals(descricao)){
                return item;
            }
        }
        return null;
    }
    
    public ArrayList inverter(){
        ArrayList<Item> temp = new ArrayList<>();
        for( int i = this.itens.size() - 1; i >= 0; i--){
            temp.add(this.itens.get(i));
        }
        return temp;
    }
    
    public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    public void ordenarItens(TipoOrdenacao tipoOrdenacao){
        if(tipoOrdenacao == TipoOrdenacao.ASC){
            for( int i = 0; i < this.itens.size(); i++){
                for( int j = 0; j< this.itens.size(); j++ ){
                    Item atual = this.itens.get(j);
                    Item proximo = this.itens.get(j+1);
                    boolean deveTrocar = tipoOrdenacao == TipoOrdenacao.ASC ?
                    (atual.getQuantidade() > proximo.getQuantidade()):
                    (atual.getQuantidade() < proximo.getQuantidade());
                    if( deveTrocar ){
                        Item itemTrocado = atual;
                        this.itens.set(j, proximo);
                        this.itens.set(j+1, itemTrocado);
                    }
                }
        }
        }else{
                boolean ordenou = false;
            while(!ordenou){
                ordenou = true;
                for (int i=0 ; i < this.itens.size() - 1; i++){
                    if(this.itens.get(i).getQuantidade() < this.itens.get(i+1).getQuantidade()){
                        Item temp = this.itens.get(i);
                        this.itens.set(i, this.itens.get(i+1));
                        this.itens.set(i+1, temp);
                        ordenou = false;
                    }
            }
        }
        }
    }
}
