

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{
    @Test
    public void elfoVerdeNasceComArcoEFlecha(){
        ElfoVerde elfo = new ElfoVerde("Legolas Verde");
        Item arco = new Item(1, "Arco");
        Item flecha = new Item(2, "Flecha");
        Item resultadoArco = elfo.getInventario().obterItem(1);
        Item resultadoFlecha = elfo.getInventario().obterItem(0);
        assertEquals(arco, resultadoArco);
        assertEquals(flecha, resultadoFlecha);
    }
    
    @Test
    public void elfoVerdeAtiraFlechaPerdeUmaUnidadeGanhaXP(){
        ElfoVerde elfo = new ElfoVerde("Legolas Verde");
        Dwarf dwarf = new Dwarf("Anao");
        elfo.atirarFlecha(dwarf);
        assertEquals(2, elfo.getExperiencia());
    }
    
    @Test
    public void elfoVerdeDeveGanharItensEspecificos(){
        ElfoVerde elfo = new ElfoVerde("Legolas Verde");
        Item espada = new Item(1, "Espada de Aco Valiriano");
        Item arco = new Item(1, "Arco de Vidro");
        Item flecha = new Item(1, "Flecha de Vidro");
        Item teste = new Item(1, "Teste");
        elfo.inventario.adicionarItem(espada);
        elfo.inventario.adicionarItem(arco);
        elfo.inventario.adicionarItem(flecha);
        elfo.inventario.adicionarItem(teste);
    }
}
