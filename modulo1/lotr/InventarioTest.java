


import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
public class InventarioTest
{

    @Test
    public void inventarioDeveAdicionarItens(){
        Inventario inventario = new Inventario();
        Item flecha = new Item(2, "Flecha");
        Item arco = new Item(1, "Arco");
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
        assertEquals(flecha, inventario.getItens().get(0));
        assertEquals(arco, inventario.getItens().get(1));
    }
    
    @Test
    public void inventarioDeveRetornarItemEspecifico(){
        Inventario inventario = new Inventario();
        Item arco = new Item(1, "Arco");
        inventario.adicionarItem(arco);
        assertEquals(arco, inventario.obterItem(0));
    }
    
    @Test
    public void inventarioPodeTerItensRemovidos(){
        Inventario inventario = new Inventario();
        Item flecha = new Item(2, "Flecha");
        inventario.adicionarItem(flecha);
        Item arco = new Item(1, "Arco");
        inventario.adicionarItem(arco);
        inventario.removerItem(0);
        assertEquals(arco, inventario.obterItem(0));
    }
    
    @Test
    public void inventarioDeveRetornarDescricaoDosItens(){
        Inventario inventario = new Inventario();
        Item flecha = new Item(2, "Flecha");
        Item arco = new Item(1, "Arco");
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
        assertEquals("Flecha,Arco", inventario.getDescricoesItens());
    }
    
    @Test
    public void inventarioDeveRetornarItemComMaiorQuantidade(){
        Inventario inventario = new Inventario();
        Item flecha = new Item(2, "Flecha");
        Item teste = new Item(1000, "Teste");
        Item arco = new Item(1, "Arco");
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
        inventario.adicionarItem(teste);
        assertEquals(teste, inventario.getItemComMaiorQuantidade());
    }
    
    @Test
    public void buscarApenasUmItem(){
        Inventario inventario = new Inventario();
        Item flecha = new Item(2, "Flecha");
        inventario.adicionarItem(flecha);
        Item resultado = inventario.getItemPorDescricao("Flecha");
        assertEquals(flecha, resultado);
    }
    
    @Test
    public void inverterDoisItens(){
        Inventario inventario = new Inventario();
        Item flecha = new Item(2, "Flecha");
        Item arco = new Item(1, "Arco");
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);        
        ArrayList<Item> resultado = inventario.inverter();
        assertEquals(arco, resultado.get(0));
        assertEquals(flecha, resultado.get(1));
        
    }
    
    @Test
    public void ordenarItens(){
        Inventario inventario = new Inventario();
        Item pocao = new Item(4, "pocao");
        Item flecha = new Item(3, "Flecha");
        Item arco = new Item(2, "Arco");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
        inventario.adicionarItem(pocao);
        inventario.ordenarItens();
        assertEquals("Escudo", inventario.obterItem(0).getDescricao());
        assertEquals("Arco", inventario.obterItem(1).getDescricao());
    }
    
    @Test
    public void ordenarItensDesc(){
        Inventario inventario = new Inventario();
        Item pocao = new Item(4, "pocao");
        Item flecha = new Item(3, "Flecha");
        Item arco = new Item(5, "Arco");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(flecha);
        inventario.adicionarItem(arco);
        inventario.adicionarItem(pocao);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        assertEquals("Arco", inventario.obterItem(0).getDescricao());
        assertEquals("pocao", inventario.obterItem(1).getDescricao());
    }
    
    
}
