import java.util.*;

public class ExercitoQueAtaca extends ExercitoElfo {
    private EstrategiaDeAtaque estrategia;
    
    public ExercitoQueAtaca(EstrategiaDeAtaque estrategia) {
        this.estrategia = estrategia;
    }
    
    public void trocarEstrategia(EstrategiaDeAtaque estrategia){
        this.estrategia = estrategia;
    }
    
    public void atacar( ArrayList<Dwarf> anoes ){
        ArrayList<Elfo> ordem = this.estrategia.getOrdemDeAtaque(this.buscarExcluindoUmStatus
                                                                    (Status.MORTO) );
        for(Elfo elfo : ordem){
            for(Dwarf anao : anoes){
                elfo.atirarFlecha(anao);
            }
        } 
    }
    
}
