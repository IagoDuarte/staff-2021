public class Elfo  extends Personagem{
    private int indiceFlecha = 0;
    private static int qtdElfos;
    
    {
        this.indiceFlecha = 0;
        Elfo.qtdElfos = 0;
    }
    public Elfo(String nome){
        super(nome);
        this.vida = 100.0;
        inventario.adicionarItem(new Item(2, "Flecha"));
        inventario.adicionarItem(new Item(1, "Arco"));
        Elfo.qtdElfos++;
    }
    
    public static int getQuantidade() {
        return Elfo.qtdElfos;
    }
    
    public void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
    
    public Item getFlecha() {
        return this.inventario.obterItem(indiceFlecha);
    }
    
    protected int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
    
    protected boolean podeAtirar(){
        if(this.getFlecha().getQuantidade() > 0){
            return true;
        }
        return false;
    }
    
    public void atirarFlecha(Dwarf dwarf){
        if(this.podeAtirar()){
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            this.aumentarXP();
            super.receberDano();
            dwarf.receberDano();
        }
    }
    
    public  String imprimirNomeClasse(){
        return "Elfo";
    }
}
