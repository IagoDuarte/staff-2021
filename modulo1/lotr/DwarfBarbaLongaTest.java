import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
public class DwarfBarbaLongaTest{
    @Test
    public void dwarfRecebeDano66PorCento(){
        DadoFalso dado = new DadoFalso();
        dado.simularValor(4);
        DwarfBarbaLonga balin = new DwarfBarbaLonga("Balin", dado);
        balin.receberDano();
        assertEquals(100.0, balin.getVida(), 1e-8);
    }
    
    @Test
    public void dwarfNaoRecebeDano33PorCento(){
        DadoFalso dado = new DadoFalso();
        dado.simularValor(2);
        DwarfBarbaLonga balin = new DwarfBarbaLonga("Balin", dado);
        balin.receberDano();
        assertEquals(110.0, balin.getVida(), 1e-8);        
        
    }
}
