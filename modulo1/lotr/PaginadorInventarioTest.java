import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class PaginadorInventarioTest{
    @Test
    public void limitarItens(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem(new Item(1, "Espada"));
        inventario.adicionarItem(new Item(2, "Escudo de metal"));
        inventario.adicionarItem(new Item(3, "Poção de HP"));
        inventario.adicionarItem(new Item(4, "Bracelete"));
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        ArrayList<Item> itens = new ArrayList<>();
        paginador.pular(0);
        itens = paginador.limitar(2);
        assertEquals("Escudo de metal", itens.get(1).getDescricao());        
        paginador.pular(2);
        itens = paginador.limitar(2);
        assertEquals("Poção de HP", itens.get(0).getDescricao());  

    }
    
    @Test
    public void pularLimitarComApenasUmItem(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.adicionarItem( espada );
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertEquals(espada, primeiraPagina.get(0));
        assertEquals(1, primeiraPagina.size());
    }
    
    @Test 
    public void pularLimitarForaDosLimites(){
        Inventario inventario = new Inventario();   
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item lanca = new Item(3, "Lanca");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(lanca);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(1000);
        
        assertEquals(espada, primeiraPagina.get(0));
        assertEquals(escudo, primeiraPagina.get(1));
        
        assertEquals(lanca, segundaPagina.get(0));
    }
    
    @Test
    public void pularLimitarDentroDosLimites(){}
}
