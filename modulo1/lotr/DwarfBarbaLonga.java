public class DwarfBarbaLonga extends Dwarf{
    private Sorteador sorteador;
    
    public DwarfBarbaLonga(String nome){
        super(nome);
        this.sorteador = new DadoD6();
    }
    
    public DwarfBarbaLonga( String nome, Sorteador sorteador ){
        super(nome);
        this.sorteador = sorteador;
    }
    
    protected boolean podeSofrerDano(){
        return this.vida > 0 && sorteador.sortear() > 2;
    }
}
