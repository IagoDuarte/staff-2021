

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest{
    @Test
    public void elfoDaLuzDeveNascerComUmaEspada(){
        ElfoDaLuz elfo = new ElfoDaLuz("Feanor");
        Item espada = new Item( 1, "Espada de Galvorn");
        assertEquals(espada, elfo.inventario.obterItem(2));
    }
    
    @Test
    public void elfoPerde21DeVidaEmAtaquesImpares(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.atacarComEspada(new Dwarf("Farlum")); 
        assertEquals(79.0, feanor.getVida(), 0.00001);
    }
    
    @Test
    public void elfoRcebe10DeVidaEmAtaquesPares(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.atacarComEspada(new Dwarf("Farlum"));
        feanor.atacarComEspada(new Dwarf("Farlum")); 
        assertEquals(89.0, feanor.getVida(), 0.00001);
    }
}
