

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DwarfTest
{
        
    @Test
    public void dwarfDeveNascerCom110DeVida(){
        Dwarf dwarf = new Dwarf("Anao");
        assertEquals(110.0, dwarf.getVida(), 0.1);
        assertEquals(Status.RECEM_CRIADO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfDevePerder10DeVidaPorFlecha(){
        Dwarf dwarf = new Dwarf("Anao");
        Elfo elfo = new Elfo("Legolas");
        elfo.atirarFlecha(dwarf);
        assertEquals(100.0, dwarf.getVida(), 0.1);
    }
    
    @Test
    public void dwarfDeveFicarComStatusMortoQuandoSemVida(){
        Dwarf dwarf = new Dwarf("Anao");
        Elfo elfo = new Elfo("Legolas");
        elfo.getFlecha().setQuantidade(100);
        for (int i = 0; i < 11 ; i++){
            elfo.atirarFlecha(dwarf);
        }
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
    @Test
    public void anaoNasceComEscudoNoInventario(){
        Dwarf dwarf = new Dwarf("Gimli");
        Item esperado = new Item(1, "Escudo");
        Item resultado = dwarf.getInventario().obterItem(0);
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void anaoEquipadoTomaMetade(){
        Dwarf anao = new Dwarf( "Gimli" );
        anao.equiparEDesequiparEscudo();
        anao.receberDano();
        assertEquals(105.0, anao.getVida(), 0.00001);
        
    }
    
    @Test
    public void anaoNaoEquipadoTomaDanoIntegral(){
        Dwarf anao = new Dwarf( "Gimli" );
        anao.receberDano();
        assertEquals(100.0, anao.getVida(), 0.00001);        
    }
    
}
