
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoTest{
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void exemploMudancaValoresItem(){
        Elfo elfo = new Elfo( "Legolas" );
        Item flecha = elfo.getFlecha();
        flecha.setQuantidade(1000);
        assertEquals(1000, flecha.getQuantidade());
    }
    
    
    @Test
    public void elfoDeveNascerCom2Flechas() {
        Elfo elfo = new Elfo("Legolas");
        assertEquals(2, elfo.getFlecha().getQuantidade());
    }
    
    @Test
    public void elfoAtiraFlechaPerdeUmaUnidadeGanhaXP(){    
        Elfo elfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Anao");
        elfo.atirarFlecha(dwarf);
        assertEquals(1, elfo.getFlecha().getQuantidade());
    }
    
    @Test
    public void elfoNaoDeveAtirarSemTerFlecha(){
        int contador=0;
        Elfo elfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Anao");
        while(contador<3){
            elfo.atirarFlecha(dwarf);
            contador++;
        }
        assertEquals(0, elfo.getFlecha().getQuantidade());
        assertEquals(2, elfo.getExperiencia());
    }
    
    @Test
    public void elfoDeveAtirarApenasEmDwarf(){
        Dwarf dwarf = new Dwarf("Anao");
        Elfo elfo = new Elfo("Legolas");
        elfo.atirarFlecha(dwarf);
        assertEquals(1, elfo.getFlecha().getQuantidade());
    }
    
}
