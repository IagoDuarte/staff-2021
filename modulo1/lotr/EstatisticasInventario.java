import java.util.*;
public class EstatisticasInventario{
    
    private Inventario inventario;
    
    public EstatisticasInventario( Inventario inventario ){
        this.inventario = inventario;
    }
    
    private boolean eVazio(){
        return this.inventario.getItens().isEmpty();
    }
    
    public double calcularMedia(){
        if( eVazio() ){
            return Double.NaN;
        }
        double somaQuantidade = 0;
        for(Item item : this.inventario.getItens() ){
            somaQuantidade += item.getQuantidade();        
        }
        return somaQuantidade / inventario.getItens().size();        
    }
    
    private ArrayList ordenarItens(ArrayList<Item> itens){
        boolean ordenou = false;
        while(!ordenou){
            ordenou = true;
                for (int i=0 ; i < itens.size() - 1; i++){
                    if(itens.get(i).getQuantidade() > itens.get(i+1).getQuantidade()){
                    Item temp = itens.get(i);
                    itens.set(i, itens.get(i+1));
                    itens.set(i+1, temp);
                    ordenou = false;
                }
            }
        }  
        return itens;
    }
    
    public double calcularMediana(){
        if( eVazio() ){
            return Double.NaN;
        }
        int qtdItens = this.inventario.getItens().size();
        int meio = qtdItens/2;
        int qtdMeio = this.inventario.obterItem(meio).getQuantidade();
        boolean qtdImpar = qtdItens % 2 == 1;
        if(qtdImpar){
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventario.obterItem(meio - 1).getQuantidade();
        return (qtdMeio + qtdMeioMenosUm)/ 2.0;
    }
    
    public int qtdItensAcimaDaMedia (){
        double media = this.calcularMedia();
        int qtdAcima = 0;
        for (Item item : this.inventario.getItens() ){
            if(item.getQuantidade() > media){
                qtdAcima++;
            }
        }
        return qtdAcima;
    }

}
