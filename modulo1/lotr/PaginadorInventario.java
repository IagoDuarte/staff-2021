import java.util.*;
public class PaginadorInventario{
    private Inventario inventario;
    private int marcador = 0;
    
    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public void pular(int marcador){
        this.marcador = marcador > 0 ? marcador : 0;
    }
    
    public ArrayList<Item> limitar(int num){       
        ArrayList<Item> subConjunto = new ArrayList<>();
        int fim = this.marcador + num;
        for(int i = this.marcador; i < fim && i < this.inventario.getItens().size(); i++){
            subConjunto.add( this.inventario.obterItem(i) );
        }
        return subConjunto;
    }

}
