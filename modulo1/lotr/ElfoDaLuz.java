import java.util.*;
public class ElfoDaLuz extends Elfo{
    private final double QTD_VIDA_GANHA = 10;
    private int qtdAtaques;
    boolean ataqueImpar = true; 
    
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    
    {
        this.qtdAtaques = 0;
    }
    
    public ElfoDaLuz(String nome){
        super(nome);
        super.qtdDano = 21.0;
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_VALIDAS.get(0)));
    }
    
    private void alternarDano(){
        ataqueImpar = !ataqueImpar;
        this.qtdDano = ataqueImpar ? 21.0 : (-10.0); 
    }
    
    @Override
    public void perderItem(Item item){
        if(item.getDescricao().equals(DESCRICOES_VALIDAS.get(0))){
            this.inventario.removerItem(item);
        }
    }
    
    private boolean devePerderVida(){
        return qtdAtaques % 2 == 1.0;
    }
    
    public void atacarComEspada(Dwarf dwarf){
        Item espada = getEspada();
        if( espada.getQuantidade() > 0){
            qtdAtaques++;
            dwarf.receberDano();
            if( this.devePerderVida() )
                receberDano();
            else
                ganharVida();
        }
        this.alternarDano();
    }
    
    private Item getEspada() {
        return this.getInventario().getItemPorDescricao(DESCRICOES_VALIDAS.get(0));
    }
    
    private void ganharVida() {
        super.vida += QTD_VIDA_GANHA;
    }
    
}
