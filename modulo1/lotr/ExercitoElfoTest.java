

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
public class ExercitoElfoTest{
    @Test
    public void adicionarElfoVerde(){
        ArrayList<Elfo> resultado = new ArrayList<>();
        ExercitoElfo exercito = new ExercitoElfo();
        ElfoVerde elfo = new ElfoVerde("Elfo Verde");
        exercito.alistarElfo(elfo);
        resultado = exercito.getExercito();
        assertEquals(elfo, resultado.get(0));
    }
    
    @Test
    public void adicionarElfoNoturno(){
        ArrayList<Elfo> resultado = new ArrayList<>();
        ExercitoElfo exercito = new ExercitoElfo();
        ElfoNoturno elfo = new ElfoNoturno("Elfo Noturno");
        exercito.alistarElfo(elfo);
        resultado = exercito.getExercito();
        assertEquals(elfo, resultado.get(0));
    }
    
    @Test
    public void adicionarApenasElfoNoturnoEVerde(){
        ArrayList<Elfo> resultado = new ArrayList<>();
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistarElfo(new ElfoNoturno("Elfo Noturno"));
        exercito.alistarElfo(new ElfoVerde("Elfo Verde"));
        exercito.alistarElfo(new Elfo("Elfo Normal"));
        resultado = exercito.getExercito();
        assertEquals(2, resultado.size());    
    }
    
    @Test
    public void buscarPorStatusRecemCriado(){
        ArrayList<Elfo> resultado = new ArrayList<>();
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistarElfo(new ElfoNoturno("Elfo Noturno"));
        resultado = exercito.buscarLista(Status.RECEM_CRIADO);
        assertEquals(Status.RECEM_CRIADO, resultado.get(0).getStatus());
    }
    
    

}
