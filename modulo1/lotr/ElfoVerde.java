import java.util.*;
public class ElfoVerde extends Elfo{
    
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de aco valiriano",
            "Arco de vidro",
            "Flecha de vidro"
        )
    );
    
    public ElfoVerde(String nome){
        super(nome);
        super.qtdExperienciaPorAtaque = 2;
    }
    
    
    public boolean isDescricaoValida(String descricaoItem){
         return this.DESCRICOES_VALIDAS.contains(descricaoItem);           
    }
    
    @Override
    public void ganharItem(Item item){
        if( this.isDescricaoValida( item.getDescricao() ) ){
            super.inventario.adicionarItem(item);
        }
    }
    
    @Override
    public void perderItem(Item item){
        if( this.isDescricaoValida( item.getDescricao() ) ){
            super.inventario.removerItem(item);
        }
    }
    
}
